---
title: Links
links:
  - title: Error de replicación de Active Directory
    description: Windows Server 2012 R2 - El nombre principal de destino es incorrecto.
    website: https://docs.microsoft.com/es-es/troubleshoot/windows-server/identity/replication-error-2146893022
    image: https://imgs.search.brave.com/f8zILAaRjnr37kiCGfXGJUfVK_mcxqGHod5JTHJWSIw/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9ibG9n/LnRvcGVkaWEuY29t/L3dwLWNvbnRlbnQv/dXBsb2Fkcy8yMDIw/LzA1L01pY3Jvc29m/dF9Mb2dvLnBuZw

  - title: Error 8452
    description: Windows Server 2012 R2 - El contexto de nomenclatura se está quitando o no se replica desde el servidor especificado.
    website: https://docs.microsoft.com/es-es/troubleshoot/windows-server/identity/replication-error-8452
    image: https://imgs.search.brave.com/f8zILAaRjnr37kiCGfXGJUfVK_mcxqGHod5JTHJWSIw/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9ibG9n/LnRvcGVkaWEuY29t/L3dwLWNvbnRlbnQv/dXBsb2Fkcy8yMDIw/LzA1L01pY3Jvc29m/dF9Mb2dvLnBuZw

  - title: Error 1722
    description: Windows Server 2012 R2 - Error de replicación de Active Directory, el servidor RPC no está disponible.
    website: https://docs.microsoft.com/es-es/troubleshoot/windows-server/identity/replication-error-1722-rpc-server-unavailable
    image: https://imgs.search.brave.com/f8zILAaRjnr37kiCGfXGJUfVK_mcxqGHod5JTHJWSIw/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9ibG9n/LnRvcGVkaWEuY29t/L3dwLWNvbnRlbnQv/dXBsb2Fkcy8yMDIw/LzA1L01pY3Jvc29m/dF9Mb2dvLnBuZw

  - title: Error 1256
    description: Windows Server 2012 R2 - Error de replicación de Active Directory, el sistema remoto no está disponible.
    website: https://docs.microsoft.com/es-es/troubleshoot/windows-server/identity/active-directory-replication-error-1256
    image: https://imgs.search.brave.com/f8zILAaRjnr37kiCGfXGJUfVK_mcxqGHod5JTHJWSIw/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9ibG9n/LnRvcGVkaWEuY29t/L3dwLWNvbnRlbnQv/dXBsb2Fkcy8yMDIw/LzA1L01pY3Jvc29m/dF9Mb2dvLnBuZw

  - title: Error 504 nginx
    description: Como solucionar un error 504 en un servidor Nginx.
    website: https://bobcares.com/blog/504-timeout-nginx/
    image: https://imgs.search.brave.com/HVADi4f8dk8et-zuRGlT5j9xtcmxUfUBNJ14vfIdkDE/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9sb2dv/ZG93bmxvYWQub3Jn/L3dwLWNvbnRlbnQv/dXBsb2Fkcy8yMDE4/LzAzL25naW54LWxv/Z28tMy5wbmc

  - title: Error 504 Zentyal
    description: Configuración para solucionar el error 504 en Zentyal.
    website: https://forum.zentyal.org/index.php/topic,24356.msg95446.html#msg95446
    image: https://imgs.search.brave.com/EdoUhzbUxBU749u4FryxGbOLtNSgHxIm25ARF-JW_80/rs:fit:500:500:1/g:ce/aHR0cHM6Ly93d3cu/Ymllbi5ubC96ZW50/eWFsX2xvZ29fdmVy/dF9wb3NpdGl2by5w/bmc

menu:
    main: 
        weight: -20
        params:
            icon: link

comments: false
---

# Horario

| Hora          | Lunes     | Martes         | Miércoles | Jueves         | Viernes   |
| ------------- | --------- | -------------- | --------- | -------------- | --------- |
| 07:55 - 08:50 |  |             |   |        |    |
| 08:50 - 09:45 |  |             |   |         |    |
| 09:45 - 10:40 |   |  |  |  |  |
| 10:40 - 11:10 | Recreo    | Recreo         | Recreo    | Recreo         | Recreo    |
| 11:10 - 12:05 |   |  |  |  |  |
| 12:05 - 13:00 | Servicios |        | Servicios | Web            |  |
| 13:00 - 13:55 | Servicios |        | Servicios | Web            | Web       |