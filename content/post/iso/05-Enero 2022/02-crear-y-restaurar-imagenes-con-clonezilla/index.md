---
title: Crear y restaurar imágenes con clonezilla
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-01-22"
date: "2022-01-22 01:00:00+0200"
slug: /2022/01/crear-y-restaurar-imagenes-con-clonezilla
tags:
  - Windows 10
  - Clonezilla
  - Redes
  - SMB
---

# Crear y restaurar imágenes con clonezilla

[![](photo.png)](IMÁGENES_CLONEZILLA_Raúl_Sánchez_Tarazona.pdf)