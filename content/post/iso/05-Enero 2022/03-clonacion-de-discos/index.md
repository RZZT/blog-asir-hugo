---
title: Clonación de discos
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-01-12"
date: "2022-01-12 01:00:00+0200"
slug: /2022/01/clonacion-de-discos
tags:
  - Windows 10
  - Clonezilla
  - Hirens Boot
  - UEFI
---

# Clonación de discos

[![](photo.png)](CLONACIÓN_Raúl_Sánchez_Tarazona.pdf)