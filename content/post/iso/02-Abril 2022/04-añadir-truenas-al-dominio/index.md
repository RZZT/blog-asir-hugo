---
title: Añadir TrueNAS al dominio
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-11"
date: "2022-04-11 01:00:00+0200"
slug: /2022/04/añadir-truenas-al-dominio
tags:
  - Windows 10
  - Windows Server 2012
  - Active Directory
  - Redes
  - DHCP
  - SMB
  - TrueNAS
  - NAS
---

# Añadir TrueNAS al dominio

[![](photo.png)](TRUENAS_WS_Raúl_Sánchez_Tarazona.pdf)