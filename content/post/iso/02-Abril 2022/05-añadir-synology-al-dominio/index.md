---
title: Añadir Synology al dominio
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-08"
date: "2022-04-08 01:00:00+0200"
slug: /2022/04/añadir-synology-al-dominio
tags:
  - Windows 10
  - Windows Server 2012
  - Active Directory
  - Redes
  - DHCP
  - SMB
  - Synology
  - NAS
---

# Añadir Synology al dominio

[![](photo.png)](SYNOLOGY_WS_Raúl_Sánchez_Tarazona.pdf)