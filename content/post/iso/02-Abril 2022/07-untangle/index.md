---
title: Untangle
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-02"
date: "2022-04-02 01:00:00+0200"
slug: /2022/04/untangle
tags:
  - Windows 10
  - Windows Server 2012
  - Redes
  - DHCP
  - DNS
  - Enrutamiento
  - Untangle
---

# Untangle

[![](photo.png)](UNTANGLE_Raúl_Sánchez_Tarazona.pdf)