---
title: PfSense
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-04"
date: "2022-04-04 01:00:00+0200"
slug: /2022/04/pfsense
tags:
  - Windows 10
  - Windows Server 2012
  - Redes
  - DHCP
  - DNS
  - Enrutamiento
  - PfSense
---

# PfSense

[![](photo.png)](PFSENSE_Raúl_Sánchez_Tarazona.pdf)