---
title: Añadir XigmaNAS al dominio
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-13"
date: "2022-04-13 01:00:00+0200"
slug: /2022/04/añadir-xigmanas-al-dominio
tags:
  - Windows 10
  - Windows Server 2012
  - Active Directory
  - Redes
  - DHCP
  - SMB
  - XigmaNAS
  - NAS
---

# Añadir XigmaNAS al dominio

[![](photo.png)](XIGMANAS_WS_Raúl_Sánchez_Tarazona.pdf)