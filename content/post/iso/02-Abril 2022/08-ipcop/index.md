---
title: IPCOP
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-01"
date: "2022-04-01 01:00:00+0200"
slug: /2022/04/ipcop
tags:
  - Windows 10
  - Windows Server 2012
  - Redes
  - DHCP
  - DNS
  - Enrutamiento
  - IPCOP
---

# IPCOP

[![](photo.png)](IPCOP_Raúl_Sánchez_Tarazona.pdf)