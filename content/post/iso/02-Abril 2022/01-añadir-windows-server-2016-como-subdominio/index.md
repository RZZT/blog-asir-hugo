---
title: Añadir Windows Server 2016 como subdominio
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-04-30"
date: "2022-04-30 01:00:00+0200"
slug: /2022/04/añadir-windows-server-2016-como-subdominio
tags:
  - Windows 10
  - Windows Server 2012
  - Windows Server 2016
  - Active Directory
  - Redes
---

# Añadir Windows Server 2016 como subdominio

[![](photo.png)](SUBDOMINIO_Raúl_Sánchez_Tarazona.pdf)