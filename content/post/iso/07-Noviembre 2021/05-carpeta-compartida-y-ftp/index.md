---
title: Crear carpeta compartida y servidor FTP en Windows 10
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2021-11-14"
date: "2021-11-14 01:00:00+0200"
slug: /2021/11/carpeta-compartida-y-ftp
tags:
  - Windows 10
  - Redes
  - FTP
---

# Crear carpeta compartida y servidor FTP en Windows 10

[![](photo.png)](https://vimeo.com/739736276)