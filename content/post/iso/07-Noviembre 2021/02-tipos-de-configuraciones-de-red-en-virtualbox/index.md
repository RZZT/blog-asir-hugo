---
title: Tipos de configuraciones de red en VirtualBox
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2021-11-16"
date: "2021-11-16 01:00:00+0200"
slug: /2021/11/tipos-de-configuraciones-de-red-en-virtualbox
tags:
  - Windows 10
  - Redes
---

# Tipos de configuraciones de red en VirtualBox

[![](photo.png)](https://vimeo.com/745503991)
