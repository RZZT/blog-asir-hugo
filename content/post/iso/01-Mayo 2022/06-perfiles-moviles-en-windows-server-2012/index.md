---
title: Perfiles móviles en Windows Server 2012
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-07"
date: "2022-05-07 01:00:00+0200"
slug: /2022/05/perfiles-moviles-en-windows-server
tags:
  - Windows 10
  - Windows Server 2012
  - Active Directory
  - DHCP
  - Perfiles Móviles
  - Redes
---

# Perfiles móviles en Windows Server 2012

[![](photo.png)](PERFILES_Raúl_Sánchez_Tarazona.pdf)