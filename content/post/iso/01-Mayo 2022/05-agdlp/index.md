---
title: AGDLP
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-09"
date: "2022-05-09 01:00:00+0200"
slug: /2022/05/agdlp
tags:
  - Windows 10
  - Windows Server 2012
  - ACL
  - Active Directory
  - AGDLP
---

# AGDLP

[![](photo.png)](AGDLP_Raúl_Sánchez_Tarazona.pdf)