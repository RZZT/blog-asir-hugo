---
title: Práctica Sites
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-02"
date: "2022-05-02 01:00:00+0200"
slug: /2022/05/practica-sites
tags:
  - Windows 10
  - Windows Server 2012
  - Windows Server 2016
  - Active Directory
  - DNS
  - Enrutamiento
  - Redes
  - Servidor de Respaldo
  - Sites
---

# Práctica Sites

[![](photo.png)](SITES_Raúl_Sánchez_Tarazona.pdf)