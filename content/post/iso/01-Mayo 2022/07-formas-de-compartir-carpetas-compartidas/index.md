---
title: 5 Formas de compartir carpetas compartidas
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-06"
date: "2022-05-06 01:00:00+0200"
slug: /2022/05/5-formas-de-compartir-carpetas-compartidas
tags:
  - Windows 10
  - Windows Server 2012
  - ACL
  - Active Directory
  - SMB
---

# 5 Formas de compartir carpetas compartidas

[![](photo.png)](CARPETAS_Raúl_Sánchez_Tarazona.pdf)