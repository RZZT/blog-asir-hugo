---
title: Proxy IPCOP
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-19"
date: "2022-05-19 01:00:00+0200"
slug: /2022/05/proxy-ipcop
tags:
  - Enrutamiento
  - IPCOP
  - Proxy
  - Windows Server 2012
  - Ubuntu
---

# Proxy IPCOP

[![](photo.png)](PROXY_Raúl_Sánchez_Tarazona.pdf)