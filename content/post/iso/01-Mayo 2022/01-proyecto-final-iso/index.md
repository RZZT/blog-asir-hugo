---
title: Proyecto Final ISO
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-20"
date: "2022-05-20 01:00:00+0200"
slug: /2022/05/proyecto-final-iso
tags:
  - Windows 10
  - Windows Server 2012
  - Windows Server 2016
  - Active Directory
  - DHCP
  - DNS
  - Enrutamiento
  - IPCOP
  - iSCSI
  - NAS
  - PfSense
  - Proyecto Final ISO
  - Redes
  - Servidor de Respaldo
  - Sites
  - SMB
  - Synology
  - TrueNAS
  - Ubuntu
  - Zentyal
---

# Proyecto Final ISO

[![](photo.png)](PROYECTO_Raúl_Sánchez_Tarazona.pdf)