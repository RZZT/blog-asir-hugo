---
title: Añadir Ubuntu al dominio
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-17"
date: "2022-05-17 01:00:00+0200"
slug: /2022/05/añadir-ubuntu-al-dominio
tags:
  - Active Directory
  - DHCP
  - DNS
  - Enrutamiento
  - Redes
  - Windows Server 2012
  - SMB
  - Ubuntu
---

# Añadir Ubuntu al dominio

[![](photo.png)](UBUNTU_Raúl_Sánchez_Tarazona.pdf)