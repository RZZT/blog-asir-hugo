---
title: Zentyal
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-05-16"
date: "2022-05-16 01:00:00+0200"
slug: /2022/05/zentyal
tags:
  - Windows 10
  - ACL
  - Active Directory
  - DNS
  - Enrutamiento
  - Redes
  - Ubuntu
  - Zentyal
---

# Zentyal

[![](photo.png)](ZENTYAL_Raúl_Sánchez_Tarazona.pdf)