---
title: MBR y GPT
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2021-12-28"
date: "2021-12-28 01:00:00+0200"
slug: /2021/12/mbr-y-gpt
tags:
  - Windows 10
  - MBR
  - GPT
  - Diskpart
  - Ease US
  - Gparted
---

# MBR y GPT

[![](photo.png)](MBR-GPT_Raúl_Sánchez_Tarazona.pdf)
