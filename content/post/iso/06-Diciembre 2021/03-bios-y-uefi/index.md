---
title: BIOS y UEFI
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2021-12-26"
date: "2021-12-26 01:00:00+0200"
slug: /2021/12/bios-y-uefi
tags:
  - Windows 10
  - BIOS
  - UEFI
  - Easy2Boot
  - Ventoy
---

# BIOS y UEFI

[![](photo.png)](BIOS-UEFI_Raúl_Sánchez_Tarazona.pdf)
