---
title: Active Directory y DNS en Windows Server 2012
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-03-16"
date: "2022-03-16 01:00:00+0200"
slug: /2022/03/active-directory-y-dns-en-windows-server-2012
tags:
  - Windows 10
  - Windows Server 2012
  - Active Directory
  - DNS
  - Redes
last_update:
  date: 3/16/2022
  author: Raúl Sánchez
---

# Active Directory y DNS en Windows Server 2012

[![](photo.png)](AD_DNS_Raúl_Sánchez_Tarazona.pdf)