---
title: DHCP y Enrutamiento en Windows Server 2012
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-03-22"
date: "2022-03-22 01:00:00+0200"
slug: /2022/03/dhcp-y-enrutamiento-en-windows-server-2012
tags:
  - Windows 10
  - Windows Server 2012
  - DHCP
  - Enrutamiento
  - Wireshark
---

# DHCP y Enrutamiento en Windows Server 2012

[![](photo.png)](DHCP_ENRUTAMIENTO_Raúl_Sánchez_Tarazona.pdf)