---
title: Añadir cliente Windows 10 a Windows Server 2012
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-03-17"
date: "2022-03-17 01:00:00+0200"
slug: /2022/03/añadir-cliente-windows-10-a-windows-server-2012
tags:
  - Windows 10
  - Windows Server 2012
  - Active Directory
  - DNS
  - Redes
---

# Añadir cliente Windows 10 a Windows Server 2012

[![](photo.png)](W10_Raúl_Sánchez_Tarazona.pdf)