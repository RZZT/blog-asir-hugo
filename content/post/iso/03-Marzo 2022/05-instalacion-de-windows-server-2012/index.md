---
title: Instalación de Windows Server 2012
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-03-15"
date: "2022-03-15 01:00:00+0200"
slug: /2022/03/instalacion-de-windows-server-2012
tags:
  - Windows 10
  - Windows Server 2012
  - Redes
last_update:
  date: 3/15/2022
  author: Raúl Sánchez
---

# Instalación de Windows Server 2012

[![](photo.png)](INSTALACIÓN_Raúl_Sánchez_Tarazona.pdf)