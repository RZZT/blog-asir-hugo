---
title: XigmaNAS
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-26"
date: "2022-02-26 01:00:00+0200"
slug: /2022/02/xigmanas
tags:
  - Windows 10
  - ESXi
  - FTP
  - iSCSI
  - NAS
  - SMB
  - SSH
  - XigmaNAS
---

# XigmaNAS

[![](photo.png)](XIGMANAS_Raúl_Sánchez_Tarazona.pdf)