---
title: OpenMediaVault
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-11"
date: "2022-02-11 01:00:00+0200"
slug: /2022/02/openmediavualt
tags:
  - Windows 10
  - FTP
  - iSCSI
  - NAS
  - NFS
  - OpenMediaVault
  - RAID
  - SMB
  - SSH
  - Ubuntu
---

# OpenMediaVault

[![](photo.png)](OMV_Raúl_Sánchez_Tarazona.pdf)