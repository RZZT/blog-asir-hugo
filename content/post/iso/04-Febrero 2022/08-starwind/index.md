---
title: Starwind
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-01"
date: "2022-02-01 01:00:00+0200"
slug: /2022/02/starwind
tags:
  - ESXi
  - iSCSI
  - NAS
  - SMB
  - SSH
  - Starwind
---

# Starwind

[![](photo.png)](STARWIND_Raúl_Sánchez_Tarazona.pdf)