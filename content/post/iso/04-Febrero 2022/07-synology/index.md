---
title: Synology
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-04"
date: "2022-02-04 01:00:00+0200"
slug: /2022/02/synology
tags:
  - Windows 10
  - ESXi
  - iSCSI
  - NAS
  - NFS
  - SMB
  - SSH
  - Synology
---

# Synology

[![](photo.png)](SYNOLOGY_Raúl_Sánchez_Tarazona.pdf)