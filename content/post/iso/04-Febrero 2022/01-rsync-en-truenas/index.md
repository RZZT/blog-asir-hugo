---
title: RSYNC en TrueNAS
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-28"
date: "2022-02-28 01:00:00+0200"
slug: /2022/02/rysnc-en-truenas
tags:
  - Windows 10
  - NAS
  - Redes
  - SMB
  - TrueNAS
---

# RSYNC en TrueNAS

[![](photo.png)](RSYNC_Raúl_Sánchez_Tarazona.pdf)