---
title: NextCloud en TrueNAS
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-20"
date: "2022-02-20 01:00:00+0200"
slug: /2022/02/nextcloud-en-truenas
tags:
  - Windows 10
  - NAS
  - NextCloud
  - TrueNAS
---

# NextCloud en TrueNAS

[![](photo.png)](NEXTCLOUD_Raúl_Sánchez_Tarazona.pdf)