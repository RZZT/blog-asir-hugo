---
title: TrueNAS
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-18"
date: "2022-02-18 01:00:00+0200"
slug: /2022/02/truenas
tags:
  - Windows 10
  - ACL
  - ESXi
  - iSCSI
  - NAS
  - SMB
  - TrueNAS
  - Ubuntu
---

# TrueNAS

[![](photo.png)](TRUENAS_Raúl_Sánchez_Tarazona.pdf)