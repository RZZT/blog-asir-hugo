---
title: Instalar Ubuntu 20.04 en VMware Workstation
description: 
image:
categories:
  - ISO
author: "Raúl Sánchez"
lastmod: "2022-02-08"
date: "2022-02-08 01:00:00+0200"
slug: /2022/02/instalar-ubuntu-20-04-en-vmware-workstation
tags:
  - Ubuntu
---

# Instalar Ubuntu 20.04 en VMware Workstation

[![](photo.png)](INSTALAR_UBUNTU_Raúl_Sánchez_Tarazona.pdf)