---
title: Conmutación por error DHCP
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fblogs.iuvotech.com%2Fhubfs%2F3999715-broken-windows-wallpaper.jpg%23keepProtocol&f=1&nofb=1&ipt=a53989635067c3f627a73495ee92e19243347f8acd39c30ff9d583863ff2801d&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-28"
date: "2022-09-28 01:00:00+0200"
slug: /2022/09/conmutacion-por-error-dhcp
tags:
- DHCP
- Windows Server 2016
- Windows 10 
- Conmutación por error
---

> Necesitamos dos servidores con el rol *DHCP* instalado, pero solo configurado en el *DC01*.

## Configuración

### Añadir servidor *DC02*

Comenzamos agregando el servidor *DC02* al panel de control.

![](1.png)

Seleccionamos el servidor *DC02*.

![](2.png)

### Conmutación por error

Luego, configuraremos la *Conmutación por error* en el servidor *DC01*.

![](3.png)

Seleccionaremos el único ámbito disponible.

![](4.png)

Indicamos el servidor asociado **DC02**.

![](5.png)

#### Modo equilibrio de carga (50/50)

Escogeremos el modo de carga **(50/50)** para que de esta manera se distribuya la carga entre ambos servidores.

![](6.png)

Finalizaremos la configuración.

![](7.png)

Ahora solicitaremos una nueva configuración de red en el cliente *Windows 10*.

![](8.png)

Veremos que se ha concedido una dirección *IP* al cliente.

![](9.png)

Pero en el cliente, observaremos que el servidor *DHCP* ha sido el **DC02**.

![](10.png)

#### Modo equilibrio de carga (10/90)

Después, editaremos la configuración y estableceremos el porcentaje **(10/90)**.

![](11.png)

Solicitamos una nueva configuración de red en el cliente...

![](8.png)

Veremos que, el servidor *DCHP* ha sido el **DC02** otra vez.

![](10.png)

#### Modo espera activa

Por último, configuraremos el modo de espera activa. De esta manera, si uno de los servidores falla, el otro comenzará a repartir direcciones *IP*.

![](12.png)

Apagamos el servidor **DC02** y solicitamos una nueva configuración de red. Esta vez veremos que nuestro servidor *DHCP* es el **DC01**.

![](13.png)

## Desconfiguración

> Para este proceso, hay que **asegurarse** de que los dos servidores **están encendidos**. 

Por último, seleccionamos el ámbito y vamos a *desconfigurar la conmutación por error*.

![](14.png)

Nos aparece esta advertencia. Aceptamos para continuar.

![](15.png)

Aceptaremos también la siguiente advertencia para eliminar la *conmutación por error*.

![](16.png)

Aparecerá esta pantalla, donde veremos que todo se ha eliminado con éxito.

![](17.png)
