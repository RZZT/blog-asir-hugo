---
title: RSAT Tools
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.itprotoday.com%2Fsites%2Fitprotoday.com%2Ffiles%2Fstyles%2Farticle_featured_standard%2Fpublic%2Fuploads%2F2015%2F08%2Frsat_0.jpg%3Fitok%3Dj_1m2oE-&f=1&nofb=1
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-23"
date: "2022-09-23 01:00:00+0200"
slug: /2022/09/rsat-tools
tags:
- Windows Server 2016
- Windows 10
- Active Directory
- Redes
---

> Primero instalaremos las herramientas *RSAT* en el cliente *Windows 10* como hicimos en esta [práctica](https://asir.raulsanchezzt.com/2022/04/rsat-tools/).

## Configurar servidores

Una vez instalado, reiniciaremos el equipo e iniciaremos el administrador del servidor desde *Windows 10*.

![](1.png)

Buscaremos el servidor *DC01* y lo añadiremos.

![](2.png)

Veremos el Administrador del servidor igual que en el servidor.

![](3.png)

## Comprobaciones

Ahora crearemos un nuevo usuario desde las herramientas *RSAT* de *Windows 10*.

![](4.png)

Le daremos un nombre original al nuevo usuario.

![](5.png)

Le asignamos una contraseña segura que nunca expire.

![](6.png)

Veremos que se ha creado el usuario desde e *Windows 10*.

![](7.png)

Desde el *Windows Server 2016* veremos que se ha creado en el contenedor *Users*.

![](8.png)

Ahora accederemos en el cliente *Windows 10* accederemos con las credenciales de este usuario nuevo.

![](9.png)

Accederemos sin problema con las credenciales del usuario *PruebasRSAT*.

![](10.png)
