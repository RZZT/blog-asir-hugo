---
title: WLAN - Windows Server 2016
description: 
image: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fgetwallpapers.com%2Fwallpaper%2Ffull%2F5%2F0%2F5%2F896039-large-wi-fi-wallpapers-1920x1080.jpg&f=1&nofb=1
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-24"
date: "2022-09-24 01:00:00+0200"
slug: /2022/09/wlan-windows-server-2016
tags:
- Windows Server 2016
- WLAN
- Wi-Fi
---

## Preparación

Para esta práctica voy a usar el adaptador de red **TP-Link**  *TL-WN722N V3*.

![asd](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.ceos3c.com%2Fwp-content%2Fuploads%2F2021%2F05%2FWiFi-Adapter-For-Kali-Linux-000021.png&f=1&nofb=1)

En la máquina comprobaremos que no podemos conectarnos a ninguna red *Wi-Fi*.

![](0.png)

## Instalar característica

Primero, vamos a agregar la característica al servidor `DC01`.

![](1.png)

Seleccionamos el *Servicio WLAN*.

![](2.png)

Esperamos a que se instale.

![](3.png)

## Configuración

Cuando se haya instalado la característica, conectamos el adaptador a nuestro ordenador, y nos aparecerá esta ventana, dónde seleccionamos que se conecte a la máquina virtual.

![](4.png)

Comprobaremos que el servicio *WLAN* está en ejecución.

![](5.png)

Ahora si que veremos las redes *Wi-Fi*, incluida la nuestra...

![](6.png)

> Si no aparecen las redes *Wi-Fi* puede que el servicio esté parado o detenido.

Desde el panel de configuración de red, veremos que ahora hay en total 3 adaptadores.

![](7.png)
