---
title: Eliminar DC Huérfano
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmagazine.souc.ch%2Fwp-content%2Fuploads%2F2020%2F11%2FSORA-memory-card-graph-scaled.jpg&f=1&nofb=1&ipt=8c3687b62be47e6c95b1f3a435403ece628a692de18abb25fe19838023437a09&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-30"
date: "2022-09-29 01:00:00+0200"
slug: /2022/09/dc-huerfano
tags:
- Windows Server 2016
- Windows Server 2019
- Windows 10
- Redes
- Active Directory
- DC Huérfano
- DNS
- Comandos de Administración
- Servidor de Respaldo
---

> Primero, instalaremos *Windows Server 2019* en una máquina virtual, con los mismos adaptadores de red que las anteriores.

## Preparar *Windows Server 2019*

### Red

Primero configuraremos las interfaces de red de este servidor de esta manera.

![](1.png)

Luego, nos uniremos al dominio `aso.org`.

![](2.png)

### *Active Directory*

Después instalaremos un nuevo rol.

![](3.png)

Seleccionamos el rol de *Active Directory*.

![](4.png)

Promovemos este servidor de *Active Directory* y lo añadiremos al dominio `aso.org`.

![](5.png)

Escribimos una contraseña...

![](6.png)

Aquí, seleccionamos **Replicar desde cualquier controlador de dominio**. *(A mí no me aparecía ninguna opción más)*.

![](7.png)

Si todo va bien, instalaremos *AD*.

![](8.png)

### Comprobaciones

Ahora, veremos en el *DC01* que se ha unido en el contenedor *Domain Controllers*.

![](9.png)

Nos aseguraremos de replicar la configuración en todos los servidores. Esto evitará muchos errores después...

![](10.png)

## Eliminar DC01

Ahora, apagamos el servidor *DC01*. Desde el *DC02* verificaremos que está apagada porque no tiene conexión. 

![](11.png)

Luego, lo eliminaremos de los controladores de dominio.

![](12.png)

Nos aparecerá este mensaje para confirmar la eliminación del controlador de dominio.

![](13.png)

Nos advierte que este controlador de dominio es un catálogo global.

![](14.png)

Transferimos los roles de maestro **FSMO** al servidor *Windows Server 2019* con nombre *NEWDC01*.

![](15.png)

Ahora ya no aparecerá en el contenedor de *Controladores de Dominio*.

![](16.png)

Después, eliminaremos el servidor *DC01* desde los *Sitios y Servicios*.

![](17.png)

Confirmamos la eliminación...

![](18.png)

Ahora ya solo tendremos al *DC02* y *NEWDC01*.

![](19.png)

Eliminaremos todos los registros que tengan con el *DC01* en los servidores.

![](20.png)

## Comprobaciones

### netdom query fsmo

Comprobaremos quien tiene los permisos **FSMO** con este comando.

![](21.png)

Ahora, cambiaremos la dirección *IP* al servidor *NEWDC01* y le asignaremos la que tenía el antiguo *DC01* en mi caso, la `192.168.10.1`.

![](22.png)

### ipconfig

En el servidor *NEWDC01* escribiremos estos comandos para limpiar los registros **DNS**.

![](23.png)

### dcfiag /fix

En el servidor *NEWDC01* haremos un **diagnóstico** del controlador de dominio para comprobar que todo funciona bien.

![](24.png)

![](25.png)

### netlogon

Luego reiniciaremos el servicio **netlogon**.

![](26.png)

### DNS

Primero nos aseguraremos que el servidor *NEWDC01* solo dará servicio a las consultas que vengan por la interfaz *LAN*.

![](29.png)

Luego, eliminaremos todos los registros *DNS* que apunten al servidor *DC01*.

![](30.png)

#### dcdiag /test:DNS

Para comprobar el servicio *DNS*, ejecutaremos el comando en el servidor *NEWDC01*.

![](31.png)

#### nslookup

Desde el servidor **DC02**, comprobaremos que resuelve bien los nombres del dominio y del equipo.

![](32.png)

### ping

Comprobaremos la resolución de nombres *DNS* y la conexión entre el servidor *DC02* y *NEWDC01*.

![](27.png)

También funciona desde el servidor *NEWDC01* al *DC02*.

![](28.png)

### repadmin /showrepl

Verificamos la replicación entre los servidores, primero desde el servidor *NEWDC01*.

![](33.png)

Observaremos que todo funciona también si ejecutamos el mismo comando pero en el servidor *DC02*.

![](34.png)

### dcdiag /S:\<SERVER>

Por último, haremos un diagnóstico al controlador de dominio desde el servidor *DC02* al *NEWDC01*.

![](35.png)

![](36.png)

Haremos lo mismo pero a la inversa, ejecutaremos el comando en el servidor *NEWDC01* para comprobar el *DC02*.

![](37.png)

![](38.png)
