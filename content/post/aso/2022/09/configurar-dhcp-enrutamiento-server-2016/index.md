---
title: Configurar DHCP y Enrutamiento en Windows Server 2016
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.explicit.bing.net%2Fth%3Fid%3DOIP.1_dILyHTNx76wx-tKJoV4wHaFw%26pid%3DApi&f=1
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-19"
date: "2022-09-15 01:00:00+0200"
slug: /2022/09/configurar-dhcp-enrutamiento-server-2016
tags:
  - Windows Server 2016
  - Windows 10
  - DHCP
  - Enrutamiento
---

## Instalación de los roles
Primero, instalaremos los roles *Acceso remoto* y *Servidor DHCP* en el servidor.

![](1.png)

Seleccionaremos el servicio de *Enrutamiento* para el rol de *Acceso Remoto*.

![](2.png)

Esperaremos a que se instale.

![](3.png)

## Configuración
### DHCP
Una vez instalado, iniciaremos la configuración posterior de *DHCP*.

![](4.png)

Seleccionaremos que queremos usar las credenciales del usuario **Administrador**.

![](5.png)

Veremos que el estado está *Listo*.

![](6.png)

#### Ámbito nuevo
Luego, crearemos un nuevo ámbito en este servidor.

![](7.png)

Lo llamaremos *ASO*.

![](8.png)

Indicaremos el intervalo de direcciones *IP* y la máscara de subred.

![](9.png)

Excluiremos el intervalo de direcciones desde la **1** hasta la **100**.

![](10.png)

Después, indicaremos que la **puerta de enlace predeterminada** es la dirección `192.168.10.1` **de momento**.

![](11.png)

El servidor *DNS* también es la dirección `192.168.10.1`.

![](12.png)

Activaremos este ámbito ahora.

![](13.png)

Finalizaremos el asistente y veremos las opciones de este ámbito.

![](14.png)

#### Cliente Windows 10
En el cliente, cambiaremos la configuración para obtener una dirección *IP* de manera dinámica por *DHCP*.

![](15.png)

Observaremos como nos da una dirección *IP* por *DHCP* en el rango que le hemos dado.

![](16.png)

En el servidor, podemos ver las concesiones de direcciones.

![](17.png)

#### Crear Reserva
Por último, crearemos una nueva reserva para el cliente *Windows 10*.

![](18.png)

En el cliente, usaremos el comando `ipconfig /all` para ver la dirección física *(MAC)*.

![](19.png)

Copiaremos esta dirección *MAC* en la configuración de la reserva *DHCP*.

![](20.png)

En el cliente, solicitaremos otra dirección *IP* por *DHCP* y nos asignará la reserva.

![](21.png)

### Enrutamiento

Luego, configuraremos el enrutamiento.

![](22.png)

Seleccionaremos la configuración de traducciones de red (NAT).

![](23.png)

Elegiremos la interfaz de red que sale a *Internet* (WAN).

![](24.png)

Finalizaremos el asistente y veremos las configuraciones de los adaptadores de red.

![](25.png)

## Comprobaciones

Desde el cliente *Windows 10* veremos que tenemos conexión a *Internet*.

![](26.png)