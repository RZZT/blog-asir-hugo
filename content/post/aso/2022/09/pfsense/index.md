---
title: PfSense
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpaperaccess.com%2Ffull%2F4868861.jpg&f=1&nofb=1
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-21"
date: "2022-09-21 01:00:00+0200"
slug: /2022/09/pfsense
tags:
- Windows Server 2016
- Windows 10
- PfSense
- Redes
- DHCP
- Enrutamiento
- DNS
---

> Primero crearemos una nueva máquina virtual e instalaremos `PfSense` como hicimos en esta [práctica](https://asir.raulsanchezzt.com/2022/04/pfsense/).

## Configuración

Una vez hayamos creado la máquina virtual, añadiremos los dos adaptadores de red *Custom*. Uno para la red **LAN** y otro para la red **WAN**.

![](0.png)

Luego, configuraremos el adaptador *WAN* de forma dinámicamente por *DHCP* y le asignaremos la dirección estática `192.168.10.254` al adaptador *LAN*.

![](1.png)  

### Web

> Las credenciales por defecto del panel *web* son: **admin:pfsense**.

Terminaremos la configuración desde el panel web, indicando nuestro dominio `aso.org`.

![](2.png)

Desde este panel también podemos configurar las interfaces de red.

![](3.png)

## Comprobaciones

Cambiaremos la configuración del servicio *DHCP* en el servidor *DC01* para indicar la dirección *IP* de *PfSense*.

![](4.png)

En el cliente *Windows 10* solicitaremos una nueva configuración por *DHCP* y nos cambiará la puerta de enlace.

![](5.png)

Si hacemos un `tracert` hasta *google.com* veremos que pasa por el *firewall* y luego sale a *Internet* hasta llegar al destino.    

![](6.png)

> Como estamos utilizando un adaptador *NAT* para la interfaz *WAN* no podemos ver los saltos que hace hasta llegar al destino.
