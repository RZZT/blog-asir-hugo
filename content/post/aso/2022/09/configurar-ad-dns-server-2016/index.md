---
title: Configurar Active Directory y DNS en Windows Server 2016
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F4.bp.blogspot.com%2F-5Jc65jfpUiQ%2FWLsPYvVGJbI%2FAAAAAAAABj0%2Fd_7ZPLDV2lQR0Jpo-dgkLe2EjKBfsLlcQCLcB%2Fs1600%2Fwindows-server-2016-screenshot-2.png&f=1&nofb=1
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-14"
date: "2022-09-12 01:00:00+0200"
slug: /2022/09/configurar-ad-dns-server-2016
tags:
  - Windows Server 2016
  - Active Directory
  - DNS
  - Redes
---
> Primero crearemos una nueva máquina virtual e instalaremos `Windows Server 2016` como hicimos en esta [práctica](https://asir.raulsanchezzt.com/2022/04/a%C3%B1adir-windows-server-2016-como-subdominio/).
## Ajustes
### Red
Configuraremos el adaptador *Custom* de tipo *NAT* con la dirección de red `10.10.10.0/24` con el servicio *DHCP* activado.

![](1.png)

La puerta de enlace de esta red será la `10.10.10.254`.

![](2.png)

Luego, crearemos un nuevo adaptador de red *Custom* **Host-Only** con la dirección de red `192.168.10.0/24` sin el servicio **DHCP** y sin conectarnos con el *host*.

![](3.png)

Añadiremos ambos adaptadores *Custom* a la máquina virtual.

![](4.png)

En el servidor, renombraremos las interfaces de red, le asignaremos una dirección estática a la interfaz *LAN*  y veremos que la interfaz *WAN* tiene una dirección en dinámica de la red `10.10.10.0/24`.

![](5.png)


### Nombre del equipo
Antes de instalar roles y características, cambiaremos el nombre del equipo y lo llamaremos **DC01**.

![](6.png)

Tendremos que reiniciar el servidor para cambiar el nombre.

## Instación de AD y DNS
Luego, instalaremos roles.

![](7.png)

Seleccionaremos el servidor **actual**.

![](8.png)

Después, seleccionaremos los roles de *Active Directory* y *DNS*.

![](9.png)

Esperaremos a que se instalen.

![](10.png)

## Configuración
### Active Directory
#### Promover a controlador de dominio
Después de instalarlo, tendremos que promover este servidor a controlador de dominio.

![](11.png)

Crearemos un nuevo bosque, con el nombre de dominio `aso.org`.

![](12.png)

Escribiremos la contraseña.

![](13.png)

Revisaremos las opciones y continuaremos.

![](14.png)

También podemos ver el script en **PowerShell**:

```PowerShell
#
# Script de Windows PowerShell para implementación de AD DS
#

Import-Module ADDSDeployment
Install-ADDSForest `
-CreateDnsDelegation:$false `
-DatabasePath "C:\Windows\NTDS" `
-DomainMode "WinThreshold" `
-DomainName "aso.org" `
-DomainNetbiosName "ASO" `
-ForestMode "WinThreshold" `
-InstallDns:$true `
-LogPath "C:\Windows\NTDS" `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-Force:$true
```

Nos aparecerá este error, porque el usuario **Administrador** no tiene contraseña.

![](15.png)

#### Solucionar error - Administrador local
Buscaremos la configuración de cuentas de usuario.

![](16.png)

Seleccionaremos el usuario **Administrador**.

![](17.png)

Crearemos una **contraseña**.

![](18.png)

Escribiremos la misma contraseña que para el usuario, para no olvidarla.

![](19.png)

Comprobaremos los requisitos de nuevo, y ahora nos dejará instalar.

![](20.png)

Después de unos 15 minutos, podremos acceder al servidor con el usuario **Administrador** de nuestro dominio.

![](21.png)


### DNS
#### Zona de búsqueda inversa
Accederemos al **Administrador de DNS**

![](22.png)

Crearemos una nueva zona de **búsqueda inversa**.

![](23.png)

En el asistente, seleccionaremos que queremos crear una **Zona principal**.

![](24.png)

Replicaremos los datos *DNS* para todos los servidores *DNS* que se ejecutan en este dominio.

![](25.png)

Seleccionaremos **IPv4**.

![](26.png)

Indicaremos la dirección de red.

![](27.png)

Solo permitiremos actualizaciones dinámicas seguras.

![](28.png)

Revisaremos todas las opciones y finalizaremos el asistente.

![](29.png)

#### Propiedades
Luego, modificaremos el registro de nuestra **dirección IP**.

![](30.png)

Aquí seleccionaremos el siguiente *check* para actualizar el registro del puntero asociado.

![](31.png)

Después, editaremos las propiedades del servidor.

![](32.png)

Solo escucharemos peticiones *DNS* desde la dirección *IPv4* privada.

![](33.png)

Añadiremos los servidores *DNS* de *Google* como **Reenviadores**.

![](38.png)

#### Adaptadores
Por último, entraremos en la configuración *IPv6* de los adaptadores.

![](34.png)

Obtendremos una dirección automáticamente.

![](35.png)
> Cambiaremos la configuración IPv6 en ambos adaptadores.

En la configuración *IPv4* del adaptador LAN indicaremos nuestra dirección IP como servidor *DNS*.

![](36.png)

En cambio no tocaremos nada en la configuración *IPv4* del adaptador WAN.

![](41.png)

## Comprobaciones
### nslookup
Usando el comando `nslookup` que nuestro servidor resuelve los nombres de dominio.

![](37.png)

### ping
Observaremos que tenemos conexión a *Internet* usando el comando `ping`.

![](39.png)

### tracert
Con el comando `tracert` veremos por dónde pasa el paquete hasta llegar a `google.com`.

![](40.png)