
---
title: Añadir cliente Windows 10 a Windows Server 2016
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages.everyeye.it%2Fimg-notizie%2Fwindows-10-wallpaper-v2-230522.jpg&f=1&nofb=1
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-14"
date: "2022-09-14 01:00:00+0200"
slug: /2022/09/cliente-windows-10-server-2016
tags:
  - Windows Server 2016
  - Windows 10
  - Active Directory
  - Redes
---

> Primero crearemos una nueva máquina virtual e instalaremos `Windows 10` como hicimos en esta [práctica](https://asir.raulsanchezzt.com/2021/11/instalar-windows-10-en-vmware-workstation/).

## Configuraciones
### Red
Añadiremos el adaptador de red *Custom* que hemos creado antes, para que se pueda conectar con el servidor mediante la red *LAN*.

![](1.png)

De momento, estableceremos una dirección *IP* estática hasta que instalemos el servicio *DHCP* en el servidor.

![](2.png)

Veremos nuestra dirección *IP* y comprobaremos que tenemos conexión con el servidor **DC01** usando el comando `ping`.

![](3.png)

Además, comprobaremos la resolución de nombres *DNS* usando el comando `nslookup`.

![](4.png)

### Añadir al dominio
Luego, cambiaremos el nombre del equipo y nos uniremos al dominio `aso.org`.

![](5.png)

Escribiremos las credenciales del usuario **Administrador**.

![](6.png)

Si hemos hecho todo bien, nos uniremos.

![](7.png)

En el servidor, veremos que se ha unido en el contenedor *Computers*.

![](8.png)

## Crear usuarios del dominio
Crearemos un nuevo usuario del dominio llamado `Usuario01`.

![](9.png)

Le asignaremos una contraseña que nunca expire.

![](10.png)

Iniciaremos con este usuario en el cliente `Windows 10` que ahora pertenece al dominio.
 
![](11.png)

## Comprobaciones
En las propiedades del equipo, vereremos cual es el nombre completo del dispositivo.

![](12.png)

Desde el `cmd` veremos que pertenecemos al dominio también usando el comando `ipconfig /all`.

![](13.png)