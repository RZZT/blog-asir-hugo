---
title: Controlador de dominio adicional
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.vox-cdn.com%2Fthumbor%2F0GYjFACY9D3VsSPG3WLBwuK94kg%3D%2F0x0%3A925x617%2F1200x800%2Ffilters%3Afocal(0x0%3A925x617)%2Fcdn.vox-cdn.com%2Fuploads%2Fchorus_image%2Fimage%2F48641945%2Fbackblaze.0.0.jpg&
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-20"
date: "2022-09-20 01:00:00+0200"
slug: /2022/09/servidor-respaldo-server-2016
tags:
- Windows Server 2016
- Windows 10
- Active Directory
- DNS
- DHCP
- Enrutamiento
- Redes
- Servidor de Respaldo
---

> Primero, crearemos una nueva máquina virtual e instalaremos *Windows Server 2016* como hicimos en prácticas anteriores.

## Configuraciones

### Red

Añadiremos los mismos adaptadores de red que para el servidor *DC01*.

![](1.png)

Luego los configuraremos de esta manera:

- El adaptador **LAN** tiene una dirección estática.

- El adaptador **WAN** tiene una dirección dinámica por *DHCP* del *router* virtual.

![](2.png)

Desde el *PowerShell* comprobaremos la configuración y veremos que tenemos conexión con el servidor *DC01*.

![](3.png)

### Añadir al dominio

Luego, cambiaremos el nombre del equipo y nos uniremos al dominio `iso.org`.

![](4.png)

Escribiremos las credenciales del usuario **Administrador**.

![](5.png)

Si hemos hecho todo bien, nos uniremos sin problema.

![](6.png)

Después de que se reinicie, veremos los cambios...

![](7.png)

Desde el servidor *DC01* veremos que este nuevo servidor se ha añadido al contenedor *Computers*.

![](8.png)

## Active Directory

### Instalación

Ahora instalaremos un nuevo rol en el servidor *DC02*.

![](9.png)

Seleccionaremos el rol *Servicios de dominio de Active Directory*.

![](10.png)

Esperaremos a que se instale.

![](11.png)

### Configuración

Cuando se haya instalado, comenzaremos con la configuración posterior.

![](12.png)

Como va a ser un servidor de respaldo **(Controlador Adicional de Dominio)** seleccionaremos estas opciones.

![](13.png)

Indicaremos la contraseña...

![](14.png)

Replicaremos las configuraciones desde el servidor *DC01*.

![](15.png)

Esperaremos a que se instale.

![](16.png)

### Comprobaciones de replicación

Desde el servidor *DC01* replicaremos la configuración al *DC02*.

![](17.png)

De la misma manera, podremos forzar la replicación del *DC01* desde *DC02*.

![](19.png)

Si se ha replicado bien, nos aparecerá esta ventana.

![](20.png)

Ahora desde el administrador de Usuarios de *AD* del *DC02* veremos que se ha replicado el objeto del equipo que está en el dominio.



![](21.png)

También veremos que se han replicado los controladores de dominio.

![](22.png)

La zona de búsqueda directa del *DNS*.

![](23.png)

La zona inversa del *DNS*.

![](24.png)

Los servidores de nombres.

![](25.png)

El usuario que creamos anteriormente se ha replicado también.

![](26.png)

## DHCP

Instalaremos el rol *DHCP* en el servidor *DC02*.

![](27.png)

Crearemos un nuevo ámbito.

![](28.png)

Nombraremos el ámbito desde el asistente.

![](29.png)

Luego indicaremos el intervalo de direcciones a repartir.

![](30.png)

Agregaremos un rango de direcciones excluidas como el que creamos en el *DC01*.

![](31.png)

Configuraremos las puertas de enlace predeterminadas.

![](32.png)

Introducimos las direcciones *IP* de los servidores *DNS* y el dominio primario.

![](33.png)

Veremos el resultado final.

![](34.png)

## Enrutamiento

Por último, instalaremos el rol de acceso remoto en el servidor *DC02*.

![](35.png)

Seleccionaremos los servicios de *Enrutamiento*.

![](36.png)

Luego lo configuraremos.

![](37.png)

Configuraremos el enrutamiento *NAT*.

![](38.png)

Seleccionaremos la Interfaz *WAN*.

![](39.png)

## Comprobar Servidor de Respaldo

En el servidor *DC01* reconfiguraremos el servicio *DHCP* y añadiremos la dirección *IP* del servidor de respaldo (*DC02*) en el enrutador y *DNS*.

![](40.png)

En el cliente, solicitaremos otra vez al servidor *DHCP* y comprobaremos que tenemos la puerta de enlace y DNS de respaldo.

![](41.png)

Luego, apagaremos el servidor *DC01* e iniciaremos con el **Usuario01**.

![](42.png)

Veremos que tenemos conexión con *Internet* y que salimos por el servidor *DC02*.

![](43.png)

Finalmente, en el servidor *DC02* veremos que tenemos la concesión de la dirección *IP*.

![](44.png)
