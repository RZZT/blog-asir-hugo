---
title: NIC Teaming
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fwp2044697.jpg&f=1&nofb=1&ipt=20e0bef26b9590e61a035cf78b75339d4014c61bd9f982bce2a2c95e6a56019f&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-09-27"
date: "2022-09-27 01:00:00+0200"
slug: /2022/09/nic-teaming
tags:
- NIC Teaming
- Windows Server 2016
- Windows 10
- Redes
---

## Configuración

### Adaptadores

Primero añadiremos un nuevo adaptador *Custom* **LAN** al servidor *DC01*.

![](1.png)

> Eliminamos la configuración estática de todos los adaptadores.

### NIC

Luego, habilitaremos la *Formación de equipos NIC*.

![](2.png)

Añadimos un nuevo equipo con los adaptadores de red **LAN**.

![](3.png)

Ahora veremos un nuevo adaptador llamado **ASO**.

![](4.png)

Configuraremos este nuevo adaptador con la configuración que teníamos en el adaptador **LAN**.

![](5.png)

Veremos que los otros adaptadores, solo tienen configurado el *Protocolo de multiplexor*.

![](7.png)

Observamos que ahora solo tenemos dos adaptadores y la *Formación de equipos NIC* está habilitada.

![](6.png)

## Comprobaciones

Primero, comprobaremos que tenemos conexión con el servidor *DC01* desde el cliente *Windows 10* con los dos adaptadores funcionando.

![](8.png)

Luego, desconectaremos un adaptador *LAN* en el servidor *DC01*.

![](9.png)

Volveremos a hacer `ping` y veremos que aún tenemos conexión debido a que el adaptador **ASO** sigue funcionando.

![](10.png)

Ahora, desconectaremos los dos adaptadores *LAN* y observamos que el adaptador **ASO** tampoco funciona.

![](11.png)

Desde el cliente *Windows 10* comprobamos que no tenemos conexión.

![](12.png)
