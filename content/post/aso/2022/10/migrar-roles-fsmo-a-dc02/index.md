---
title: Migrar Roles FSMO a DC02
description: 
image: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.techiebird.com%2Fwp-content%2Fuploads%2F2019%2F11%2Fimage-25.png&f=1&nofb=1&ipt=4f9113ae1792012c029d5aa788e3aefaef49b62007efa9fa3195c0f2cc0a71b4&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-10"
date: "2022-10-10 01:00:00+0200"
slug: /2022/10/migrar-roles-fsmo-a-dc02
tags:
- Windows Server 2019
- Active Directory
- Comandos de Administración
- Redes
- Servidor de Respaldo
---

> **Microsoft** divide las responsabilidades de un *DC* en 5 roles separados que juntos hacen un sistema de **AD** completo.

## Preparación

Antes de comenzar comprobaremos que la replicación entre el servidor *NEWDC01* y *DC02* funciona.

![](0.png)

Ahora desde el servidor *DC02* veremos que el servidor *NEWDC01* es el que tiene todos los roles **FSMO**.

![](1.png)

## Transferir Roles Maestros de Operaciones

### RID

Desde la configuración de *Usuarios y equipos de Active Directory* accederemos a la configuración del **Maestro de operaciones**.

![](2.png)

Primero cambiaremos el **Maestro de operaciones de identificador relativo (RID)**.

![](3.png)

Confirmaremos la transferencia.

![](4.png)

Veremos que ha cambiado.

![](5.png)

### Controlador principal de dominio.

Luego, cambiaremos el **Controlador principal de dominio**.

![](6.png)

Verificamos que ha cambiado.

![](7.png)

### Infraestructura

Por último, cambiaremos el **Maestro de operaciones de Infraestructura**.

![](8.png)

Todo se ha transferido con éxito.

![](9.png)

### Nomenclatura de dominio

Después, desde la herramienta *Dominios y confianzas de Active Directory* entramos a la configuración de **Maestro de operaciones**.

![](10.png)

Aquí cambiaremos el **Maestro de operaciones de nomenclatura de dominios**.

![](11.png)

Comprobamos que se ha transferido.

![](12.png)

### Esquema

Primero registraremos el `.dll` del esquema del *Active Directory* desde una consola de comandos.

![](13.png)

Después abriremos la consola `mmc` desde la ventana de ejecutar. *(Windows + R)*

![](14.png)

Aquí agregaremos el complemento *Esquema de Active Directory*.

![](15.png)

Haciendo clic derecho, entramos en **Cambiar el controlador de dominio de AD**.

![](16.png)

Aquí indicaremos el servidor **DC02**.

![](17.png)

Aparecerá este mensaje y continuaremos.

![](18.png)

Luego, entraremos en la configuración de **Maestro de operaciones** de la misma manera.

![](19.png)

Cambiaremos el **Maestro de operaciones de esquema**.

![](20.png)

Comprobamos que se ha transferido bien.

![](21.png)

## Comprobación

Ejecutaremos el mismo comando que al principio, y observaremos que hemos transferido todos los roles **FSMO** al servidor **DC02** con éxito.

![](22.png)
