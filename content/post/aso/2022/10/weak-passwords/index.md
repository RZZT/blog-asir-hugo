---
title: Contraseñas Débiles
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.cravingtech.com%2Fblog%2Fwp-content%2Fuploads%2F2017%2F12%2Fweak-passwords.jpg&f=1&nofb=1&ipt=0db40e1c77663600d68cd5110b6c0f810c5fa4ccd74f3f72d293c55ac01b7bee&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-05"
date: "2022-10-07 01:00:00+0200"
slug: /2022/10/contrasenas-debiles
tags:
- Windows Server 2019
- Windows 10
- Active Directory
---

> **Objetivo**: Establecer que los usuarios de un grupo puedan tener contraseñas de 4 dígitos. (Muy débiles)

## Configuración

Primero, iniciaremos el *Centro de administración de Active Directory*.

![](1.png)

Aquí entraremos en la carpeta *System*.

![](2.png)

Luego entramos en la carpeta *Password Settings Container*.

![](3.png)

Crearemos una nueva configuración.

![](4.png)

Exigiremos una longitud mínima de 1 carácter y desmarcaremos la opción **Las contraseñas deben cumplir los requisitos de complejidad**. Asignaremos esta configuración directamente al grupo de **Recursos Humanos**.

![](5.png)

Luego, seleccionaremos los dos usuarios de **Recursos Humanos** y configuraremos que tengan que cambiar la contraseña en el siguiente inicio de sesión.

![](6.png)

## Comprobación

Ahora iniciaremos con un usuario de **Recursos Humanos** y nos obligará a cambiar la contraseña.

![](7.png)

Indicaremos una contraseña de *4 dígitos*.

![](8.png)

Veremos que sí que nos deja cambiar a una contraseña **no segura**.

![](9.png)
