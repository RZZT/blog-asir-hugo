---
title: Consultas en Active Directory
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fblog.udemy.com%2Fwp-content%2Fuploads%2F2013%2F06%2Fshutterstock_73381360.jpg&f=1&nofb=1&ipt=cfde7d15f97c0c4c91faacd6f342077af41e6846ab3b989033f4640d8da82d95&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-05"
date: "2022-10-08 01:00:00+0200"
slug: /2022/10/consultas-en-active-directory
tags:
- Windows Server 2019
- Active Directory 
- Consultas Active Directory
---

## Configuración

Primero crearemos una nueva consulta en *Active Directory*.

![](1.png)

### Usuarios que empiecen por "A"

En esta consulta queremos buscar los nombres de usuarios que empiecen con la letra *"A"*.

![](2.png)

En este caso solo tenemos al usuario **Administrador**.

![](3.png)

### Usuario Deshabilitados

Luego, buscaremos a los usuarios que tienen la cuenta *deshabilitada*.

![](4.png)

Veremos que tenemos varios usuarios *deshabilitados* en nuestro dominio.

![](5.png)

### Equipos con Sistema Operativo Windows

Por último, buscaremos equipos que tengan el sistema operativo *Windows* instalado.

![](6.png)

Observaremos todos los equipos que pertenecen a nuestro dominio con *Windows* instalado.

![](7.png)

## Exportar

Puede que estas consultas desaparezcan, así que las vamos a exportar por seguridad.

![](10.png)

Indicaremos la ubicación y el nombre del archivo en formato `.xml`.

![](11.png)

Podemos ver el contenido de la consulta.

![](12.png)

## Importar

Si salimos y volvemos a entrar a la herramienta de **Usuarios y equipos de Active Directory** veremos que las consultas han desaparecido así que vamos a importarlas.

![](13.png)

Seleccionamos un archivo `.xml`.

![](14.png)

Nos deja modificar la consulta antes de importarla pero en este caso la dejaremos igual.

![](15.png)

Ahora tendremos la consulta de nuevo.

![](16.png)
