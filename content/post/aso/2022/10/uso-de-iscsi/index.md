---
title: Uso de iSCSI
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.itprotoday.com%2Fsites%2Fitprotoday.com%2Ffiles%2Fstyles%2Farticle_featured_standard%2Fpublic%2Fuploads%2F2013%2F02%2Fdata-storage-rack-680x340_0.jpg%3Fitok%3Dtn8aZ9ua&f=1&nofb=1&ipt=9fe741f6857fcbbded9396bac968b514884bcc60810f6c3ef5d04e5b33215e28&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-17"
date: "2022-10-16 01:00:00+0200"
slug: /2022/10/uso-de-iscsi
tags:
- Windows Server 2019
- Windows 10
- iSCSI
- RAID
---

## Preparación

### Añadir Discos a *DC02*

Primero, añadiremos tres nuevos discos duros de 10 *GB* al servidor *DC02*.

![](1.png)

Luego iniciaremos los discos desde el **Administrador de Discos**.

![](2.png)

### Crear *RAID-5*

Después crearemos un **RAID-5**.

![](3.png)

Seleccionaremos todos los discos que vamos a usar.

![](4.png)

Le asignaremos la letra *D:*

![](5.png)

Le pondremos un nombre original.

![](6.png)

Nos advertirá que se van a convertir en discos dinámicos.

![](7.png)

Ahora veremos un nuevo volumen **RAID-5**.

![](8.png)

Veremos que tenemos un nuevo dispositivo desde el **Explorador de Archivos**.

![](9.png)

### Rol

Ahora instalaremos el rol de **iSCSI**.

![](10.png)

## Configuración

Una vez hayamos instalado el rol, crearemos un nuevo disco virtual.

![](11.png)

Seleccionaremos el volumen *D:* del servidor *DC02*

![](12.png)

Nombraremos el disco virtual.

![](13.png)

Especificaremos el tamaño total.

![](14.png)

Crearemos un nuevo destino.

![](15.png)

Nombraremos el destino.

![](16.png)

Añadiremos los dos clientes de *Windows 10* mediante su nombre en el **DNS**. 

![](17.png)

Veremos los dos registros.

![](18.png)

Confirmaremos la creación.

![](19.png)

Esperaremos a que se cree.

![](20.png)

## Comprobación Cliente 1

Ahora usaremos el iniciador de *iSCSI* desde el cliente.

![](21.png)

Buscaremos mediante el nombre de dominio.

![](22.png)

Conectaremos el único destino posible.

![](23.png)

Luego, iniciaremos este disco desde el **Administrador de Discos**.

![](24.png)

Crearemos un nuevo volumen.

![](25.png)

Este volumen se montará en la letra *E:* y crearemos una nueva carpeta.

![](26.png)

## Crear nuevos destinos *iSCSI*

Ahora le vamos a asignar diferentes destinos a un mismo disco virtual.

![](27.png)

Veremos el destino que hemos creado antes y crearemos uno nuevo.

![](28.png)

### *IP*

Le damos nombre...

![](29.png)

Añadiremos los dos clientes *Windows 10* mediante la dirección *IP*.

![](30.png)

Veremos los registros.

![](31.png)

Confirmaremos todos los ajustes.

![](32.png)

#### Comprobación Cliente 2

En el otro cliente *Windows 10* añadiremos el nuevo destino buscando por *IP*.

![](33.png)

Veremos que está conectado.

![](34.png)

Se ha creado y montado directamente el volumen.

![](35.png)

Entraremos en el directorio y crearemos una nueva carpeta.

![](36.png)

### *MAC*

Por último, crearemos un nuevo destino usando las direcciones **MAC**.

![](37.png)

Lo nombraremos...

![](38.png)

Crearemos 3 nuevos registros con las direcciones *MAC* de los clientes más la del servidor *DC02*.

![](39.png)

Confirmaremos la creación.

![](40.png)

#### Comprobación *DC02*

En el servidor *DC02* buscaremos los *iSCSI* del propio servidor mediante el nombre de dominio.

![](41.png)

Veremos el destino que hemos conectado.

![](42.png)

Observaremos que se ha añadido desde el *Administrador de Discos*.

![](43.png)

Crearemos otra carpeta desde el servidor.

![](44.png)
