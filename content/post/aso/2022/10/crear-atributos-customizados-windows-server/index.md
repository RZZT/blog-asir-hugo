---
title: Crear atributos customizados AD
description: 
image: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fetutorials.org%2Fshared%2Fimages%2Ftutorials%2Ftutorial_63%2Ff05tk13.jpg&f=1&nofb=1&ipt=1750f7fe6d99ea76b15103085477590a8d763b4565c2d609f43c45b50ca090e8&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-11"
date: "2022-10-11 01:00:00+0200"
slug: /2022/10/crear-atributos-customizados-windows-server
tags:
- Windows Server 2019
- Atributos y Clases
- Active Directory
- Servidor de Respaldo
- PowerShell
- OID 
---

## Obtención de *OID*

Para ampliar el esquema de **AD** vamos primero vamos a generar un **OID** raíz único a partir de este *script* que hemos encontrado en esta [página](https://learn.microsoft.com/es-es/windows/win32/ad/obtaining-an-object-identifier-from-microsoft) de **Microsoft**. Copiamos el contenido en un archivo de texto en el servidor y editamos el archivo de salida para que se guarde en el escritorio del usuario **Administrador**.

![](1.png)  

Lo guardaremos con la extensión `.vbs` en el escritorio.

![](2.png)

Ejecutaremos el *script* y veremos que ha funcionado correctamente.

![](3.png)

Nos dirigiremos al directorio de salida que hemos indicado antes y veremos nuestro **OID** raíz. 

![](4.png)

## *DNI*

### Crear atributo

Crearemos un nuevo atributo usando la *Consola* donde hemos añadido el complemento **Esquema de AD**.

![](5.png)

Nos advertirá que esto es una operación permanente, pero continuaremos.

![](6.png)

Le daremos un nombre y pegaremos el **OID** raíz que hemos obtenido. Añadimos `.2.1` al final del identificador e indicaremos que es una *Cadena numérica* de 8 caracteres.

![](7.png)

Una vez lo hayamos creado, veremos sus propiedades.

![](8.png)

### Añadir a clase

Luego, añadiremos este nuevo atributo a la clase *users*.

![](9.png)

Seleccionamos el nuevo atributo y lo agregamos.

![](10.png)

Comprobamos que ahora es un atributo opcional.

![](11.png)

Reiniciaremos el **Servicio de dominio de Active Directory** para que se apliquen los cambios y evitar errores.

![](12.png)

### Editar *DNI* a un usuario

Ahora, tendremos que habilitar ver las características avanzadas en *Usuarios y equipos de AD* para poder ver todos los atributos que tienen los usuarios.

![](13.png)

Entramos en las propiedades de un usuario y editamos el nuevo atributo personalizado añadiendo su **DNI**.

![](14.png)

## *NIF*

### Crear atributo

Ahora crearemos un nuevo atributo llamado *NIF* de la misma manera, pero cambiando la terminación del identificador **OID**.

![](15.png)

### Añadir a clase

Editaremos de nuevo la clase *users*.

![](9.png)

Agregamos el nuevo atributo.

![](16.png)

Reiniciamos el **Servicio de dominio de AD** de nuevo...

![](12.png)

### Editar *NIF* a un usuario

En el editor de atributos de un usuario añadiremos un nuevo *NIF*.

![](17.png)

### Comando *PowerShell*

Por último, podemos utilizar este comando en **PowerShell** para mostrar el *NIF* del usuario.

```powershell
Get-ADUser usuario01 -Properties nIF | Where-Object {{$_.nIF -like "F"}}
```

![](18.png)
