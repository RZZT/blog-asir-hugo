---
title: Directivas de grupo GPO
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic1.makeuseofimages.com%2Fwordpress%2Fwp-content%2Fuploads%2F2016%2F06%2Fgroup-policy-editor.jpg&f=1&nofb=1&ipt=e6324b1a5c332ea894c95b1d4b27fd3f97715a8772c05df037ac1b0be5f013bc&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-28"
date: "2022-10-23 01:00:00+0200"
slug: /2022/10/uso-directivas-de-grupo-gpo
tags:
- Windows Server 2019
- Windows 10
- Directivas de Grupo
- GPO
---

## Panel de Control

Primero vamos a practicar con la **Herencia, Resolución y el Forzado de Conflictos** de *GPOs*. Crearemos esta estructura:

- **ASO1** *(Unidad Organizativa)*
  
  - *user1* *(Usuario)*
  
  - **ASO2** *(Unidad Organizativa)*
    
    - *user2* *(Usuario)*

![](1.png)

Abrimos el *Administrador de directivas de grupo* y creamos una nueva.

![](2.png)

Nombramos la *GPO*.

![](3.png)

Habilitamos la directiva para *Prohibir el acceso al Panel de control*.

![](4.png)

### Herencia

Luego, vincularemos una nueva *GPO* a la *Unidad Organizativa* que hemos creado antes.

![](5.png)

Seleccionamos la *GPO*.

![](6.png)

Veremos que se ha vinculado y su configuración se propaga a todo lo que esté debajo.

![](7.png)

Desde el *user1* comprobaremos que no tenemos acceso al *Panel de Control*.

![](8.png)

Desde el otro usuario, *user2* pasa lo mismo.

![](9.png)

### Bloquear Herencia

Ahora, seleccionaremos la *UO* **ASO2** y bloquearemos la herencia de las *GPOs* superiores.

![](10.png)

Veremos que aparece con una exclamación azul.

![](11.png)

Desde el *user2*, actualizaremos las *GPOs* y veremos que ahora sí que podemos acceder.

![](12.png)

### Exigir Aplicación

Después, vamos a forzar la aplicación de la *GPO* para que funcione obligatoriamente.

![](13.png)

Aceptamos cambiar la configuración.

![](14.png)

Observaremos que ahora aparece con un candado.

![](15.png)

Volveremos a comprobarlo con *user2* y verificaremos que en este caso sobrepasará el **bloqueo de herencia** hecho anteriormente, y se aplicará a *user2* aunque esté cortada la herencia en **ASO2**.

![](16.png)

### Resolución de conflictos

Quitamos las configuraciones anteriores: El **bloqueo de herencia** y el **forzado del enlace**. Vamos a crear otra para permitir el acceso al *Panel de Control*.

![](17.png)

Deshabilitamos la misma directiva que antes.

![](18.png)

Vinculamos esta nueva *GPO* con una configuración contraria a **ASO2**.

![](19.png)

Veremos la estructura.

![](20.png)

Como la última *GPO* prevalece, ahora sí que tenemos acceso al *Panel de Control*.

![](21.png)

Por último, podemos poner las dos *GPOs* en el mismo contenedor y jugar con la prioridad de ejecución.

![](22.png)

## Eliminar los botones de Inicio / Apagado

Primero crearemos una nueva *GPO* y la editaremos.

![](23.png)

Habilitaremos la configuración para *Quitar y evitar el acceso a los botones...*

![](24.png)

Vinculamos esta *GPO* a la *Unidad Organizativa* **ASO1**.

![](25.png)

Forzaremos la actualización de las directivas y veremos que no podemos apagar ni reiniciar el equipo mediante el *user1*.

![](26.png)

Desde el *user2* verificamos que ocurre lo mismo.

![](27.png)

## Mostrar mensaje de *login*

Luego, crearemos una nueva para mostrar un mensaje en el *login*.

![](28.png)

Configuraremos el texto que se va a mostrar.

![](29.png)

Vincularemos a **ASO1**.

![](30.png)

Para que esta configuración funcione, tendremos que mover los equipos a la *Unidad Organizativa* **ASO1**.

![](32.png)

Seleccionamos la *UO*.

![](33.png)

Forzaremos la actualización de las *GPOs* y reiniciamos el equipo. Ahora cuando entremos veremos este mensaje.

![](34.png)

Ocurre lo mismo desde el otro equipo.

![](35.png)

## Desactivar el *Firewall*

De nuevo, crearemos una nueva *GPO*...

![](36.png)

Primero deshabilitaremos el *firewall* a nivel de dominio.

![](37.png)

Después lo deshabilitaremos a nivel estándar.

![](38.png)

Vinculamos la *GPO*...

![](39.png)

Desde el *user1* veremos que se ha desactivado el *firewall* y no podemos cambiarlo.

![](40.png)

Con el *user2* pasa lo mismo.

![](41.png)

## Cambiar el Fondo de Escritorio

Para que los equipos puedan tener la misma imagen de fondo, crearemos una nueva carpeta en **SYSVOL**.

![](42.png)

Aquí, guardaremos la imagen de fondo.

![](43.png)

Antes de continuar, comprobaremos que los equipos pueden acceder a la carpeta compartida.

![](44.png)

Luego crearemos una nueva *GPO*.

![](45.png)

Indicaremos la ruta del nuevo fondo de pantalla.

![](46.png)

Vinculamos la nueva *GPO*.

![](47.png)

Forzaremos la actualización de las directivas y reiniciamos el equipo. Observaremos que ha cambiado el fondo.

![](48.png)

En el otro equipo con *user2* pasa lo mismo.

![](49.png)

## Página de inicio de *IE* personalizada

Creamos una nueva *GPO*...

![](50.png)

Indicamos la ruta de la página principal.

![](51.png)

Vinculamos la *GPO*...

![](52.png)

Actualizamos las directivas y ahora si ejecutamos *Internet Explorer* veremos nuestro **blog**. Por alguna razón, no carga el *CSS* y se ve bastante mal la página.

![](53.png)

Desde el *user2* pasará lo mismo.

![](54.png)

## Contraseñas y Bloqueo de Cuentas

Creamos una nueva *GPO*...

![](55.png)

Deshabilitamos los requisitos de complejidad y establecemos una longitud mínima de 2 caracteres.

![](56.png)

**¡Importante!** Vinculamos la *GPO* sobre el dominio `aso.org` y la ubicamos antes que la por defecto. 

![](57.png)

Actualizamos las directivas del *user2*...

![](58.png)

Desde el *DC02* restablecemos la contraseña al usuario.

![](59.png)

Escribimos una contraseña de **2 caracteres**.

![](60.png)

Comprobamos que nos permite poner esta contraseña *no segura*.

![](61.png)

Ahora intentaremos acceder en el equipo con el *user2*.

![](62.png)

Nos dejará acceder...

![](63.png)

## Bloquear Ejecución de Aplicaciones

Creamos una nueva *GPO*...

![](64.png)

Indicamos que no se pueda ejecutar el ejecutable del navegador *Firefox*.

![](65.png)

Vincularemos esta *GPO* a la *Unidad Organizativa*.

![](66.png)

Ahora si intentamos usar el navegador *Firefox* no podemos.

![](67.png)

## Bloquear Inicio de Sesión Local

Esta nueva *GPO* establece que solo pueden iniciar sesión usuarios que pertenezcan al grupo de **Administradores**.

![](68.png)

Buscamos el grupo de **Administradores**...

![](69.png)

La vinculamos a la *Unidad Organizativa*.

![](70.png)

Reiniciamos el equipo y ahora no podemos acceder con el *user1*...

![](71.png)

## Bloquear *Pendrives*

Esta *GPO* bloqueará el uso de almacenamiento externo.

![](72.png)

Habilitaremos la siguiente configuración.

![](73.png)

Vinculamos la *GPO* a la *Unidad Organizativa*.

![](74.png)

Para comprobarlo, conectaremos una unidad *USB* a la máquina cliente.

![](75.png)

Si intentamos entrar desde el explorador de archivos no podremos.

![](76.png)

## Mapear Unidades de Red

Por último, vamos a crear una *GPO* para mapear una unidad de red en los usuarios. Así que lo primero que haremos será crear una carpeta compartida en el servidor *DC02*, en mi caso voy a compartir la unidad **D:**

![](77.png)

Editamos la *GPO*...

![](78.png)

Luego, indicamos la ubicación de la carpeta compartida.

![](79.png)

Vinculamos la *GPO* a la *Unidad de Organizativa*.

![](80.png)

Ahora, veremos la unidad de red montada desde el explorador de archivos.

![](81.png)
