---
title: Repaso Batch
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fuser-images.githubusercontent.com%2F3974108%2F64225033-b438ea80-cea7-11e9-89f5-da5ed83b2814.png&f=1&nofb=1&ipt=7e6847c117599b18ce473e26aea267cc2d7bf2f0d6f7518fb2c3ad3c85d37f6f&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-19"
date: "2022-10-19 01:00:00+0200"
slug: /2022/10/repaso-batch
tags:
- Windows 10
- Script BAT
---

## Unidades de Red

Primero, crearemos una carpeta compartida en el propio cliente o podemos utilizar una ya existente en el servidor.

![](1.png)

### Montar

Ejecutaremos este *script* y veremos que se monta la carpeta compartida como unidad de red.

```batch
@echo off

color 0a
net use Z: \\W10-CLIENTE-2\Compartida /persistent:yes

@pause
```

![](2.png)

### Eliminar

Luego, ejecutaremos este otro *script* para desmontar la unidad de red **Z:**

```batch
@echo off

net use Z: /delete

@pause
```

![](3.png)

## Procesos

### Listar

Ahora usaremos este *script* para listar los procesos y ver su información.

```batch
@echo off

TASKLIST /FI "STATUS eq RUNNING" /FI "MEMUSAGE gt 15000"

@pause
```

![](4.png)

### Matar

Por último, usaremos este *script* para matar los procesos que consuman más memoria. Yo tenía muchos navegadores abiertos pero hay ciertos procesos que no se pueden eliminar debido a que son **vitales** para el sistema.

```batch
@echo off

taskkill /FI "MEMUSAGE GT 10000"

@pause
```

![](5.png)
