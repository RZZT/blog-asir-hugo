---
title: Actualizar a Windows Server 2019
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.extremetech.com%2Fwp-content%2Fuploads%2F2016%2F10%2FNothing-is-more-frustrating-than-having-a-time-consuming-update-take-even-more-time-to-uninstall.jpg&f=1&nofb=1&ipt=182008a21c3a470cdf5b0992a6947b3654b49d94fc8773b99d41129c40b9ae80&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-07"
date: "2022-10-09 01:00:00+0200"
slug: /2022/10/actualizar-de-server-2016-a-server-2019
tags:
- Windows Server 2016
- Windows Server 2019
---

## Preparación

Primero, insertaremos el disco de instalación de *Windows Server 2019* en la máquina **DC02**.

![](1.png)  

Iniciaremos el instalador desde el explorador de archivos de *Windows*.

![](2.png)

## Configuración

Aquí, seleccionamos la primera opción.

![](3.png)

Instalaremos las actualizaciones disponibles.

![](4.png)

Esperamos a que se instalen las actualizaciones...

![](5.png)

Introduciremos una clave de activación válida para *Windows Server 2019*.

![](6.png)

Seleccionamos el sistema operativo con interfaz gráfica.

![](7.png)

Aceptamos los términos de licencia.

![](8.png)

Queremos conservar los archivos y aplicaciones.

![](9.png)

Comenzaremos la instalación de *Windows Server 2019*.

![](10.png)

Tardará bastante...

![](11.png)

Finalmente, veremos que se ha actualizado.

![](12.png)
