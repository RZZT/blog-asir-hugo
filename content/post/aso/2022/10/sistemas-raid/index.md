---
title: Sistemas RAID
description: 
image: https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fs.glbimg.com%2Fpo%2Ftt%2Ff%2Foriginal%2F2012%2F10%2F29%2Fraid02.png&f=1&nofb=1&ipt=dcff20381fc933f3e9348606f7d48dc0561384f9bf487f263b28010e4dc295d8&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-20"
date: "2022-10-21 01:00:00+0200"
slug: /2022/10/sistemas-raid
tags:
- RAID
- Windows Server 2019 
---

## Preparación

Primero, añadiremos 3 nuevos discos duros con **10 GB** de capacidad cada uno, en el servidor *DC03*.

![](1.png)

## RAID 0

Ahora, iniciaremos los discos y crearemos un nuevo volumen distribuido.

![](2.png)

Seleccionaremos solo 2 discos.

![](3.png)

Le asignaremos la letra *D:*

![](4.png)

Lo nombramos.

![](5.png)

Veremos que se ha creado el disco.

![](6.png)

Se montará en el sistema.

![](7.png)

Crearemos una carpeta para hacer pruebas.

![](8.png)

Desconectamos un disco y nos da un **error**.

![](9.png)

Ya no aparecerá en el sistema porque no puede recuperar los datos.

![](10.png)

## RAID 1

Después, crearemos un volumen reflejado.

![](11.png)

Seleccionaremos los dos discos como antes...

![](12.png)

Veremos que se ha creado.

![](13.png)

También se montará en el sistema.

![](14.png)

Crearemos una nueva carpeta dentro.

![](15.png)

Desconectaremos un disco y no perderemos los datos, porque los datos están duplicados. Por lo que aún podemos seguir utilizando el *RAID* pero sin tenerlo reflejado.

![](16.png)

## RAID 5

Por último, crearemos un nuevo volumen *RAID 5*.

![](17.png)

Seleccionaremos los 3 discos...

![](18.png)

Esperaremos a que se cree el *RAID*...

![](19.png)

Observaremos que se ha montado en el sistema de ficheros.

![](20.png)

Crearemos una carpeta para hacer pruebas.

![](21.png)

Ahora, desconectaremos un disco y comprobaremos que aún funciona.

![](22.png)

Pero si desconectamos dos, ya no funcionará.

![](23.png)
