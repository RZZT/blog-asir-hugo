---
title: LDAP - Ubuntu
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages.hdqwalls.com%2Fwallpapers%2Fubuntu-linux-minimal-4k-on.jpg&f=1&nofb=1&ipt=03cdfdd2e7151aa6d723ef511547c7335a2a132f9c73bd63a60dcd6dff165628&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-02"
date: "2022-10-31 01:00:00+0200"
slug: /2022/10/ldap-ubuntu
tags:
- Linux
- Ubuntu Server
- LDAP
- Ubuntu
- Windows 10
- pGina
---

## Ubuntu Server

### Configuraciones de Red

Primero, instalaremos *Ubuntu Server* en una máquina virtual, y añadiremos un adaptador de red *Custom*.

![](0.png)

Detecta solo el adaptador *Bridged*.

![](1.png)

En mi caso me conectaré, por *SSH* desde la **Terminal** de mi máquina física para la configuración.

![](2.png)

Editaremos el archivo `/etc/netplan/00-installer-config.yaml`. Aplicamos los cambios con el comando `netplan apply`.

![](3.png)

### Configuración LDAP

Ahora, instalaremos los paquetes `sldapd` y `ldap-utils`.

![](4.png)

Introducimos una contraseña para el administrador.

![](5.png)

Confirmamos la contraseña...

![](6.png)

Editaremos el archivo `/etc/ldap/ldap.conf`.

![](7.png)

Indicaremos el dominio y la dirección del servidor **LDAP**.

![](8.png)

Ejecutaremos el siguiente comando para configurar de nuevo.

![](9.png)

No omitiremos la configuración...

![](10.png)

Indicamos el nombre de dominio **DNS**. 

![](11.png)

El nombre de la organización...

![](12.png)

Indicamos la contraseña del administrador...

![](13.png)

Otra vez...

![](14.png)

Indicaremos que sí queremos purgar la base de datos si borramos el paquete, aunque no va a ser el caso...

![](15.png)

Moveremos la base de datos antigua...

![](16.png)

### Panel de Administración Web

Instalaremos los paquetes necesarios para el servidor de administración *web* **LAM** *(LDAP Account Manager)*.

![](17.png)

Habilitamos el sitio en *Apache* y reiniciamos el servicio.

![](18.png)

Instalamos el paquete `ldap-account-manager`.

![](19.png)

Configuraremos el archivo `lam.conf`.

![](25.png)

Indicamos el nombre del dominio y las credenciales del usuario `admin`.

![](26.png)

Editaremos el nombre de los grupos.

![](27.png)

### Interfaz Gráfica

Instalaremos `taskel` para configurar el sistema de forma sencilla.

![](20.png)

Ejecutamos la aplicación.

![](21.png)

Seleccionamos el entorno de escritorio de **Debian**.

![](22.png)

Esperamos a que se descargue e instale...

![](23.png)

Después de un buen rato, tendremos una interfaz gráfica en el servidor.

![](24.png)

Accederemos al panel de **LAM** desde el navegador *web*.

![](28.png)

### Grupos y Usuarios

Crearemos los grupos que hemos editado antes en el archivo de configuración.

![](29.png)

![](30.png)

Ahora, crearemos un nuevo grupo.

![](31.png)

Lo llamaremos `administradores` y le asignaremos el **GID** `10000`.

![](32.png)

![](33.png)

Luego, crearemos un nuevo usuario.

![](34.png)

Le damos un nombre y apellido.

![](35.png)

Establecemos una contraseña para el usuario...

![](36.png)

Creamos este usuario para *Unix* también, y le establecemos una contraseña otra vez.

![](37.png)

> El **nombre de usuario** que aparece aquí será el nombre con el que tendremos que acceder en el cliente.

![](38.png)

Veremos que se ha creado el usuario.

![](39.png)

## Ubuntu Desktop

### Configuraciones de Red

Crearemos una nueva máquina virtual *Ubuntu Desktop 20.04* y le añadiremos el adaptador *Custom* para que tenga conexión con el servidor.

> Yo lo intenté con la versión **Ubuntu Desktop 20.10** y no me funcionó la conexión con el servidor **LDAP**.

![](40.png)

Veremos las dos interfaces en los ajustes del cliente.

![](41.png)

Estableceremos una configuración *IP* manual.

![](42.png)

Observamos la configuración de red del cliente.

![](43.png)

Verificamos que hay conexión con el servidor. **¡Importante!**

![](44.png)

### Configuración LDAP

Instalaremos los siguientes paquetes para configurar la conexión a **LDAP**.

![](45.png)

En esta ventana, indicaremos la dirección *IP* del servidor **LDAP**.

> Hay que eliminar la letra `i` y una barra `/`.

![](46.png)

Yo no eliminé la barra y lo tuve que solucionar editando el archivo `/etc/ldap.conf` **:'D**

![](77.png)

Indicamos el nombre de dominio...

![](47.png)

Seleccionamos la versión 3...

![](48.png)

Hacemos la contraseña del usuario `admin` local...

![](49.png)

No necesitamos `login` para acceder a la base de datos **LDAP**.

![](50.png)

Indicamos el nombre del usuario `root`...

![](51.png)

Su contraseña...

![](52.png)

Luego, instalamos el paquete `slapd` pero esta vez en el cliente...

![](53.png)

Indicamos la contraseña del **Administrador**...

![](54.png)

La escribimos otra vez...

![](55.png)

Configuraremos de nuevo el **LDAP**...

![](56.png)

No omitiremos la configuración…

![](57.png)

Indicamos el nombre de dominio **DNS**.

![](58.png)

El nombre de la organización…

![](59.png)

Indicamos la contraseña del administrador…

![](60.png)

Otra vez…

![](61.png)

Indicaremos que sí queremos purgar la base de datos si borramos el paquete, aunque no va a ser el caso…

![](62.png)

Moveremos la base de datos antigua…

![](63.png)

Editaremos el archivo `/etc/ldap/ldap.conf`.

![](64.png)

Luego, editamos el archivo `/etc/nssswitch.conf` y añadimos `ldap`.

![](65.png)

Actualizamos la base de datos de la configuración...

![](66.png)

Después editamos el archivo `/usr/share/pam-configs/mkhomedir`.

![](67.png)

En el archivo `/etc/pam.d/common-session` añadiremos la siguiente línea.

![](68.png)

Aplicamos la configuración con este comando.

![](69.png)

Habilitaremos la `Creación del directorio del usuario al iniciar`.

![](70.png)

Reiniciaremos el equipo.

### Conexión **CLI**

En el servidor, crearemos otro usuario llamado `pepe`.

![](71.png)

En el cliente, cambiaremos de `TTY` con las teclas **CTRL + ALT + F2** y entraremos con el usuario `pepe`.

![](72.png)

Para volver a la interfaz gráfica pulsaremos las teclas **CTRL + ALT + F1**. En el *login*, veremos los dos usuarios, y ahora podemos acceder con `pepe`.

![](73.png)

### Conexión **GUI**

Para poder iniciar con la interfaz gráfica, instalaremos el paquete `nslcd`:

```bash
sudo apt install nslcd
```

Indicamos la dirección `URI` del servidor **LDAP**.

![](74.png)

Escribimos el nombre de dominio...

![](75.png)

Comprobamos que podemos cambiar de usuario desde la terminal también...

![](76.png)

Ahora entraremos desde el otro usuario.

![](78.png)

Escribimos su contraseña.

![](79.png)

Veremos que hemos entrado.

![](80.png)

## Windows 10

### Configuraciones de Red

Crearemos una nueva máquina virtual **Windows 10** y le añadiremos un adaptador *Custom* para que se pueda conectar con el servidor.

![](81.png)

Configuraremos la dirección **IP** estática en el adaptador **LAN**.

![](82.png)

Veremos las configuraciones de los adaptadores.

![](83.png)

Comprobaremos que tenemos conexión con el servidor. **¡Importante!**

![](84.png)

### Instalar *pGina*

Después, descargaremos *pGina* de su página *web* oficial.

![](85.png)

Ejecutamos el instalador y continuamos con el asistente.

![](86.png)

Aceptamos la licencia...

![](87.png)

Cuando terminemos de instalarlo, lo lanzaremos.

![](88.png)

### Configuración de pGina

En *pGina*, habilitamos los *plugins* de *LDAP* y **aplicamos los cambios.** Luego, pulsamos en el botón `Configure`

![](89.png)
Indicamos los parámetros para la configuración con el servidor **LDAP**.

![](90.png)

Guardaremos la configuración y **aplicamos los cambios**. Comprobaremos que funciona todo haciendo una simulación con el usuario `pepe`.

![](91.png)

### Conexión desde *pGina*

Por último, cerramos sesión y accedemos desde *pGina* con el usuario `pepe`.

![](92.png)

Accederemos con este usuario de **LDAP** a *Windows 10*.

![](93.png)
