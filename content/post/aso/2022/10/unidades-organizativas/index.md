---
title: Unidades Organizativas
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpaperaccess.com%2Ffull%2F766757.jpg&f=1&nofb=1&ipt=10f50a00569a8f82232fcc07323e07aa5b8b49acb46504f4477650ea2929b874&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-10-05"
date: "2022-10-05 01:00:00+0200"
slug: /2022/10/unidades-organizativas
tags:
- Windows Server 2019
- Windows 10
- Unidades Organizativas
- Active Directory 
---

## Configuración

Primero, crearemos una *Unidad Organizativa* raíz llamada **ASO**. Dentro crearemos 4 *Unidades Organizativas* llamadas como el departamento. Además, crearemos un grupo de informáticos en el contenedor *Users*.

![](1.png)

### Admon

En cada *Unidad Organizativa* crearemos un grupo global de seguridad con dos usuarios. Uno de mañana y otro de tarde.

![](2.png)

### Diseño

Haremos lo mismo en las demás *Unidades Organizativas*.

![](3.png)

### Fabricación

![](4.png)

### Recursos Humanos

![](5.png)

## Delegar control

Ahora delegaremos el control de las *Unidades Organizativas* usando el asistente.

![](6.png)

Seleccionamos el **grupo** de informáticos que hemos creado antes.

![](7.png)

Delegaremos todas las tareas posibles.

![](8.png)

Finalizamos el asistente.

![](9.png)

> Haremos este proceso en cada *Unidad Organizativa*.

## Comprobaciones

Ahora intentamos iniciar sesión con un usuario de mañana, por la tarde.

![](10.png)

No tendremos permitido acceder.

![](11.png)

Después, intentaremos acceder con un usuario de tarde.

![](12.png)

Con este sí que podremos iniciar sesión porque estamos haciendo las pruebas por la tarde.

![](13.png)

## Eliminar *Unidad Organizativa*

Por último, crearemos una nueva para comprobar si la podemos eliminar. Nos fijaremos que al crearla, se habilita la protección contra la eliminación.

![](14.png)

Ahora la intentaremos eliminar...

![](15.png)

Nos aparecerá esta advertencia donde confirmaremos que queremos eliminar la *Unidad Organizativa*.

![](16.png)

No nos permite eliminar la *Unidad Organizativa* porque está protegida.

![](17.png)

Para poder quitar la protección tendremos que habilitar ver las **Características avanzadas**.

![](18.png)

De esta manera podremos quitar la protección del objeto y eliminarla sin problemas.

![](19.png)
