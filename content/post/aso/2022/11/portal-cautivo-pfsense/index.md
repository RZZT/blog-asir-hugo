---
title: Portal Cautivo PfSense
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.nettix.com.pe%2Fwp-content%2Fuploads%2F2020%2F09%2Fportal-cautivo.jpg&f=1&nofb=1&ipt=21f2364fa449dcbda37188752945a47adea2fcf1a5436e7f1426340280a00d79&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-23"
date: "2022-11-22 01:00:00+0200"
slug: /2022/11/portal-cautivo-pfsense
tags:
- Portal Cautivo
- PfSense
- Redes
- Enrutamiento
- DHCP
- DNS
- Windows 10
---

## DNS

Primero, accedemos al panel de administración de *PfSense* desde un equipo **Windows 10 cliente** con el usuario `admin`.

![](1.png)

Luego, activaremos el servicio de resolución **DNS**.

![](2.png)

Añadimos un nuevo `host` para anularlo.

![](3.png)

Completamos con los datos de el router.

![](4.png)

A partir de ahora tendremos que guardar y aplicar los cambios para cada configuración que hagamos.

![](5.png)

Aparecerá que se han aplicado los cambios correctamente.

![](6.png)

## DHCP

Ahora activaremos el servicio **DHCP** para que otorgue direcciones **IP** dinámicamente con el rango que le indiquemos.

![](7.png)

Indicamos la dirección **IP** de *PfSense* como servidor **DNS** y **puerta de enlace**.

![](8.png)

## Firewall

Después, añadiremos unas reglas de **cortafuegos** en la interfaz `LAN`.

![](9.png)

La primera, permitirá el paso por la interfaz `LAN` de cualquier dirección **IP** mediante cualquier protocolo.

![](10.png)

Indicamos que su origen es cualquiera y el destino es este cortafuegos.

![](11.png)

Guardaremos la regla y veremos que se ha añadido. Crearemos otra debajo.

![](12.png)

Esta nueva regla, **bloquerá** las conexiones.

![](13.png)

![](14.png)

Guardaremos y este será el resultado. 

![](15.png)

> En las reglas de **firewall** es muy **importante** el orden.

## Configuraciones de red

Comprobamos que los servicios **DHCP y DNS** están funcionando.

![](16.png)

En el cliente, solicitaremos una nueva configuración de red.

![](17.png)

Nos aseguramos que el servidor **DNS** de *PfSense* son los de **Google**.

![](19.png)

## Portal Cautivo

Ahora, nos dirigimos a la configuración del **Portal Cautivo**.

![](20.png)

Añadimos una nueva zona.

![](21.png)

Le damos un nombre y una descripción.

![](22.png)

Habilitamos el servicio de **Portal Cautivo** por la interfaz **LAN**. A los 10 minutos de inactividad desconectará al cliente.

![](23.png)

Indicamos que después de iniciar sesión redireccione a los usuarios a este blog.

![](24.png)

Subiremos unas imágenes para el logo y el fondo  de la pantalla de **login**.

![](25.png)

Seleccionamos el método de autenticación **backend** con una base de datos local. Solo permitiremos el acceso a usuarios y grupos con el privilegio de **Portal cautivo** establecido.

![](26.png)

Habilitamos el login **HTTPS** con el siguiente servidor de dominio.

![](27.png)

## Usuarios y Grupos

Ahora, crearemos un nuevo grupo.

![](28.png)

No añadiremos ningún usuario de momento y asignaremos unos privilegios.

![](30.png)

Seleccionamos el privilegios para que los usuarios puedan acceder al **Portal cautivo**.

![](31.png)

Guardamos el grupo.

![](32.png)

Observamos que se ha creado.

![](33.png)

Luego, crearemos un nuevo usuario.

![](34.png)

Añadimos este usuario al grupo que hemos creado antes.

![](35.png)

Veremos que se ha creado, y lo editamos.

![](36.png)

Ahora aparecerán los permisos que ha heredado del grupo.

![](37.png)

Comprobamos que el servicio está activado.

![](38.png)

## Comprobaciones

Si hacemos cualquier consulta en el navegador nos obligará a iniciar sesión.

![](39.png)

Accedemos con el usuario que hemos creado.

![](40.png)

Nos redirigirá a este blog. Tal como hemos indicado en la configuración.

![](41.png)

Si volvemos hacia atrás, nos permite desconectarnos también.

![](42.jpg)

Si intentamos acceder con unas credenciales incorrectas no nos dejará navegar.

![](43.jpg)

En el panel de control podemos acceder a las sesiones que hay activas.

![](44.png)

Veremos que solo hay una, y la podemos eliminar. Se desconectaría al usuario y tendría que volver a iniciar sesión.

![](45.png)

Además, también podemos ver el registro de lo que ha pasado en el **Portal cautivo**. Se puede ver el inicio de sesión, y la desconexión.

![](46.png)

Para comprobar que nos cierra sesión automáticamente, he cambiado el tiempo de ausente y *timeout* a 1 minuto.

![](47.png)

Si ahora iniciamos sesión otra vez y esperamos un minuto, nos volverá a pedir que iniciemos sesión de nuevo para continuar.

![](48.png)
