---
title: Autenticación LDAP Dominio en PfSense
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.veracode.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fblog_post_resize_960%2Fpublic%2Fshutterstock_178664090_1.png%3Fitok%3D7IbmyPQA&f=1&nofb=1&ipt=3231f549d047f65f987eb162ca8b80495eb0cbfaeacd602025329ab3f3d4cea1&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-24"
date: "2022-11-23 01:00:00+0200"
slug: /2022/11/autenticacion-ldap-dominio-pfsense
tags:
- Redes
- PfSense
- Active Directory
- LDAP 
---

## Firewall

Para permitir la conexión con *PfSense* mediante **LDAP**, crearemos una nueva regla de entrada en el servidor *Windows Server*.

![](1.png)

Esta regla permitirá un puerto.

![](2.png)

Indicamos el puerto **389/TCP** (LDAP).

![](3.png)

Aplicaremos esta regla para todos los casos.

![](4.png)

La nombramos y le damos una descripción.

![](5.png)

## Usuarios y Grupos de Active Directory

Primero, crearemos un nuevo grupo llamado `gpfsense` en el contenedor *Users* de nuestro dominio.

![](6.png)

Luego, crearemos un nuevo usuario llamado `wpfsense` en el mismo contenedor.

![](7.png)

Le asignamos una contraseña...

![](8.png)

Después de crearlo, lo añadiremos al grupo que hemos creado antes.

![](9.png)

## PfSense

### Configuración del servidor de autenticación

Desde el panel de control de *PfSense*, añadiremos otro servidor de autenticación.

![](10.png)

Este servidor será el **Active Directory**, indicaremos su dirección **IP** y el nombre de dominio.

![](11.png)

Indicamos los siguientes parámetros para continuar.

![](12.png)

Luego, en los ajustes, indicamos que el servidor de autenticación es el que acabamos de crear. (Active Directory)

![](13.png)

Comprobamos que todo funciona bien.

![](14.png)

### Crear grupo

Ahora crearemos un nuevo grupo en *PfSense*.

![](15.png)

Lo llamaremos igual que el grupo que hemos creado en **Active Directory**.

![](16.png)

Lo crearemos y ahora lo editamos.

![](17.png)

Le asignamos permisos para que pueda acceder al panel de configuración *web*.

![](18.png)

Comprobaremos que el usuario *wpfsense*, creado en *Active Directory* se puede autenticar.

![](19.png)

### Portal Cautivo

En la configuración del *Portal Cautivo*, cambiaremos el modo de autenticación.

![](20.png)

## Comprobaciones

Desde un cliente *Windows 10* nos identificaremos con el usuario de **Active Directory** en el *Portal Cautivo*.

![](21.png)

Funciona sin problemas.

![](22.png)

Luego, accederemos al panel de control de **PfSense** con el usuario del dominio.

![](23.png)

Observaremos que hemos accedido con el usuario `wpfsense`.

![](24.png)

Por último, veremos que el usuario `wpfsense` tiene una sesión iniciada en el portal cautivo.

![](25.png)
