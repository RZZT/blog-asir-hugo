---
title: OwnCloud, LDAP y Guacamole
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fs17026.pcdn.co%2Fwp-content%2Fuploads%2Fsites%2F9%2F2017%2F04%2FCloud-storage-13417.jpeg&f=1&nofb=1&ipt=079016d181bfb1d9a82e0146db1264e7e27bf3e936032cd30242c35f5dd251c4&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-09"
date: "2022-11-07 01:00:00+0200"
slug: /2022/11/owncloud-ldap-guacamole
tags:
- OwnCloud
- LDAP
- Guacamole
- Active Directory
---

## OwnCloud

### Descarga

Primero, descargaremos la imágen que funciona desde [Google Drive](https://drive.google.com/file/d/1kSJVJr6UJxg8u5IpQcQr-iIpF21RkWha/view?usp=share_link). 

Descomprimimos el archivo en nuestro directorio para las máquinas virtuales.

![](2.png)

Añadimos el adaptador de red *Custom* para que se pueda conectar con nuestra red interna.

![](3.png)

### Instalación

Iniciamos la máquina y estableceremos la ubicación en **Madrid**.

![](4.png)

Ajustamos las configuraciones de red.

![](5.png)

Luego, indicaremos que nos queremos unir a un dominio de **Active Directory** ya existente.

![](6.png)

Configuramos el servidor al que nos queremos unir, el usuario **Administrador** y su contraseña.

![](7.png)

Después indicaremos el nombre del host y su contraseña local.

![](8.png)

Confirmaremos las configuraciones y dejamos que instale...

![](9.png)

Esperamos a que se instale...

![](10.png)

Después de **un buen rato** terminará la instalación.

![](11.png)

Se reiniciará el servidor, y aparecerá esta ventana donde se indica que nos tenemos que conectar mediante la página *web*.

![](12.png)

En el servidor, veremos que se ha añadido el equipo en el contenedor *Computers*.

![](13.png)

### Configuración inicial

Nos conectaremos por la *web*. Introduciremos un correo electrónico para que nos mande la licencia de actividad.

![](14.png)

Cuando nos llegue la licencia al correo, la subiremos para activarlo.

![](15.png)

Observamos que hemos activado *OwnCloud*.

![](16.png)

En este panel, entraremos en la aplicación de *OwnCloud*.

![](17.png)

Accederemos con el usuario `owncloud` y contraseña `owncloud`.

![](18.png)

### Usuarios y Grupos

Lo primero que veremos será los archivos y carpetas que hay en la nube, entraremos en los ajustes.

![](19.png)

Antes de continuar, crearemos un nuevo usuario en nuestro **Active Directory**.

![](20.png)

Indicaremos el usuario en la autenticación de **LDAP**.

![](21.png)

Seleccionaremos el tipo objeto y los grupos.

![](22.png)

Estableceremos esta configuración de atributos de acceso.

![](23.png)

Por último, los grupos.

![](24.png)

Cerraremos sesión, e intentaremos acceder con el usuario que acabamos de crear.

![](25.png)

Accederemos sin problema.

![](26.png)

Volveremos a entrar con el usuario `owncloud` y comprobaremos que se ha creado el usuario del dominio aquí.

![](27.png)

### Almacenamiento Externo con SMB

Ahora, habilitaremos el almacenamiento externo desde el usuario `owncloud`.

![](28.png)

Usaremos la carpeta compartida que usamos en esta [práctica](https://asir.raulsanchezzt.com/2022/10/uso-directivas-de-grupo-gpo/#mapear-unidades-de-red).

![](29.png)

En el servidor, indicaremos la ruta de esta carpeta.

![](30.png)

Observamos que ahora aparece en nuestro espacio de trabajo con el usuario `raul`.

![](31.png)

Si entramos veremos los archivos compartidos que hay en la carpeta del servidor **DC02**.

![](32.png)

## Guacamole

### Instalación

Ahora, accederemos a los ajustes del sistema.

![](33.png)

Usamos las credenciales del **Administrador** del dominio para acceder.

![](34.png)

Buscamos la aplicación y la instalamos. 

![](35.png)

Esperamos a que se instale...

![](36.png)

### Activar Usuarios

Una vez se haya instalado entramos en la configuración de usuarios.

![](37.png)

Seleccionamos al usuario **Administrador**.

![](38.png)

Activamos la aplicación *Guacamole*.

![](39.png)

### Configuración LDAP

Luego, entraremos en la configuración del dominio **LDAP**.

![](40.png)

Añadiremos un nuevo contenedor en el directorio `guacamole`.

![](41.png)

Seleccionamos la configuración de *Guacamole*.

![](42.png)

Añadimos el usuario *Administrador*.

![](43.png)

Indicamos los siguientes parámetros para que nos permita acceder al servidor **DC01**.

![](44.png)

Nos aseguramos que en el servidor de destino, está habilitado el **Escritorio Remoto**.

![](45.png)

### Comprobación Conexión

Ahora, nos conectaremos con el usuario **Administrador**.

![](46.png)

Por último, accederemos y veremos el *login* con el usuario **Administrador**.

![](47.png)
