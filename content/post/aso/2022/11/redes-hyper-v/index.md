---
title: Redes Hyper-V
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages.idgesg.net%2Fimages%2Farticle%2F2019%2F08%2Fnw_cso_wide_area_network_connections_across_smart_city_by_metamorworks_gettyimages-858526756_2400x1600-100807751-large.jpg&f=1&nofb=1&ipt=cce9598bf70cdfdb56fa7e5e9d7337d7ab055d2f9e6cffb5580550ed41fb1920&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-30"
date: "2022-11-30 01:00:00+0200"
slug: /2022/11/redes-hyper-v
tags:
- Hyper-V
- Redes
- Windows Server 2019
- DHCP
- Enrutamiento
- Windows 10
---

## Preparaciones

Para hacer esta práctica usaremos la misma infraestructura que hemos usado en la [práctica anterior](https://asir.raulsanchezzt.com/2022/11/hyper-v/).

- Windows Server 2019 - **DC02**:
  
  - DHCP
  
  - Enrutamiento

- Windows Server 2019 - **NEWDC01**:
  
  - Hyper - V: *(Host físico)*
    
    - Windows 10 H1
    
    - Windows 10 H2

Antes de comenzar nos aseguramos que el servicio **DHCP** de **DC02** está bien configurado.

![](1.png)

También comprobamos que el enrutamiento está funcionando.

![](2.png)

## Tipos de conmutadores virtuales

### Red externa

#### Compartir adaptador

Accedemos al *Administrador de conmutadores virtuales* de **Hyper-V**.

![](3.png)

Creamos un nuevo conmutador virtual si no hay ninguno, con un tipo de conexión **red externa** y en este caso permitiremos que el **host físico** comparta el adaptador de red.

![](4.png)

Una vez hayamos configurado el adaptador, se lo asignaremos como adaptador de red a las dos máquinas.

![](5.png)

Por el momento funciona todo, las máquinas obtienen una dirección **IP** válida del **DHCP**.

![](6.png)

Además, el **host físico** ha creado otro adaptador de red que ha copiado la configuración del adaptador original.

![](7.png)

Desde las máquinas virtuales, comprobaremos que pueden hacer `ping` entre ellas y al **host físico (NEWDC01)** y **DC02**.

![](8.png)

Verificaremos que las máquinas virtuales tienen conexión con *Internet*.

![](19.png)

Desde el **host físico**, comprobaremos que tiene conexión con ambas máquinas y a *Internet*.

![](18.png)

#### Sin compartir adaptador

Editamos el conmutador virtual para **no permitir que el sistema operativo de administración comparta este adaptador de red**.

![](9.png)

No hace falta que editemos el adaptador de las máquinas porque es el mismo. Solo tenemos que esperar unos segundos.

Con este adaptador, lo que conseguimos es quitar la conexión con el **host físico**.

![](10.png)

Observaremos que ha desaparecido el adaptador de red en el **host físico**.

![](11.png)

Comprobaremos que las máquinas siguen teniendo conexión a *Internet*, mientras el **host físico** no tiene ningún adaptador de **red**.

![](20.png)

### Red Interna

Cambiamos el mismo adaptador de **Red interna**.

![](12.png)

Lo que conseguimos con este tipo de conmutador, es crear una red **interna** de las máquinas virtuales con el **host físico**. Para hacer las pruebas, tenemos que configurar manualmente las direcciones **IP**, porque no tiene conexión con el **DC02**. *(También podríamos crear un servidor DHCP dentro de la red interna)*.

Desde las máquinas virtuales comprobaremos que tenemos conexión entre ellas y al **host físico**.

![](13.png)

Desde el **host físico** comprobaremos que también tenemos conexión con las máquinas virtuales.

![](14.png)

### Red Privada

Por último, utilizaremos la **red privada**.

![](15.png)

En este caso, crearemos una red totalmente **aislada** del **host físico** y la red **física**.

Desde las máquinas virtuales, comprobaremos que seguimos teniendo conexión entre ellas pero no con el **host físico**.

![](16.png)

Observaremos que en el **host físico**, el adaptador de red ha **desaparecido**.

![](17.png)
