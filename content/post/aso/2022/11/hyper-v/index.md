---
title: Hyper-V
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fchrisblanche.files.wordpress.com%2F2019%2F06%2Fhyperv-hero-image.png%3Fw%3D2800&f=1&nofb=1&ipt=0bdedcbaf8748a606343aee6dd305ea4c917c4ba572aeb3f21f88cf4c63c1f0a&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-17"
date: "2022-11-16 01:00:00+0200"
slug: /2022/11/hyper-v
tags:
- Windows Server 2019
- Enrutamiento
- Redes
- Windows 10
- Hyper-V
- RAID 
---

## Preparaciones

### Características de Windows

Primero, nos aseguraremos que tenemos estas características desactivadas en nuestro equipo **Windows 10 local** para evitar problemas con la virtualización después.

![](1.png)  

Utilizaremos **Hyper-V** en el servidor que **NO** estén los roles **FSMO**, en mi caso es el **NEWDC01**. Configuraremos esta máquina virtual con **4** núcleos del procesador, **8 GB** de memoria **RAM** y **3 discos** de **20 GB**.

![](2.png)

### Redes y Enrutamiento

Encendemos el servidor **NEWDC01** e indicamos el servidor **DC02** (que tiene todos los roles **FSMO**) como **puerta de enlace**.

![](3.png)

Nos aseguramos que el servidor **DC02** tiene las 2 interfaces de red configuradas para enrutar paquetes como hicimos en esta [práctica](https://asir.raulsanchezzt.com/2022/09/configurar-dhcp-enrutamiento-server-2016/#enrutamiento).

![](4.png)

### RAID-5

Ahora, entraremos en el **Administrador de Discos** de **NEWDC01** e inicializamos los discos con *GPT*.

![](5.png)

Haciendo clic derecho sobre un disco, crearemos un nuevo volumen **RAID-5**.

![](6.png)

Seleccionamos los **3** discos que van a formar parte del **RAID**.

![](7.png)

Finalizamos el asistente.

![](8.png)

Veremos que ha creado el **RAID** correctamente.

![](9.png)

## Hyper-V

### Instalación

Instalamos el rol **Hyper-V** en el servidor **NEWDC01**.

![](10.png)

Seleccionamos la única interfaz de red que tenemos.

![](11.png)

Dejamos esta opción por defecto.

![](12.png)

También dejamos estas ubicaciones por defecto.

![](13.png)

Esperaremos a que se instale...

![](14.png)

### Crear Máquina Virtual

Una vez lo hayamos instalado, entraremos en el **Administrador de Hyper-V** y crearemos una nueva máquina virtual.

![](15.png)

Primero especificamos un nombre y la ubicación la unidad **RAID** que hemos creado antes.

![](17.png)

> **Hyper-V** creará una carpeta dentro de la unidad para esta máquina virtual.

Seleccionamos que vamos a usar la primera generación.

![](18.png)

Le asignaremos **2 GB** de memoria **RAM**.

![](19.png)

Establecemos el adaptador de red que usará.

![](20.png)

Indicamos que cree un disco virtual en la carpeta de antes.

![](21.png)

Luego, seleccionaremos la imagen `.iso` de **Windows 10**.

![](22.png)

Finalizaremos el asistente.

![](23.png)

Ahora, iniciaremos la máquina y nos conectamos.

![](24.png)

Instalamos **Windows 10**...

![](25.png)

Cuando se haya instalado, veremos que el servidor **DHCP** nos asigna una dirección **IP**.

![](26.png)

### Clonar Máquina

Primero, apagamos la máquina y la **exportamos**.

![](27.png)

Indicamos la carpeta de **Descargas** como destino.

![](28.png)

Esperamos a que se exporte.

![](29.png)

Cuando termine, **importaremos** la máquina virtual.

![](30.png)

Indicamos la carpeta de la máquina exportada.

![](31.png)

Seleccionamos la máquina que queremos importar.

![](32.png)

Seleccionamos que queremos crear una nueva máquina virtual con un nuevo **identificador único** nuevo.

![](33.png)

Seleccionamos la unidad **RAID** como destino para la nueva máquina.

![](34.png)

Dejamos esta opción por defecto.

![](35.png)

Esperamos a que se termine de importar...

![](36.png)

Cambiamos el nombre a la nueva máquina.

![](37.png)
