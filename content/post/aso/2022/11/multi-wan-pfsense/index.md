---
title: Multi-WAN PfSense
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fwp4056221.jpg&f=1&nofb=1&ipt=3804f7be9cb836d4ea1575f7203f7afd65253d571e47fd87e0687036a8dd7164&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-11-25"
date: "2022-11-24 01:00:00+0200"
slug: /2022/11/multi-wan-pfsense
tags:
- PfSense
- Redes 
---

## Configuraciones previas

Primero, añadiremos otra interfaz de red a la máquina virtual de *PfSense*. En mi caso he añadido una **Bridged** pero cualquier interfaz **WAN** es válida.

![](1.png)

Luego, desde el panel de control, activaremos el balanceo de carga.

![](2.png)

También activaremos el monitoreo de la puerta de enlace.

![](3.png)

## Configuraciones de Interfaces

Después, veremos que se ha añadido la nueva interfaz. La editaremos ahora.

![](4.png)

Habilitamos esta interfaz, le daremos el nombre `WAN2` e indicaremos que va a utilizar la configuración del **DHCP**.

![](5.png)

Ahora, veremos las puertas de enlace que tiene configuradas este *router*.

![](6.png)

### Grupo de puerta de enlace

Crearemos un nuevo grupo.

![](7.png)

Indicamos un nombre y las prioridades de las puertas de enlace.

![](8.png)

Observamos que se ha creado el grupo.

![](9.png)

Ahora, seleccionaremos el nuevo grupo de puerta de enlace como predeterminada.

![](10.png)

Verificamos que todo funciona bien desde el panel de control.

![](11.png)

### Cortafuegos

Después, editaremos la regla del cortafuegos de la interfaz **LAN**.

![](12.png)

En las **opciones avanzadas**, indicaremos la puerta de enlace en grupo que hemos creado antes.

![](13.png)

Veremos que ahora aparece.

![](14.png)

## Comprobaciones

Añadimos el *widget* de las puertas de enlace al panel de control.

![](15.png)

Si desconectamos una interfaz de red, veremos que aparece en color rojo la **pérdida de paquetes**.

![](16.png)

En cambio, si una interfaz está muy saturada, veremos una **advertencia** sobre la **pérdida de paquetes.**

![](17.png)
