---
title: Asistencia Rápida
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcomptech.ca%2Fcomptech%2Fwp-content%2Fuploads%2F2015%2F09%2Fwindows-support.jpg&f=1&nofb=1&ipt=066031e7c4558bf282234811ad88d491f499414e2590ab679410f7a01876dc2b&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-12-14"
date: "2022-12-13 01:00:00+0200"
slug: /2022/12/asistencia-rapida
tags:
- Asistencia Rápida
- Windows 10
- NoMachine 
---

## Asistencia Rápida de Windows

Primero, buscaremos la aplicación de asistencia rápida en una máquina Windows 10.

![](1.png)

Iremos a la tienda de *Microsoft* para actualizar la aplicación a la versión más reciente.

![](2.png)

Instalamos la nueva versión.

![](3.png)

Seleccionaremos *ayudar a otra persona*.

![](4.png)

Iniciaremos sesión con una cuenta de *Microsoft* y nos dará un código de seguridad...

![](5.png)

En otra máquina **Windows** introduciremos el código que nos ha dado.

![](6.png)

Ahora, seleccionaremos que tome el control por completo.

![](7.png)

Permitimos el acceso para continuar.

![](8.png)

Nos conectaremos sin problemas.

![](9.png)

## No Machine

### Instalación

Ahora instalaremos *No Machine* desde su página web oficial.

![](10.png)

> Lo instalaremos en dos máquinas virtuales Windows 10.

![](11.png)

Aceptaremos el acuerdo de licencia.

![](12.png)

Dejamos la ruta por defecto...

![](13.png)

Finalizamos el asistente.

![](14.png)

### Uso

Una vez instalado, lo abriremos y veremos nuestra dirección para conectarnos.

![](15.png)

Permitiremos el acceso a invitado.

![](16.png)

Observaremos que aparece la otra máquina.

![](17.png)

Haremos doble clic para conectarnos...

![](18.png)

Nos advertirá de la autenticidad del *host*, pero continuamos...

![](19.png)

Seleccionaremos **Solicitar acceso como invitado**.

![](20.png)

Dejaremos el mensaje por defecto.

![](21.png)

En la máquina de destino, aparecerá este mensaje, que aceptaremos.

![](22.png)

Esperamos a que haga la conexión...

![](23.png)

Veremos las instrucciones que nos da el programa.

![](24.png)

Continuaremos para conectarnos.

![](25.png)

Finalmente, comprobaremos que nos hemos conectado desde una máquina a otra.

![](26.png)

### Remmina

Por último, vamos a utilizar **Remmina** desde una máquina Ubuntu, para conectarnos por escritorio remoto a **Windows 10**.

### Preparación

Primero, tendremos que habilitar el escritorio remoto de la máquina **Windows**.

![](27.png)

Comprobaremos la dirección **IP** de la máquina.

![](28.png)

### Conexión

En la máquina **Ubuntu** introduciremos la dirección **IP** del equipo **Windows**.

![](29.png)

Aceptaremos el certificado.

![](30.png)

Introduciremos las credenciales para conectarnos.

![](31.png)

Veremos que nos hemos conectado sin problema.

![](32.png)
