---
title: Veyon
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.giuseppefava.com%2Fwp-content%2Fuploads%2F2020%2F02%2Fveyon_controllo_remoto_laboratorio-336x163.jpg&f=1&nofb=1&ipt=5d65572325df6be75808032a70419e119a92f16536bf0a1afc0efa734de7b48f&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-12-19"
date: "2022-12-19 01:00:00+0200"
slug: /2022/12/veyon
tags:
- Veyon
- Windows 10
- Redes
- Monitoreo
---

## Instalación

Primero, descargaremos el instalador desde su página oficial en los 3 equipos.

![](1.png)

Lo ejecutamos y seguimos el asistente.

![](2.png)

Aceptamos el acuerdo de licencia.

![](3.png)

Seleccionamos la carpeta de destino por defecto.

![](4.png)

En la primera máquina que instalemos, seleccionaremos el componente *Veyon Master*.

![](5.png)

Esperamos a que se instale.

![](6.png)

Finalizaremos y ejecutaremos el **configurador** ahora.

![](7.png)

## Configuración

### Master

Ahora, crearemos un par de claves para poder acceder a otros equipos.

![](8.png)

Veremos la ubicación donde se han guardado.

![](9.png)

Verificamos que tenemos una privada y otra pública.

![](10.png)

Exportaremos la clave pública.

![](11.png)

Añadiremos los dos equipos en la misma ubicación.

![](16.png)

### Alumnos

En la instalación de los otros equipos, es importante desmarcar la siguiente opción.

![](12.png)

Importaremos la clave que hemos exportado antes de **Master**.

![](13.png)

Confirmaremos que se ha importado bien.

![](14.png)

Seleccionaremos el método de autenticación mediante el archivo de clave y aplicamos la configuración.

![](15.png)

Es necesario que habilitemos el escritorio remoto en estos equipos.

![](17.png)

## Resultado final

Una vez hayamos configurado los dos equipos, podremos monitorear que están viendo, reiniciar o apagarlos, entre otras cosas.

![](18.png)
