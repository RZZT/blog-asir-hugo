---
title: Proyecto Personal Navidad
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Finvestidorsardinha.r7.com%2Fwp-content%2Fuploads%2F2021%2F04%2Fo-que-e-blockchain-definicao-como-funciona-e-aplicacoes-da-tecnologia.jpg&f=1&nofb=1&ipt=b0a670cb96656a84e689f9465ceb5a4189c8bcf3b37a37449d70fccb1c812ef7&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2022-12-30"
date: "2022-12-30 01:00:00+0200"
slug: /2022/12/proyecto-personal-navidad
tags:
- Hyperledger Besu
- Docker
- Blockchain
- Node.js
- Grafana
- ETH
- Ubuntu Server
- Metamask
- Smart Contracts
- dApp
---

## Introducción

Para este proyecto he decidido crear una *blockchain* privada usando la tecnología **Hyperledger**. El objetivo es crearla usando una máquina virtual *Ubuntu Server*, y usar [Docker](https://www.docker.com/) para crear diferentes **nodos** validadores. 

Una vez hecho esto comprobaremos que funciona, transfiriendo *tokens* entre **wallets**, firmaremos transacciones de una *dApp* y por último desplegaremos un *Smart Contract* muy sencillo en nuestra propia red.

### ¿Qué es *Hyperledger*?

[Hyperledger](https://www.hyperledger.org/) es un proyecto de **código abierto** liderado por la **Fundación Linux** que tiene como objetivo desarrollar tecnologías de **registro distribuido** ([DLT](https://www.investopedia.com/terms/d/distributed-ledger-technology-dlt.asp)) y **aplicaciones descentralizadas para uso empresarial**. Es una plataforma de tecnología que permite a las empresas **crear y ejecutar aplicaciones descentralizadas de manera segura y confiable**.

### ¿Qué es *blockchain*?

En resumen, es una **base de datos distribuida y descentralizada** que se utiliza para **registrar y validar transacciones** de manera segura y confiable **sin la necesidad de una autoridad central**.

Es utilizada en una variedad de aplicaciones, la más conocida son las criptomonedas, seguro que hemos oído hablar alguna vez sobre [Bitcoin](https://bitcoin.org/es/) o [Ethereum](https://ethereum.org/es/), pero como veremos aquí, tiene multitud de usos como la **identidad digital, la contratación de seguros y el registro de propiedad** etc...

## Preparaciones

### Ubuntu Server

Primero, crearemos una nueva máquina virtual *Ubuntu Server* con **8GB** de *RAM* y **2** procesadores.

![](0.png)

Iniciaremos sesión y observaremos la dirección *IP* para conectarnos mediante **SSH**.

![](1.png)

Nos conectaremos desde nuestro equipo local y actualizaremos los paquetes.

![](2.png)

### Docker

Ahora instalaremos [Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) siguiendo al documentación oficial.

![](3.png)

Añadiremos el usuario actual al grupo de [Docker](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

![](4.png)

Reiniciaremos el servidor y comprobaremos que se ha instalado correctamente.

![](5.png)

#### Docker Compose

Después, instalaremos [Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04), que nos permitirá definir y ejecutar aplicaciones compuestas por múltiples contenedores **Docker**.

![](6.png)

### Node.js

Necesitaremos instalar [Node.js](https://nodejs.org/es/) para poder ejecutar *JavaScript* en el lado del servidor. En este caso vamos a utilizar [nvm](https://github.com/nvm-sh/nvm#install--update-script) para poder administrar diferentes versiones facilmente.

![](7.png)

Reiniciaremos de nuevo el servidor, y ahora tendremos disponible el comando `nvm` para instalar, listar y utilizar diferentes versiones.

![](8.png)

### Truffle

Una vez hayamos instalado *Node.js*, usaremos el gestor de paquetes por defecto, [npm](https://www.npmjs.com/) para instalar [Truffle](https://trufflesuite.com/) de forma global.

![](9.png)

Comprobaremos

![](19.png)

## Instalación

Para empezar, instalaremos [Quorum Dev Quickstart](https://github.com/ConsenSys/quorum-dev-quickstart) utilizando `npx` para que descargue el paquete como haría `npm` y además lo ejecute.

![](10.png)

Se iniciará un asistente, en el que seleccionaremos que queremos ejecutar el cliente **Hyperledger Besu**.

![](11.png)

Establecemos que no queremos usar mas herramientas para no sobrecargar el servidor y finalizará la instalación.

![](12.png)

Luego, accederemos a la carpeta que se ha creado y ejecutaremos el *script* `run.sh` y esperaremos a que se inicie la red.

![](13.png)

Después de un buen rato, ya tendremos montada nuestra red con varios *endpoints*.

![](14.png)

Usando el comando `docker ps -a`, veremos todos los contenedores, y borraremos el `hello-world`.

![](15.png)

Si accedemos a la dirección **IP** del servidor por el puerto *25000* en el navegador, observaremos el explorador de nuestra red.

![](16.png)

También podríamos hacer una consulta usando el comando `curl`.

![](17.png)

En el caso de que hayamos apagado la máquina, podemos volver a iniciarla usando el *script* `resume.sh` y volver a consultar todos los *endpoints* `list.sh`.

![](22.png)

## Metamask

### Añadir red

Utilizaremos la extensión [Metamask](https://metamask.io/) como *wallet* para nuestra red. Una vez hayamos terminado la configuración inicial, expandiremos la vista para las configuraciones.

![](18.png)

Aquí, añadiremos una nueva red con los siguientes parámetros.

![](20.png)

Si hemos hecho todo bien, ahora podremos cambiar entre redes y aparecerá con un *tick* verde.

![](21.png)

### Importar cuenta

Después de añadir la red, importaremos una cuenta nueva a esta *wallet*.

![](23.png)

Pegaremos una **clave privada** que nos da la [documentación oficial](https://besu.hyperledger.org/en/stable/private-networks/tutorials/quickstart/#create-a-transaction-using-metamask). 

![](24.png)

En esta *wallet* tendremos **90.000 Ethers**, no son los reales (los de la *mainnet*). Solo son válidos en nuestra red.

![](25.png)

### Transferir *tokens*

Desde esta cuenta, podemos transferir *tokens* a la otra cuenta que tenemos.

![](26.png)

Seleccionamos la cuenta 1.

![](27.png)

Le vamos a enviar **10 Ethers**. También tendremos que darle *gas* para que se ejecute la transacción.

![](28.png)

Confirmaremos la transacción.

![](29.png)

En el explorador, veremos que se ha ejecutado en el bloque **607**.

![](30.png)

En la cuenta 1, ahora tendremos **10 Ethers** (en nuestra red privada).

![](31.png)

En la cuenta 2, veremos en la actividad se han enviado.

![](32.png)

### Conectarse al explorador

Otra manera de transferir *tokens* sería conectándonos al explorador con **Metamask**. Seleccionaremos la cuenta 2 y continuaremos.

![](33.png)

Una vez hayamos conectado la cuenta 2, pegaremos la dirección de la cuenta 1 para enviarle **100 Ethers**.

![](34.png)

Confirmaremos la transacción desde la extensión.

![](35.png)

Aparecerá una ventana emergente, donde veremos el número del bloque donde se ha almacenado la transacción.

![](36.png)Consultaremos de nuevo la cuenta 1, y veremos que ahora tiene **110 Ethers**.

![](37.png)

## Tienda de Mascotas Descentralizada

### Desplegar *dApp*

Ahora, vamos a desplegar una *dApp*, que será una tienda de mascotas donde podemos donar *Ethers*.

![](38.png)

Accederemos al directorio donde encontraremos el *script* para iniciar la aplicación.

![](39.png)

Esperaremos a que termine de ejecutarse.

![](40.png)

Observaremos que comienza la migración de un contrato a nuestra *blockchain*...

![](41.png)

Esperaremos a que se termine de construir el contenedor...

![](42.png)

### Usar *dApp*

Cuando termine, podremos acceder a la aplicación a través del puerto **3001** y si pulsamos en adoptar, nos aparecerá **Metamask** y seleccionaremos la cuenta 2.

![](43.png)

Confirmaremos la transacción para transferir **70 Ethers**.

![](44.png)

## Despliegue de *Smart Contract*

Después, usaremos [Remix IDE](https://remix.ethereum.org/) para desplegar un **contrato inteligente** que almacenará un número en la *blockchain* y luego lo podremos consultar. Así que lo primero será compilarlo.

![](45.png)

Luego, lo desplegaremos en nuestro entorno de la red privada conectándonos con la cuenta 2 en **Metamask**.

![](46.png)

Confirmaremos la transacción para implementar el contrato.

![](47.png)

Una vez lo hayamos desplegado, podremos usarlo. En la ventana de la izquierda veremos un registro con la transacción de la implementación verificada.

![](48.png)

Para poder almacenar un número, llamaremos a la función del *contrato* con el número 17. Confirmaremos la transacción (el coste de procesamiento).

![](49.png)

Observaremos que se ha registrado el número en la *blockchain* y luego, llamaremos a la función para consultarlo.

![](50.png)

## Grafana

Por último, comprobaremos el estado de nuestra red usando *Grafana*.

![](51.png)

Veremos todas las métricas que nos ofrece de manera predeterminada.

![](52.png)

> [Documentación utilizada](https://besu.hyperledger.org/en/stable/private-networks/tutorials/quickstart/).
