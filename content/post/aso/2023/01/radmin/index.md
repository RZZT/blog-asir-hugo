---
title: Radmin
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpaperaccess.com%2Ffull%2F545501.jpg&f=1&nofb=1&ipt=92d8d49b761ff92a7e5b069bcaf0f582eb169a19e81e02abc96ec02016f38f0b&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-01-07"
date: "2023-01-07 01:00:00+0200"
slug: /2023/01/radmin
tags:
- Radmin
- Windows 10
- Redes
---

## Server

### Instalación

Primero descargaremos los instaladores desde su página web oficial. En un equipo instalaremos el *server*.

![](1.png)  

Aceptamos los términos de licencia.

![](2.png)

Instalamos.

![](3.png)

Esperamos a que termine de instalarse...

![](4.png)

Cuando finalice, iniciaremos la configuración.

![](5.png)

### Configuración

Aquí, vamos a configurar los permisos.

![](6.png)

Añadiremos un nuevo usuario a **Radmin**.

![](7.png)

Agregaremos un usuario llamado `raul`.

![](8.png)

A este usuario le daremos acceso a todo.

![](9.png)

## Viewer

### Instalación

Después, instalaremos el *viewer* en otro equipo, conectado a la misma red.

![](10.png)

Aceptaremos los términos de licencia.

![](11.png)

Lo instalaremos para todos los usuarios.

![](12.png)

Instalamos.

![](13.png)

Finalizaremos el instalador y abrimos la aplicación.

![](14.png)

### Configuración

En la aplicación, añadiremos una conexión.

![](15.png)

Indicaremos la dirección *IP* del servidor, y el puerto predeterminado.

![](16.png)

### Conexión

Por último, ejecutaremos la conexión usando el usuario que hemos creado antes.

![](17.png)

Nos conectaremos sin problema como si fuese una conexión de *TeamViewer*...

![](18.png)
