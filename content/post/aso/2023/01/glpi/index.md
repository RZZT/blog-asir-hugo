---
title: GLPI
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.U9nSUa8HJPHHaGg0gBXLAgHaDt%26pid%3DApi&f=1&ipt=13f45b4dd5cf9ecfb8b6aaa945c4899fff107fc129eb410f39b8a20dd2312014&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-01-23"
date: "2023-01-23 01:00:00+0200"
slug: /2023/01/glpi
tags:
- Debian
- Linux
- Windows 10
- GLPI
- Fusion Inventory
---

## Preparar Máquina Debian

### Máquina Virtual

Crearemos una nueva máquina virtual con la imagen `.iso` de Debian.

![](1.png)

Configuraremos el *hardware* de la máquina virtual de esta manera.

![](2.png)

### Instalación

Seleccionamos la opción de instalación gráfica.

![](3.png)

Indicamos el idioma español.

![](4.png)

Establecemos la ubicación en España.

![](5.png)

Seleccionamos el teclado en Español también.

![](6.png)

Escribimos el nombre de la máquina.

![](7.png)

Introducimos la contraseña para el superusuario.

![](8.png)

Crearemos un usuario llamado *raul*.

![](9.png)

Configuraremos la misma contraseña para el usuario.

![](10.png)

Particionaremos utilizando todo el disco.

![](11.png)

Usaremos el esquema para **novatos**.

![](12.png)

Finalizaremos el particionado...

![](13.png)

Confirmaremos el formateo.

![](14.png)

Esperaremos a que se instale todo el sistema...

![](15.png)

Seleccionaremos que no queremos analizar el dispositivo.

![](16.png)

Configuraremos el gestor de paquetes predeterminado.

![](17.png)

No participaremos en la *telemetría*.

![](18.png)

Seleccionaremos el entorno de escritorio `GNOME` y el servidor *SSH*.

![](19.png)

Instalaremos el cargador de arranque en el mismo disco.

![](20.png)

Seleccionamos el único disco que tenemos.

![](21.png)

Reiniciamos la máquina para terminar la instalación.

## Pila LAMP

### Instalación

Ahora nos convertiremos en el usuario `root` y actualizaremos el sistema.

![](22.png)

Instalaremos `apache2`, `php`, `maria-db` y sus librerías necesarias.

![](23.png)

Comprobaremos el estado del servicio *web* y la base de datos.

![](24.png)

Ahora, descargaremos **glpi** desde su página de [GitHub](https://github.com/glpi-project/glpi/releases/tag/10.0.3).

![](25.png)

### Configuraciones

Accedemos a la base de datos con el usuario `root`. Crearemos una nueva base de datos, y un usuario llamado **glpi**. Le daremos todos los privilegios a este usuario en la base de datos.

![](26.png)

Instalaremos más librerías de `php` necesarias.

![](27.png)

Reiniciamos el servicio *web* y comprobamos que sigue funcionando.

![](28.png)

## GLPI

### Instalación

Primero, descomprimimos el archivo que hemos descargado antes en la carpeta `/var/www/`-

![](29.png)

Luego crearemos el archivo `downstream.php` con este código.

![](30.png)

Haremos lo mismo para el archivo `local_define.php`, escribiendo este código.

![](31.png)

Le cambiaremos el propietario a las carpetas para que el usuario `www-data` (usuario de apache2) tenga permisos.

![](32.png)

Después habilitaremos el módulo `rewrite` y editaremos la configuración de la *web*.

![](33.png)

Escribimos esta configuración para que todo funcione.

![](34.png)Habilitaremos el nuevo sitio y reiniciamos el servicio de nuevo.

![](35.png)

### Configuración

Accederemos al instalador *web* desde `localhost/glpi`.

![](36.png)

Aceptamos la licencia...

![](37.png)

Hacemos clic en **Instalar**

![](38.png)

Si aparece algún *warning*, continuaremos...

![](39.png)

Escribimos la configuración de la base de datos de antes...

![](40.png)

Después de comprobar la conexión, encontrará la base de datos...

![](41.png)

Continuaremos para que inicialice la base de datos...

![](42.png)

Desmarcamos el envío de estadísticas...

![](43.png)

Finalizamos la configuración.

![](44.png)

Comenzamos a utilizar GLPI.

![](45.png)

Es recomendable borrar la carpeta del instalador...

![](46.png)

Accederemos con el usuario `glpi:glpi` al panel de control.

![](47.png)

Aquí nos aparecerá un *warning* porque es recomendable cambiar las contraseñas de los usuarios.

![](48.png)

Desde la configuración de usuarios les cambiaremos las contraseñas.

![](49.png)

## Fusion Inventory

### Instalación

Ahora, descargaremos [Fusion Inventory](http://fusioninventory.org/) desde su [GitHub](https://github.com/fusioninventory/fusioninventory-for-glpi/releases/tag/glpi10.0.3%2B1.0).

![](50.png)

Copiaremos el archivo comprimido en `/var/www/glpi/plugins` y lo descomprimimos.

![](51.png)

### Configuración

Le cambiaremos el nombre usando el comando `mv`.

![](52.png)

Desde el panel de administración instalaremos el nuevo *plugin*.

![](53.png)

Luego, lo activaremos.

![](54.png)

## Comprobaciones

### Windows 10

Ahora, añadiremos un cliente **Windows 10** con adaptador **NAT** (como el servidor).

![](55.png)

Descargaremos el agente desde su página web.

![](56.png)

Iniciaremos el Instalador.

![](57.png)

Aceptamos los términos para continuar.

![](58.png)

Seleccionamos el tipo de instalación completa.

![](59.png)

Indicamos la dirección *IP* de nuestro servidor.

![](60.png)

Previamente, le hemos puesto una dirección *IP* estática a este servidor.

![](65.png)

Seleccionamos el primer modo de ejecución.

![](61.png)

Finalizamos el asistente de instalación.

![](62.png)

Una vez instalado, ejecutaremos el siguiente comando para iniciar el agente.

![](63.png)

Esperamos un poco y aparecerá el ordenador en el panel de administración.

![](64.png)

### Debian 11 Cliente

Crearemos una nueva máquina virtual como cliente *Linux*.

![](66.png)

Creamos al usuario `manolo`.

![](67.png)

Desde la terminal, instalaremos `fusioninventory-agent`.

![](68.png)

Nos conectaremos al servidor con este comando...

![](69.png)

Observaremos que ahora aparece también en el panel de control web.

![](70.png)

#### Incidencias

Por último, vamos a crear un *Ticket*, por un error usando Office.

![](71.png)

Seleccionamos el *Debian*...

![](72.png)

Aparecerá el *ticket* en el panel.

![](73.png)

Podemos entrar y dar una solución...

![](74.png)

Una vez solucionado, podemos cerrar la incidencia.
