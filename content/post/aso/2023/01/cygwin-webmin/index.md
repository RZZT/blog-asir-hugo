---
title: Cygwin y Webmin
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fwp4615505.png&f=1&nofb=1&ipt=78128d0b6237d355668db7fe5c66ffcd383e4a24c1deb2e11551d1e42e0e9022&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-01-08"
date: "2023-01-08 01:00:00+0200"
slug: /2023/01/cygwin-webmin
tags:
- Cygwin
- Webmin
- Windows 10
---

## Cygwin

### Instalación

Primero, descargaremos el instalador desde su página *web*.

![](1.png)

Lo ejecutaremos e iniciaremos el asistente.

![](2.png)

Seleccionaremos que queremos instalarlo desde *Internet*.

![](3.png)

Dejaremos el directorio por defecto.

![](4.png)

También dejaremos el directorio de los paquetes por defecto.

![](5.png)

Seleccionaremos un servidor para descargar los componentes.

![](6.png)

Seleccionaremos el paquete `perl` para **webmin**.

![](7.png)

Y `wget` para descargar desde la terminal.

![](8.png)

Instalaremos los componentes...

![](9.png)

Esperaremos a que se descargue todo...

![](10.png)

## Webmin

### Descarga

Podríamos descargar `webmin` directamente de *Internet*, pero en este caso solo copiaremos el enlace de la descarga.

![](11.png)

Descargaremos el paquete desde la terminal con el comando `wget`.

![](12.png)

Una vez se haya descargado, lo descomprimiremos.

![](13.png)

### Instalación

Entraremos en el directorio, y ejecutaremos el instalador.

![](14.png)

Dejaremos todas las opciones por defecto, menos la contraseña, escribimos `admin`.

![](15.png)

Cuando termine de ejecutarse el instalador, nos aparecerá una alerta para permitir el acceso mediante el *firewall* de **Windows**. Además, veremos la dirección *web* de nuestro `webmin`.

![](16.png)

### Acceso web

Accederemos con las credenciales que hemos configurado antes. `admin:admin`

![](17.png)

Veremos el panel de control de `webmin` y ya podremos configurar servicios mediante una interfaz gráfica *web*.

![](18.png)
