---
title: Clonezilla
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwethegeek.com%2Fwp-content%2Fuploads%2F2020%2F03%2F15-Best-Disk-Cloning-Software-for-Windows-10-8-7.jpg&f=1&nofb=1&ipt=a22415b20251d2d92cbc63e98e4632dbb115a5988b26b1ede61525aa95980837&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-01-19"
date: "2023-01-13 01:00:00+0200"
slug: /2023/01/clonezilla
tags:
- Clonezilla
- Windows 10
- Clonación
- Imágenes
- SMB
- Multicast 
---

## Preparaciones

Primero descargaremos la imagen estable desde su página web oficial.

![](3.png)

Luego añadiremos un nuevo disco duro de la **misma capacidad**, el disco de *Clonezilla* y pondremos el adaptador de red *Bridged*.

![](1.png)

Luego, iniciaremos el disco duro que acabamos de añadir.

![](2.png)

## Samba

### Crear Imágen a Disco

Ahora, iniciaremos desde el disco.

![](4.png)

Seleccionaremos la primera opción.

![](5.png)

Escogemos el idioma español.

![](6.png)

Mantendremos la distribución.

![](7.png)

Iniciaremos el programa *Clonezilla*.

![](8.png)

Seleccionaremos la primera opción para crear una imágen de un dispositivo.

![](9.png)

Indicamos que es de un dispositivo local.

![](10.png)

Pulsamos `Intro` para continuar.

![](11.png)

Pulsamo `CTRL + C` para salir de esta ventana.

![](12.png)

Seleccionamos el disco de destino.

![](13.png)

Indicamos que no queremos comprobar los archivos.

![](14.png)

Abortamos, ya que no queremos indicar ningún directorio de destino.

![](15.png)

Si aparece esta ventana, indicamos que no queremos hacerlo de nuevo.

![](16.png)

Cuando termine, pulsamos `Intro` para continuar.

![](17.png)

Ahora ejecutaremos *Clonezilla* en modo principiante.

![](18.png)

Seleccionamos que queremos guardar el disco local como imagen.

![](19.png)

Le ponemos un nombre descriptivo.

![](20.png)

Indicamos el disco que queremos copiar. (*origen*)

![](21.png)

Elegimos el primer modo de compresión.

![](22.png)

Omitimos la comprobación de los archivos de origen.

![](23.png)

Tampoco queremos comprobar la imagen grabada.

![](24.png)

No cifraremos la imagen.

![](25.png)
Cuando termine, reiniciaremos el equipo.

![](26.png)

Veremos el comando que va a ejecutar en verde. Confirmaremos la clonación.

![](27.png)

Esperaremos a que termine el proceso.

![](28.png)

Después de reiniciarse, veremos que ha creado la imagen.

![](29.png)
Ahora crearemos una carpeta llamada `images` y donde almacenaremos las imágenes. Esta carpeta la vamos a compartir en red.

![](30.png)

Nos fijaremos en la dirección de ruta de acceso.

![](31.png)

### Restaurar Imágen a Disco

Ahora, crearemos una nueva máquina, sin sistema operativo y con un disco duro de la misma capacidad. Nos aseguraremos que tiene una interfaz de red *Bridged* y el disco de *Clonezilla*.

![](32.png)

Iniciaremos la primera opción.

![](33.png)

Indicamos el idioma Español.

![](34.png)

Mantendremos la distribución de teclado.

![](35.png)

Iniciamos *Clonezilla*.

![](36.png)

Seleccionamos el modo de **Dispositivo / Imágen**.

![](37.png)

Seleccionamos la opción `samba_server`.

![](38.png)

Usaremos el modo **DHCP** para obtener la configuración de red.

![](39.png)

Ahora, indicamos la dirección *IP* del equipo que tiene la carpeta compartida.

![](40.png)

El usuario que tiene permiso en este caso se llama `raul`.

![](41.png)

El directorio que hemos creado y compartido se llama `images`.

![](42.png)

Indicamos que la negociación de `SMB` sea automática.

![](43.png)

Indicamos el sistema de seguridad por defecto.

![](44.png)

Nos obliga a poner la contraseña del usuario para poder acceder.

![](45.png)

Escribiremos la contraseña y continuaremos.

![](46.png)

Iniciaremos en modo principiante de nuevo.

![](47.png)

Seleccionamos la opción para **restaurar** imagen a disco local.

![](48.png)

Elegimos la imagen. (Si hemos llegado hasta aquí significa que ha sido capaz de acceder al recurso compartido y ver la imagen) :D

![](49.png)

Indicamos el disco de destino, donde se va a restarurar.

![](50.png)

Indicaremos la primera opción.

![](51.png)

Saltaremos la comprobación de la imagen antes de restaurar.

![](52.png)

Indicamos que queremos reiniciar cuando termine.

![](53.png)

Observaremos el comando que va a ejecutar y continuaremos.

![](54.png)

Esperaremos a que termine de restaurar...

![](55.png)

Cuando termine de restaurarse, comprobaremos que se ha clonado perfectamente.

![](56.png)

## Multicast

### Servidor

En el mismo equipo que hemos creado la imagen, iniciaremos un servidor *Clonezilla*.

![](57.png)

Seleccionamos que queremos iniciar el servidor.

![](58.png)

Indicamos que los clientes tienen que arrancar de red.

![](59.png)

Seleccionamos que detecte el servicio **DHCP** automáticamente.

![](60.png)

Escribimos `y` para continuar.

![](61.png)

Montaremos el dispositivo dónde tenemos la imagen.

![](62.png)

Esperaremos a que detecte todos los discos y después presionamos `CTRL + C` para salir.

![](63.png)

Seleccionamos el disco `IMAGENES`.

![](64.png)

No queremos comprobar ni reparar el sistema de archivos.

![](65.png)

Ahora entraremos en la carpeta donde tenemos las imagenes.

![](66.png)

Una vez hayamos entrado, tabulamos hasta el botón *Done*.

![](67.png)

Entraremos en el modo principiante.

![](68.png)

Elegiremos el modo de despliegue masivo por multicast.

![](69.png)

Escogeremos el modo de despliegue masivo de una imagen.

![](70.png)

Indicaremos que queremos restaurar la imagen a disco cliente.

![](71.png)

Por último, cuando terminemos de clonar, queremos que se apague el equipo.

![](72.png)

Seleccionaremos el modo **Multicast**.

![](73.png)

Queremos indicar el número de clientes que queremos clonar.

![](74.png)

En este caso será solo 1.

![](75.png)

Aparecerá una ventana con el servidor iniciado, **no apagaremos** este equipo hasta que terminemos de clonar.

### Cliente

Ahora, en un nuevo cliente, iniciaremos desde el disco de *Clonezilla* y seleccionaremos el arranque por **PXE**. También se puede arrancar directamente por red.

![](77.png)

Seleccionamos la imagen que estamos compartiendo desde el servidor.

![](78.png)

Veremos en la consola, las configuraciones de red que se están ejecutando.

![](79.png)

Observamos que se ha detectado el disco y se va a clonar la imagen.

![](80.png)

Comenzará la clonación...

![](81.png)

Esperaremos a que se complete...

![](82.png)

Una vez haya terminado de clonarse el equipo cliente, en el **servidor** escribiremos `y` para pararlo y se apagará.

![](83.png)

Por último, comprobaremos que se ha clonado, mirando las especificaciones.

![](84.png)
