---
title: FOG Project
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fwp4009371.jpg&f=1&nofb=1&ipt=244ecf1a1ff17d6685e4d9563f1241c2e0c606ae3922769879ac81ad7292b957&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-02-17"
date: "2023-02-17 01:00:00+0200"
slug: /2023/02/fog
tags:
- Debian
- Redes
- FOG
- Clonación
---

## Configuraciones Previas

Primero, creamos una nueva interfaz de red *Custom* llamada **LAN** y no usaremos el **DHCP** integrado.

![](0.png)

Luego, en una máquina virtual **Debian**, comprobaremos que tenemos la configuración de red bien.

![](1.png)

## Instalación

Para instalar Fog Project , descargaremos el repositorio en la carpeta `/opt/`.

![](2.png)

Cuando se haya descargado, entraremos en la carpeta y cambiaremos a la rama de desarrollo para evitar futuros errores.

![](3.png)

Entraremos en el directorio `/bin` y ejecutamos el script de instalación. En la primera pregunta, indicamos la distribución que estamos usando.

![](4.png)

Luego, indicaremos la instalación normal, la interfaz de red local y activaremos el **DHCP**.

![](5.png)

Continuamos sin conexiones seguras, cambiar el *hostname* ni enviar estadísticas.

![](6.png)

Comprobaremos la información antes de instalar...

![](7.png)

Esperaremos a que se terminen de instalar todos los componentes.

![](8.png)

Ahora, nos pide que accedamos a la página web para continuar.

![](9.png)

Lo único que tendremos que hacer será hacer clic en el botón de instalar o actualizar.

![](10.png)

Cuando termine de cargar la página web, presionamos enter en la terminal y esperamos a que se termine de configurar.

![](11.png)

Cuando termine, veremos las credenciales de usuario por defecto para conectarnos al panel de administración web.

![](12.png)

Así que nos conectaremos...

![](13.png)

## Windows 10

### Crear Imágen

Primero, crearemos una nueva imágen indicando el sistema operativo que vamos a clonar.

![](14.png)

### Crear Host

Luego, crearemos un nuevo host, indicando la dirección **MAC** y la imágen que hemos creado antes.

![](16.png)

Búscaremos la dirección **MAC** en el equipo que queremos clonar.

![](15.png)

Luego, crearemos una nueva tarea para clonar el equipo.

![](17.png)

Confirmaremos la creación de la tarea.

![](18.png)

Ahora, dejaremos esta pantalla hasta que termine la clonación.

![](19.png)

### Clonación

Ahora, iniciaremos el cliente **Windows 10** desde la interfaz de red.

![](20.png)

Observaremos que ha funcionado porque le ha asignado una dirección IP y está arrancando.

![](21.png)

Esperamos a que termine de clonarse.

![](22.png)

Cuando termine la clonación, veremos que se ha guardado la imagen.

![](23.png)

### Restauración

Ahora, desde el mismo sitio donde hemos creado una tarea para clonar, desplegaremos una para restaurar.

![](24.png)

Aceptaremos el despliegue de la tarea.

![](25.png)

Arrancaremos otro cliente mediante la red, sin sistema operativo con un disco duro de la misma capacidad.

![](26.png)

Esperamos a que termine de restaurar el disco.

![](27.png)

Cuando termine, se reiniciará y tendremos el equipo clonado.

![](28.png)

## Debian Linux

### Crear Imágen

Ahora, haremos lo mismo pero con Debian. Crearemos una nueva imágen indicando el sistema operativo **Linux**.

![](29.png)

Nos aseguramos que el equipo que vamos a clonar tiene el mismo adaptador de red, para que estén en la misma red.

![](30.png)

### Crear Host

Esta vez, para crear el host, arrancaremos directamente desde la red.

![](31.png)

Nos aparecerá esta pantalla dónde haremos un registro rápido.

![](32.png)

### Clonación

Observaremos que se ha registrado en el panel de control. Después, creamos una nueva tarea para capturar la imágen de este equipo.

![](33.png)

Aceptaremos la creación de una nueva tarea para clonar.

![](34.png)

![](35.png)

Arrancaremos el mismo equipo desde la red y esperaremos a que se termine de clonar.

![](36.png)

Cuando termine, verificamos que se ha creado la imágen en el panel de administración, y ahora tenemos las dos imágenes.

![](37.png)

### Restauración

Para restaurar la imágen, usaremos otra máquina virtual sin sistema operativo con la misma capacidad de disco duro y seleccionamos la siguiente opción.

![](38.png)

Introducimos las credenciales para continuar. En mi caso, son las predeterminadas: `fog:password`.

![](39.png)

Seleccionamos el sistema operativo que queremos restaurar.

![](40.png)

Esperaremos a que termine de restaurarse.

![](41.png)

Una vez haya terminado, veremos que inicia sin problemas.

![](42.png)

Por último, consultaremos en el panel de control el espacio que hemos usado y el que queda libre.

![](43.png)
