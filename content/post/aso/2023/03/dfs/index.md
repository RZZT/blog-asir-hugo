---
title: DFS
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fth.jobsdb.com%2Fth-th%2Fwp-content%2Fuploads%2Fsites%2F3%2F2021%2F10%2Fsystem_content-1.png&f=1&nofb=1&ipt=9ddda690f6d12da53ac32899fb7863ad8ea0ab23a9cbfafe1a7687b4349cb4bf&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-03-01"
date: "2023-03-01 01:00:00+0200"
slug: /2023/03/dfs
tags:
- DFS
- Windows Server 2019
- Active Directory
- Windows 10
- DNS
- Redes
---

## Preparaciones

Para esta práctica, tenemos una máquina virtual **Windows Server 2019** como `DC01` del dominio `aso.org` con la dirección **IP** *10.33.19.140*.

![](1.png)  

También tendremos otra máquina virtual **Windows Server 2019** añadido al dominio `aso.org` con la dirección **IP** *10.33.19.150*.

![](2.png)

Aquí veremos que el **DC02** está añadido al dominio en el contenedor *Computers*.

![](3.png)

Ahora, instalaremos los roles necesarios para usar **DFS** en el servidor **DC02**.

![](4.png)

## Espacio de nombres

### Configuración

Una vez se haya instalado el rol en **DC02**, crearemos un nuevo espacio de nombres.

![](5.png)

Seleccionamos el servidor **DC02**.

![](6.png)

Lo llamaremos `RESPALDO`.

![](7.png)

Dejamos estas opciones por defecto.

![](8.png)

Revisamos la configuración y confirmamos.

![](9.png)

![](10.png)

Ahora crearemos una carpeta llamada `eti` en la raíz del Sistema Operativo.

![](11.png)

Agregaremos esta carpeta como destino del espacio de nombres.

![](12.png)

En este caso solo permitiremos la lectura para todos los usuarios.

![](13.png)

Agregamos el destino...

![](14.png)

Observamos que se ha añadido el destino a la carpeta `eti`.

![](15.png)

### Cliente Windows 10

Añadiremos un cliente **Windows 10** al dominio `aso.org`

![](16.png)

Comprobamos que está en el dominio desde el **DC01**.

![](17.png)

Añadiremos una unidad de red con la ruta de destino que hemos creado.

![](18.png)

Veremos que se ha añadido, y solo tenemos permisos de lectura. Podemos ver el contenido del archivo de texto pero no podemos crear nada.

![](19.png)

## Replicación

### Configuración

Ahora, crearemos una nueva replicación multipropósito.

![](20.png)

Nombraremos este grupo **REPLICACIÓN**.

![](21.png)

Añadimos los dos servidores como miembros.

![](22.png)

Será de tipo **Malla completa**.

![](23.png)

Replicaremos de forma continua entre los dos servidores.

![](24.png)

El miembro principal será el **DC02**.

![](25.png)

Crearemos la carpeta `publica` en la raíz del **DC02**.

![](26.png)

Seleccionamos la carpeta `publica` para replicar.

![](27.png)

Se verá de esta manera.

![](28.png)

Crearemos la carpeta `publica` en la raíz del **DC01** también.

![](29.png)

Añadimos la carpeta del otro miembro.

![](30.png)

Se verá así.

![](31.png)

Revisamos la configuración y creamos el grupo de replicación.

![](32.png)

![](33.png)

Nos advierte que puede tardar un poco en replicar la información...

![](34.png)

Comprobamos que se ha creado el grupo y las carpetas replicadas.

![](35.png)

### Comprobaciones

Primero, crearemos un nuevo archivo en la carpeta `publica` en **DC02**.

![](36.png)

En **DC01** comprobamos que se ha replicado.

![](37.png)

Por último, creamos una carpeta desde **DC01**.

![](38.png)

En **DC02** comprobamos que se ha replicado.

![](39.png)
