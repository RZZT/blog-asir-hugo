---
title: Servidor de impresión
description: Ionut y Raúl
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.condorinformatica.uy%2Fimgs%2Fproductos%2Fproductos34_2475.png&f=1&nofb=1&ipt=d26ad97694623b65c95941431d0bbc89fa7de3cd90202408fe490b3b5448952f&ipo=images
categories:
  - ASO
author: "Raúl Sánchez"
lastmod: "2023-03-08"
date: "2023-03-07 01:00:00+0200"
slug: /2023/03/servidor-de-impresion
tags:
- Windows Server 2019
- Servicio de Impresión
- Windows 10
- Active Directory
- Unidades Organizativas
- Impresión en Internet
---

## Drivers

Primero, conectaremos la impresora a nuestro equipo físico, y de esta manera podremos conectarla a la máquina virtual.

![](0.png)

Luego, instalaremos los [drivers](https://support.hp.com/us-en/drivers/selfservice/hp-laserjet-p1100-printer-series/4110394/model/4110306) de HP en el servidor **Windows Server 2019**.

![](1.png)  

Esperaremos a que se instalen los controladores...

![](2.png)

Imprimiremos una página de prueba, para comprobar que la máquina virtual tiene una conexión directa con la impresora.

![](3.png)

## Servidor de Impresión

### Instalación

Ahora, instalaremos en el servidor **DC01** un nuevo rol.

![](4.png)

Seleccionamos el rol de **Servicios de impresión y documentos**.

![](5.png)

Nos aseguramos que el servidor de impresión está seleccionado.

![](6.png)

### Configuración

Una vez se haya instalado, lo configuraremos añadiendo una nueva impresora.

![](7.png)

Seleccionaremos el controlador que acabamos de instalar.

![](8.png)

Nos aseguraremos que estamos compartiendo esta impresora.

![](9.png)

Finalizaremos el asistente, e imprimimos otra página de prueba. (por si acaso)

![](10.png)

Esta es la página de prueba, dónde podemos encontrar mucha información.

![](11.png)

Si no hemos podido compartir la impresora antes, lo podemos cambiar en su configuración de esta manera...

![](12.png)

## GPO

Ahora crearemos una nueva **GPO** y la vinculamos al dominio directamente.

![](13.png)

La llamaremos **IMPRESORAS**.

![](14.png)

Editaremos la directiva para implementar una nueva impresora.

![](15.png)

Seleccionamos nuestra impresora. (La veremos aquí si la hemos compartido antes)

![](16.png)

La añadimos...

![](17.png)

Veremos como ha quedado la **GPO**...

![](18.png)

Luego, en el *Administrador de impresión*, seleccionamos la impresora e implementamos una nueva directiva de grupo.

![](19.png)

Añadiremos la directiva de grupo que acabamos de crear a esta impresora.

![](20.png)

## *Windows 10* Cliente

Para hacer las comprobaciones, crearemos un nuevo usuario llamado `ionut` en nuestro *Active Directory*.

![](21.png)

Después, iniciaremos sesión en un cliente *Windows 10* añadido al dominio `aso.org` y forzaremos la actualización de las **GPO** desde la terminal.

![](22.png)

Imprimiremos otra página de prueba y observaremos que aparece en los dispositivos del cliente...

![](23.png)

Por último, imprimiremos una imágen y seleccionaremos la impresora del dominio.

![](24.png)

Desde el servidor, observaremos la **cola de impresión**.

![](25.png)

Este es el resultado de la impresión sin color.

![](26.jpg)

## Unidades Organizativas

### Configuración

Crearemos dos **Unidades Organizativas** llamadas *ADMON* y *DISEÑO*.

![](26.png)

En *DISEÑO* crearemos al usuario `raul`.

![](27.png)

Luego, nos aseguraremos de cambiar los permisos de la impresora para que no todos tengan permitido imprimir.

![](28.png)

Añadiremos al usuario `ionut` que pertenece a la **UO** de **Administradores** para que tenga todos los permisos.

![](29.png)

Después, cambiaremos la **GPO** que hemos creado antes para que solo los objetos dentro de la **Unidad Organizativa** `ADMON` tengan compartida la impresora.

![](30.png)

### Comprobaciones

Ahora, verificamos que el usuario `ionut` puede seguir imprimiendo.

![](31.png)

Pero el usuario `raul` no tiene permiso ni para conectarse a la impresora.

![](32.png)

## Impresión en Internet

Para terminar, instalaremos la característica **Impresión en Internet** para montar un sitio *web* donde los usuarios puedan administrar los trabajos de impresión en el servidor.

![](33.png)

Accederemos mediante un navegador web, con las credenciales del usuario `ionut`.

![](36.png)

Veremos que está configurada la impresora.

![](37.png)

Si hay algún documento imprimiendo, la podremos ver aquí...

![](38.png)

Intentaremos acceder mediante un navegador a la ruta, accediendo con las credenciales del usuario `raul`.

![](34.png)

Al acceder con un usuario sin permisos, no veremos ninguna impresora, solo el panel de control.

![](35.png)
