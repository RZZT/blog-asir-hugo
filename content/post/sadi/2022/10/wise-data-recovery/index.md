---
title: Wise Data Recovery
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi1.wp.com%2Ffreecrack4u.com%2Fwp-content%2Fuploads%2F2018%2F08%2Fg_-_-x-_-_s_x20130716222305_0.png%3Ffit%3D1280%252C614%26ssl%3D1&f=1&nofb=1&ipt=efedca5579574137334abecc7e3615c70e20457ef8ef8f1564851e31defffda8&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-03"
date: "2022-10-01 01:00:00+0200"
slug: /2022/10/wise-data-recovery
tags:
- Wise Data Recovery
- Recuperación de archivos borrados
- Windows 10
---

> Es una herramienta que nos permite recuperar archivos borrados de nuestro disco duro, es una buena alternativa a otros programas como *Recuva*, *PhotoRec*, o *DiskDrill*.

## Instalación

Primero descargaremos el instalador de su página web oficial.

![](1.png)

Iniciamos el ejecutable y comenzará la instalación.

![](2.png)

Una vez se haya instalado, iniciamos el programa.

![](3.png)

## Recuperación de archivos

Aquí, iniciaremos la recuperación de archivos borrados en el disco *C:/*.

![](4.png)

Luego seleccionaremos que solo queremos recuperar los archivos de imágenes. Con extensión `.jpeg` y `.jpg`.

![](5.png)

Indicamos la ruta donde restaurar las imágenes.

![](6.png)

Esperamos a que se restauren...

![](7.png)

Cuando termine, nos dirigiremos al directorio que hemos indicado antes.

![](8.png)

Aquí, veremos todas las imágenes que hemos restaurado.

![](9.png)

## Conclusión

**Wise Data Recovery** es una alternativa perfecta a otros programas de recuperación de archivos. Es muy práctica ya que puedes filtrar por extensión de archivos o por la ruta.
