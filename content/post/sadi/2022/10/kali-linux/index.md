---
title: Kali Linux
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpaperaccess.com%2Ffull%2F1301120.jpg&f=1&nofb=1&ipt=70c9006427b4842a24549b94d4187f1d011944583c71e0126d717b6c7f51b7ed&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-16"
date: "2022-10-20 01:00:00+0200"
slug: /2022/10/kali-linux
tags:
- Kali Linux 
---

## ¿ Qué es ?

Es una distribución de código abierto basada en *Debian*. Este sistema está especializado en el ámbito de la seguridad informática. Tiene muchas herramientas para realizar pruebas de penetración y auditoria de seguridad. 

## Herramientas

### Nmap

**Network Mapped (Nmap)** es una herramienta de escaneo de red y detección de host. Se usa para recopilar información sobre cualquier red. Si deseas buscar un servicio *(HTTP, FTP, SSH, etc.)* ejecutándose en una computadora, el sistema operativo de cualquier dispositivo, etc., entonces puedes usar **nmap**

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flinoxide.com%2Fwp-content%2Fuploads%2F2020%2F02%2F2.-nmap-A-1024x462.png&f=1&nofb=1&ipt=e2dca5755a912307a20ea96ef74c8efa453ebebbb3ebd4a5cc7dfe807878aad0&ipo=images)

### BurpSuite

*BurpSuite* es una herramienta para realizar el análisis de seguridad de aplicaciones Web, cuenta con la integración de componentes que permiten realizar una auditoria a sitios *web* mal desarrollados e implementados como también el análisis de aplicaciones móviles, los cuales llegan a realizar uso de servicios *web*.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimg1.daumcdn.net%2Fthumb%2FR1280x0%2F%3Fscode%3Dmtistory2%26fname%3Dhttps%3A%252F%252Fblog.kakaocdn.net%252Fdn%252FcU32Gp%252Fbtq0BEbS4du%252FwxAb36Iz7NMQu0Rd7NV9K0%252Fimg.png&f=1&nofb=1&ipt=d411f59717b0a27a9e865ac4a6f4e7a1e2adf3da1ddac177a58ac532645958d5&ipo=images)

### Metasploit Framework

Esta es una herramienta muy potente, con la que podemos crear ataques, para explotar vulnerabilidades de un sistema, *Metasploit*, tiene *exploits*, tanto para **Windows**, **Linux**, o **Android**. También dispone de una versión *GUI*, llamada **Armitage**, la cual permite hacer los ataques de una forma gráfica.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.postimg.cc%2FkM2n69Zq%2Fmsfconsole-banner.jpg&f=1&nofb=1&ipt=573981776790c0ee3e3d294426cada01acb30f3fb406f89d1e1e48bf87a62629&ipo=images)

### Aircrack-ng

**Aircrack-ng** es una suite de *software* de seguridad inalámbrica. Consiste en un analizador de paquetes de redes, un **crackeador** de redes *WEP* y *WPA/WPA2-PSK* y otro conjunto de herramientas de auditoría inalámbrica.

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fe-tinet.com%2Fwp-content%2Fuploads%2F2017%2F04%2FAircrack-ng-ferramenta-para-kali.jpg&f=1&nofb=1&ipt=4359fc24a8e26762b38a74b60f9cf049446a74043d25ff65fe06882564e3256b&ipo=images)

### John the Ripper

**John the Ripper** es un programa de criptografía que aplica fuerza bruta para descifrar contraseñas. Es capaz de romper varios algoritmos de cifrado o **hash**, como *DES*, *SHA-1* y otros. 

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.blackmoreops.com%2Fwp-content%2Fuploads%2F2015%2F11%2FCracking-password-using-John-the-Ripper-in-Kali-Linux-blackMORE-Ops-3.jpg&f=1&nofb=1&ipt=d5044b52f9d72152cb6cfa29ce5ba863f5f17199bcf06a9749250c22c338e53a&ipo=images)

### SQL map

**SQLMap** es una herramienta de código abierto que permite automatizar el proceso de un ataque de inyección de *SQL*. Gracias a este software, no es necesario aprender este lenguaje de programación para inyectar código malicioso a una base de datos **MySQL**.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fprogrammersought.com%2Fimages%2F270%2Fd3c9dd7832a64343ef7d13c86b83606e.png&f=1&nofb=1&ipt=f6753f05e461106e05f523279148e471a86a857772397757c1869384a945d00e&ipo=images)

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F1.bp.blogspot.com%2F-bSK8xGOMQz8%2FVqpGA7gMN0I%2FAAAAAAAAEh4%2F2BVBZSx4zCM%2Fs1600%2Fsqlmap_7png.png&f=1&nofb=1&ipt=d49c99200a61e63f9574403bad4639497a59b8a5dfeac2ac4ff9fedeef6da5e3&ipo=images)

### Hydra

**Hydra**  es una herramienta de fuerza bruta que podemos utilizar en diversos protocolos, como: *FTP*, *SMB*, *SSH*, *MYSQL*...

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fallabouttesting.org%2Fwp-content%2Fuploads%2F2019%2F01%2Fhydra-help.jpg&f=1&nofb=1&ipt=790d4a9e558f194ffc5cbee772d6ba59023c946a01da0de023cd478c880bcc5d&ipo=images)

### Gobuster

**Gobuster** es una herramienta *open source* que permite la identificación de contenido *web* como directorios o ficheros que pudiesen estar accesibles u ocultos en un portal web. Esto lo realiza a través de solicitudes `HTTP` con un diccionario o por fuerza bruta, y detectará la existencia de las mismas en función del código de respuesta obtenido.

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fblog.itselectlab.com%2Fwp-content%2Fuploads%2Fgobuster-06.png&f=1&nofb=1&ipt=743ecff023b124a7a3bc6960468aab2050d3cf7ced6c8b327adeba8a51702d22&ipo=images)

### Netcat

**Netcat** es una herramienta de red que permite a través de intérprete de comandos y con una sintaxis sencilla abrir puertos `TCP/UDP` en un *HOST*, asociar una *shell* a un puerto en concreto y forzar conexiones `UDP/TCP`.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftechstorify.com%2Fwp-content%2Fuploads%2F2019%2F06%2FNetcat.png&f=1&nofb=1&ipt=323fbc4be0574245f208b178af15eb5fe4f944f4235b8f52cea515e71ed08782&ipo=images)

### Maltego

**Maltego** es una herramienta para recopilar información en la *web*, y la potencia que posee, permite hallar perfiles en cualquier red social que levanten alguna sospecha de operaciones malintencionadas. Además es capaz de hacer búsquedas de dominios, direcciones de correo electrónico, números telefónicos...

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F3.bp.blogspot.com%2F-cXjaOxHMu5Y%2FT1E5P50D5II%2FAAAAAAAAAHo%2FbjYluYHYuog%2Fs1600%2Fpenet.jpg&f=1&nofb=1&ipt=1c784f322990542e6fcce45eb5d8d8965c107f197089bfc84f066ff422b8aa14&ipo=images)
