---
title: Brave Browser
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.shortpixel.ai%2Fclient%2Fq_glossy%2Cret_img%2Cw_800%2Fhttps%3A%2F%2Fwww.droidviews.com%2Fwp-content%2Fuploads%2F2020%2F01%2Fbrave-browser.jpg&f=1&nofb=1&ipt=092c105943e4a02fffb9460a8b84019e1d68d6b62cf9aaeac035c2886d14aba7&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-07"
date: "2022-10-12 01:00:00+0200"
slug: /2022/10/brave-browser
tags:
- Brave Browser
- Privacidad 
---

## ¿ Qué es ?

Es un navegar *web* que ha estado ganando fama por sus funciones sobre la protección de la privacidad del usuario final. Está basado en *Chromium* al igual que *Google Chrome*. Además bloquea por defecto todos los anuncios y rastreadores raros de los sitios web que visitas.

## Funcionalidades principales

### Buscador

Este navegador no recopila, comparte, vende ni pierde tus datos. Tampoco usan métodos o algoritmos secretos para sesgar o censurar resultados. Como utiliza su propio índice independiente, es una alternativa real a **Google**.

### Cartera

Cuenta con una cartera de criptomonedas segura integrada directamente en el navegador, sin extensiones ni descargas adicionales. Esto se traduce en menos 
susceptibilidad a suplantación de aplicaciones, *phishing* y robo. Compra,
 almacena, envía e intercambia monedas.

## Otras funcionalidades

- [Firewall + VPN de Brave](https://brave.com/firewall-vpn/)
- [Videollamada](https://brave.com/talk/)
- [Noticias de Brave](https://brave.com/brave-news/)
- [Lista de reproducción de Brave](https://brave.com/playlist/)
- [Recompensas de Brave](https://brave.com/brave-rewards/)

## Conclusión

Personalmente yo llevo usando este navegador durante los dos últimos años y estoy bastante contento. Bloquea casi todos los anuncios de manera transparente al usuario y sus funcionalidades son muy útiles y fáciles de manejar.
