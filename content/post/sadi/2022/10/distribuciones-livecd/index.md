---
title: Distribuciones Live CD
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpaperaccess.com%2Ffull%2F646070.jpg&f=1&nofb=1&ipt=e5861ed14156ce09c37274b5339c6f973102e4aec37632dddc6d4e0c3f211cd2&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-21"
date: "2022-10-22 01:00:00+0200"
slug: /2022/10/distribuciones-livecd
tags:
- Distribuciones
- LiveCD
- Linux
- Tails
- Ultimate Boot CD
- Clonezilla
- Ophcrack Live CD
- Avira Antivir Rescue System 
---

> Un **LiveCD** no es otra cosa que una distribución de *Linux* que funciona al 100%, sin necesidad de instalarla en el ordenador donde la probamos. Utiliza la memoria *RAM* del ordenador para instalar y arrancar la distribución en cuestión. En la memoria también se instala un "disco virtual" que emula al disco dure de un ordenador.

## Tails

**Tails** es una distribución *Linux*, basada en *Debian*, cuya principal característica es que ha sido diseñada de tal forma que lleva la **privacidad y el anonimato** de los usuarios al máximo nivel.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fblog.desdelinux.net%2Fwp-content%2Fuploads%2F2015%2F10%2FTails_screenshot.png&f=1&nofb=1&ipt=176a39758758422d566af51d04d2cecdf3d68036e79e01ba3400574bc61f6616&ipo=images)

## Ultimate Boot CD

**Ultimate Boot CD** es un conjunto de herramientas para realizar pruebas y diagnósticos que se pueden grabar en un *Live CD*. Este disco se usa, sobre todo, para poder ejecutar pruebas en un ordenador cuyo sistema operativo estuviese dañado y no arrancase bien.

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.conchaalviz.com%2Fwp-content%2Fuploads%2F2017%2F12%2Fultimate-boot-cd.png&f=1&nofb=1&ipt=cf15a8ac78e6a094898f86148a5483cd2ab81f13b158026e7897ac0397c5599a&ipo=images)

## Gparted

Es un editor de particiones de *GNOME*, que ofrece multitud de opciones independientemente de cuál sistema operativo estés usando, ya que *Gparted* no necesita arrancar desde el disco duro. Con el puedes realizar tareas como redimensionar particiones, inspeccionar, copiar particiones, sistemas de archivos, así como simples tareas de borrado y creado de particiones desde un *CD*. 

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.portalprogramas.com%2Fimagenes%2Fprogramas%2Fes%2F253%2F9253_1.jpg&f=1&nofb=1&ipt=a49097abc62436e3aaa74bfbdca2325016f792baca52baeb9931f188f70896bd&ipo=images)

## Clonezilla

**Clonezilla** es un *software* libre de clonación de discos enteros o particiones. Por eso te puede salvar de un buen desastre, permitiéndote disponer de un *backup* o copia de seguridad de tu sistema o datos sin que se vean afectados por un fallo físico de tu *hardware* de almacenamiento o por cualquier otro problema de software, como por ejemplo un *ransomware* que te cifre el contenido de tu disco y debas pagar un rescate si quieres recuperarlo

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.clonezilla.org%2Fclonezilla-live%2Fdoc%2F04_Create_Recovery_Clonezilla%2Fimages%2Focs-01-b-sub-boot-menu.png&f=1&nofb=1&ipt=8c03ef03e5c3d13692c63f1f02e5207789eb0546e98ace4c23871ae4165d998c&ipo=images)

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fclonezilla-sysresccd.hellug.gr%2Fimages%2Frestore-18.png&f=1&nofb=1&ipt=4f7d95d92633eb11c2fb6c1091f2d1334791fb01f1b34b199a3365b3d5e22c62&ipo=images)

## Ophcrack

**Ophcrack 2.1** es una aplicación que permite obtener las contraseñas de tu sistema *Windows*.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.lifewire.com%2Fthmb%2FfvsHD87wipNOw3vUGtuFgt37OVM%3D%2F768x0%2Ffilters%3Ano_upscale()%3Amax_bytes(150000)%3Astrip_icc()%2Fophcrack-59f9ef73396e5a001a9990c8.PNG&f=1&nofb=1&ipt=a601fa455e6f54fcfd5dfc7efb3c4b947a7f10fb5a627ea1eeea728407661e70&ipo=images)

## Avira Antivir Rescue System

Es un disco de rescate que sirve para analizar unidades de disco, en busca de *malware*. Requiere unos requisitos muy bajos, por lo que puede funcionar en casi cualquier ordenador.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.avira.com%2Fimages%2Fcontent%2Fsupport%2FFAQ_KB%2FEN%2FRescue%2520System%2Frescue-system_save-report_en2.png&f=1&nofb=1&ipt=d030d37ca8bbbf18cd26349ff41a63bc6cc59f6f0769617e68ce8f2d904c5413&ipo=images)