---
title: Eraser
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.windowsreport.com%2Fwp-content%2Fuploads%2F2017%2F11%2FEraser.jpg&f=1&nofb=1&ipt=00d2d02d2bae4f487d40bba90efbd562b172069b71a2c42df7ba39eed3bbe1f6&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-03"
date: "2022-10-02 01:00:00+0200"
slug: /2022/10/eraser
tags:
- Eraser
- Windows 10
- Borrado de datos
---

## Instalación

Primero, descargaremos el instalador desde su página *web* oficial.

![](1.png)

Iniciaremos el ejecutable y comenzará la instalación.

![](2.png)

Aceptamos la licencia *GPL*.

![](3.png)

Seleccionamos el tipo de instalación típico.

![](4.png)

Comenzamos la instalación.

![](5.png)

Una vez se haya instalado, iniciamos el programa.

![](6.png)

## Borrado de forma segura

### Crear tarea

Ahora crearemos una nueva tarea.

![](7.png)

Esta tarea solo se iniciará de forma manual. Añadiremos que datos queremos borrar.

![](8.png)

Indicaremos que queremos borrar los archivos de la papelera de reciclaje con el método por defecto. Vemos que hay muchos modos de borrado más intenso.

![](9.png)

### Iniciar tarea

Veremos que en la papelera hay muchos archivos...

![](10.png)

Iniciaremos la tarea ahora.

![](11.png)

Esperaremos a que termine...

![](12.png)

## Comprobación

Finalmente, terminará de borrar todo...

![](13.png)

Ya no tendremos nada en la papelera de reciclaje. Además todo lo que hemos borrado es prácticamente **imposible** de recuperar.

![](14.png)

## Conclusión

**Eraser** es un programa perfecto como alternativa a *FreeEraser*, es muy sencillo de usar y tiene muchas más opciones. Permite crear tareas de borrado seguro y se ejecuta en segundo plano, puede ser totalmente transparente al usuario.
