---
title: Avast Antivirus
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi1.wp.com%2Fwww.meuwindows.com%2Fwp-content%2Fuploads%2F2018%2F02%2Fdesinstalar-o-avast-antivirus.jpg&f=1&nofb=1&ipt=1e55baabf0ddbe16f95aa671838eeb7e9899f040966d70e8de6b78c2549a8fa8&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-27"
date: "2022-10-25 01:00:00+0200"
slug: /2022/10/avast-antivirus
tags:
- Privacidad
- Malware
- Antivirus
- Avast 
---

## Instalación

Primero descargaremos el instalador de su página *web* oficial.

![](1.png)

Ejecutamos el archivo, y en mi caso desmarco la casilla para instalar el *Avast Secure Browser* porque ya uso un navegador seguro como [Brave Browser](https://asir.raulsanchezzt.com/2022/10/brave-browser/).

![](2.png)

Esperamos a que se instale...

![](3.png)

## Análisis

Una vez se haya instalado, nos aconsejará hacer un *análisis inteligente*.

![](4.png)

Esperamos a que termine el escaneo, puede tardar bastante...

![](5.png)

Cuando termine nos advierte que la configuración *DNS* puede ser manipulada. Pero si intentamos resolver este problema nos pide que paguemos la suscripción, así que por el momento lo omitiremos...

![](6.png)

Cerraremos este análisis y lo progamaremos para que se ejecute una vez al mes.

![](7.png)

## Otras funcionalidades

En el menú lateral podemos ver todas las herramientas de protección que podemos usar.

![](8.png)

Luego, veremos las herramientas de privacidad, la mayoría de pago.

![](9.png)

Por último, veremos las herramientas de rendimiento.

![](10.png)
