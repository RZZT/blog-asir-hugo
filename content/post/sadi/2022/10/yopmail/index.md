---
title: Yopmail
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Foptclean.com.br%2Fwp-content%2Fuploads%2F2017%2F03%2FYOPmail.jpg&f=1&nofb=1&ipt=e7d0e186d63a4768fb7434aa4a998407e95fa2e944fd616f4c140308f14421bd&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-07"
date: "2022-10-13 01:00:00+0200"
slug: /2022/10/yopmail
tags:
- Yopmail
- Correo electrónico temporal
- Privacidad 
---

## ¿ Qué es una dirección de correo temporal ?

El objetivo de la dirección de correo electrónico desechable es evitar dar su dirección de correo electrónico personal para protegerla, ya sea por razones de confidencialidad o para evitar recibir spam. 

## ¿ Qué es YopMail ?

Es una plataforma que te permite crear un correo electrónico temporal. Cualquier cuenta de correo está disponible y accesible sin necesidad de registrarse y sin tener que usar contraseña.

## Como utilizar YopMail

Primero accederemos a la página web de [YopMail](https://yopmail.com/es/email-generator) y crearemos un correo aleatorio.

![](1.png)

Podemos revisar el correo de esta cuenta. En caso de que algún servicio nos envíe un correo a esta dirección, lo veremos aquí.

![](2.png)

## Conclusión

Me ha parecido un servicio muy útil para ocasiones que necesitas recibir un correo electrónico pero no quieres arriesgar a dar tu correo personal por si acaso te mandan mucho *spam* u otras amenazas.
