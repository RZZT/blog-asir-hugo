---
title: Recopilación de datos de Google
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimg.unocero.com%2F2016%2F08%2FAnti-google.png&f=1&nofb=1&ipt=6eec3fe93dfd4d487d9e12c8e3b3fa9ac04032427b9e16d308f018a02f3b27a7&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-07"
date: "2022-10-14 01:00:00+0200"
slug: /2022/10/recopilacion-datos-google
tags:
- Google
- Recopilación de datos
- Privacidad 
---

Google puede almacenar hasta **5 GB** de datos personales de cada usuario. Esta compañía recopila, guarda y almacena muchos más datos personales de los que el usuario es consciente como los sitios que visita, sus hábitos de compras...

## ¿ Cómo lo evito ?

Podemos consultar la configuración de privacidad sobre nuestros datos en nuestra cuenta de *Google*. Yo lo tengo ya todo desactivado debido a esto.

![](1.png)

Más abajo, en la misma página, podemos ver que aplicaciones de *Google* estamos usando y podemos descargar o eliminar los datos.

![](2.png)

### Descargar

Podemos pedirle a *Google* que nos mande todos los datos que ha recopilado sobre nosotros en todos sus servicios.

![](3.png)

### Eliminar

También podemos eliminar un determinado servicio de *Google*.

![](4.png)

### Solicitar retirada de datos

Por último, podemos rellenar este [formulario](https://reportcontent.google.com/forms/rtbf) para pedirle a *Google* que elimine todos los datos sobre nosotros.

![](5.png)
