---
title: Tipos de Ciberataques
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Forig00.deviantart.net%2F51a3%2Ff%2F2016%2F306%2F0%2F9%2Fhackerman_by_shiiftyshift-dan31sc.png&f=1&nofb=1&ipt=a8e7ebaffa26bf4e26acf02a411dbaf9d2aa0d708781362e70873ec4b49aa86e&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-28"
date: "2022-10-26 01:00:00+0200"
slug: /2022/10/tipos-de-ciberataques
tags:
  - 
---

## Ataques de diccionario

La técnica consiste en **probar consecutivamente muchas palabras reales recogidas en los diccionarios de los distintos idiomas**, y también las contraseñas más usadas como *“123456”*, para tratar de **romper las barreras de acceso a sistemas protegidos con clave**. Este tipo de ataque está basado en el hecho probado de que un gran número de usuarios eligen las mismas contraseñas fáciles de recordar, pero también fáciles de adivinar por parte de los delincuentes.

## Ataques de fuerza bruta

Un ataque de fuerza bruta es un intento de **descifrar** una contraseña o nombre de usuario, de buscar una página *web* oculta o de descubrir la clave utilizada para cifrar un mensaje, que consiste en aplicar el método de **prueba y error** con la esperanza de dar con la combinación correcta finalmente. Se trata de antiguo método de ataque, pero sigue siendo **eficaz** y goza de popularidad entre los *hackers*.

## Programas

Para realizar estos ataques es recomendable usar una distribución de *Linux* enfocada a la seguridad como [Kali Linux](https://asir.raulsanchezzt.com/2022/10/kali-linux/). Estas son algunas herramientas que podemos utilizar:

### Patator

Patator es un *script* en **Python** para lanzar una gran diversidad de ataques de fuerza bruta, con un diseño modular que permite un uso flexible. Estos son los servicios que se pueden atacar: **FTP, SSH, Telnet, SMTP, HTTP, POP3, LDAP, SMB, Oracle, MySQL, PostgreSQL, VNC...**

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F4.bp.blogspot.com%2F-uGo8WLWP53o%2FWGexWrb40yI%2FAAAAAAAAB1c%2F2SSdnCmU6PMiMLfDjP6_Kf59f2X5s-ioQCLcB%2Fs1600%2Fpat4.png&f=1&nofb=1&ipt=cf0d1a0ad3664cd33dfeec28606c6fc0c080a7fb7d94e2db835c4f28dc0efa6e&ipo=images)

### THC Hydra

Esta es la herramienta más conocida para este tipo de ataques y se puede dividir en 3 tipos:

- Ataque de fuerza bruta por diccionario

- Ataque de fuerza bruta en profundidad

- Ataque de fuerza bruta en anchura

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimgconvert.csdnimg.cn%2FaHR0cDovL3d3dy56aG91bGluZ2ppZS5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTgvMDkvMjAxNzEyMjUxNTE0MTcwNjAwNzQxMDgxLnBuZw&f=1&nofb=1&ipt=6278c3eb5ae422d97526c0328c461e4a91fcbe51a811a216b79c39af4e80da07&ipo=images)

> Otras herramientas que pueden usarse para esto son *Medusa, Metasploit, BurpSuite, John the Ripper para ataques off-line y Aircrack-ng* para redes inalámbricas.

## Diccionarios

El repositorio más famoso y usado de diccionarios es el de **[Secslists](https://github.com/danielmiessler/SecLists)** de [Daniel Miessler](https://github.com/danielmiessler).



![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F1.bp.blogspot.com%2F-LjBWw9avkS0%2FXoH7aNcL5oI%2FAAAAAAAAEYI%2FPTj_z_10-nsdlY94Ujowj1ie2zMDauaugCLcBGAsYHQ%2Fs640%2Fsss.png&f=1&nofb=1&ipt=020b46505cee78b421cb1f0e71f9c23f7f9f4a4ca10e5256b7862bdd36da1989&ipo=images)
