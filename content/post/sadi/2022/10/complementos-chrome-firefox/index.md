---
title: Complementos de Seguridad en Chrome y Firefox
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.softzone.es%2Fapp%2Fuploads-softzone.es%2F2020%2F09%2FChrome-Firefox-Velocidad.jpg&f=1&nofb=1&ipt=b5e9fe7489a1d92dfef18d1820529057a1235e216f815d17d4c36a56855a7ccd&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-14"
date: "2022-10-17 01:00:00+0200"
slug: /2022/10/complementos-chrome-firefox
tags:
- Google Chrome
- Firefox
- HTTPS
- VPN
---

## Google Chrome

### HTTPS Everywhere

Esta extensión proporciona automáticamente cifrado HTTPS cuando se conecta a sitios web que no lo ofrecen.

![](1.png)

Para comprobarlo podemos entrar en la página web antigua del *CIP* *ETI* y activaremos la extensión.

![](2.png)

Nos aparecerá este aviso sobre la poca seguridad que tiene.

![](3.png)

### Privacy Badger

*Privacy Badger* supervisa los sitios **web** que visita y desarrolla una lista de rastreadores a bloquear en función de los sitios que visita con mayor frecuencia. Es un bloqueador de rastreo personalizado para *Chrome* que bloquea los rastreadores que realmente encuentra.

![](4.png)

Por ejemplo, si entramos en la página web del **Marca** veremos todos los rastreadores que ha bloqueado.

![](5.png)

### PixelBlock

*PixelBlock* agrega un pequeño ojo rojo junto a cualquier correo electrónico que intente rastrear su comportamiento, para que sepa exactamente quién trata de ver lo que está haciendo.

![](6.png)

Si miramos en nuestro correo electrónico, comprobaremos que los correos de *Instagram* contienen un rastreador.

![](7.png)

## Firefox

### AdBlock Ultimate

Esta extensión, bloquea todos los anuncios de las páginas *web*.

![](8.png)

Si entramos en *YouTube* podemos ver que ha bloqueado **94** anuncios y en total ha bloqueado **17.518**.

![](9.png)

### Browsec VPN

Ahora usaremos una extensión para usar una **VPN**.

![](10.png)

Una vez instalada podemos iniciar la conexión **VPN**.

![](11.png)

Estaremos conectados desde **Holanda**.

![](12.png)

### DuckDuckGo Privacy

Por último, instalaremos esta extensión.

![](13.png)

Esta extensión nos permitirá ver la privacidad de los sitios web como *YouTube*.

![](14.png)

Veremos que esta web tiene muy malas prácticas de privacidad.

![](15.png)
