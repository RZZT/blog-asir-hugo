---
title: Ley Orgánica de Protección de Datos
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.thewca.uk%2Fwp-content%2Fuploads%2F2018%2F03%2Fpp2.png&f=1&nofb=1&ipt=aefc8df598afbf1fec0b98d4bb0fe75ea405af267367ebfe5f8eb1f269263212&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-04"
date: "2022-10-04 01:00:00+0200"
slug: /2022/10/ley-organica-de-proteccion-de-datos
tags:
- LODP
- Borrado de Datos
---

## Borrado seguro de datos

El borrado seguro de datos es una práctica crucial para la **ciberseguridad** de las empresas. Estos datos pueden encontrarse en **soportes** como los siguientes:

- Bases de datos

- Documentos de texto

- Archivos

- Imágenes

Los **formatos** pueden ser:

- *Digital*:
  
  - CD
  
  - DVD
  
  - USB
  
  - Discos magnéticos
  
  - Tarjetas de memoria

- *Formato convencional*:
  
  - Papel
  
  - Microfichas
  
  - Películas

## Métodos de borrado seguro

### Desmagnetización

Es la exposición a un intenso **campo magnético** de los soportes de almacenamiento. Es el método adecuado para discos duros, cintas magnéticas de **backup**...


### Destrucción física

Consiste en la **eliminación física** de un documento o archivo. Existen diferentes formas:

- Desintegración

- Pulverización

- Fusión

- Incineración

- Trituración

### Sobreescritura

Se trata de **escribir un patrón de datos** encima de los datos que queremos borrar en un dispositivo de almacenamiento. Para garantizar la total destrucción hay que escribir **toda la superficie almacenada.**

## Obligaciones de la Ley Orgánica de Protección de Datos

- Tratamiento de datos con **exactitud, confidencialidad.**

- Especificación de la **finalidad** del tratamiento de datos.

- Necesidad de recabar el **consentimiento** expreso para cada finalidad en la que se utilizarán los datos personales, siempre que sea necesario.

- Derecho de **acceso, rectificación o supresión.**

- Derecho de **oposición, limitación y portabilidad**.

- Disposiciones para tratamientos de **datos concretos**.

- Principio de **responsabilidad proactiva**.

- La Ley de Protección de Datos regula la transferencia **internacional** de datos.

- **Transparencia** sobre el tratamiento de la información solicitada por la empresa.

- **Plazo de conservación de datos** Responsabilidad para bloquear o destruir los datos después de un tiempo determinado.

- Regulación y competencias de determinadas **comunidades autónomas** en términos de protección de datos.

- **Minimización** de datos personales, siempre cuando sea posible, se deberán tratar los datos estrictamente necesarios para el efecto necesario.

- Procedimientos en caso de la **vulneración** de la LOPD.

- **Sanciones** por infracciones a la ley (leves, graves o muy graves), marcadas por la normativa europea RGPD.

- **Plazos** **de prescripción** de infracciones.

- Reconocimiento y garantía de **derechos digitales**.

- **Otros** derechos digitales, como el acceso universal a la red, el derecho de seguridad o el derecho a la educación digital.
