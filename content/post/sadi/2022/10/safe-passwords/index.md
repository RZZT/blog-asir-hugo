---
title: Contraseñas Seguras
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.sage.com%2Fes-es%2Fblog%2Fwp-content%2Fuploads%2Fsites%2F8%2F2020%2F10%2F1024_contra.jpg&f=1&nofb=1&ipt=bebe91a1df10a81ce514d75b39708369a8ec5b5b6cd915d26be4ac1e01c91a19&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-27"
date: "2022-10-24 01:00:00+0200"
slug: /2022/10/contraseñas-seguras
tags:
- Privacidad
- Contraseñas
- LastPass
---

Actualmente, usamos muchas contraseñas debido a que tenemos multitud de servicios. Por eso es que las contraseñas están muy demandadas entre los ciberdelincuentes. En este *post* vamos a explicar unos consejos para tener contraseñas más seguras.

## Contraseñas distintas para cada servicio

Normalmente, solemos usar la misma contraseña para todo, porque es más cómodo y fácil de recordar y usar, pero a su vez es una brecha de seguridad debido a que si descubren la contraseña de un servicio como por ejemplo *Facebook*, podrían acceder a otros servicios de banca. Una buena práctica sería añadir como prefijo o sufijo el nombre del servicio a la contraseña. 

Si tu contraseña es **Welcome1**, puedes usar **Welcome1@Instagram**, **Welcome1@Facebook**... 

Así para cada servicio. 

## Cambia la contraseña regularmente

Deberíamos **cambiar la contraseña** cada cierto tiempo **(4 o 5 meses)**. De esta manera si alguien consigue nuestra contraseña en una filtración de datos, no le servirá.

## Contraseñas fuertes

Es recomendable seguir estos consejos para crear contraseñas **seguras**.

1. **Evita usar tu nombre**, tus apellidos o tu fecha de nacimiento como contraseña.

2. **Mezcla letras mayúsculas, letras minúsculas, números y signos.**

3. Escribe **8 o más caracteres**.

## Crear contraseña fácil de recordar

- Utilizamos una frase que podamos recordar, como por ejemplo:

> "Donde no hay mata, no hay patata."

- Cogemos las iniciales de la frase, alternando mayúsculas y minúsculas. Añadiremos un número y símbolos.

> @DnHmNhP1234

- Añadiremos el nombre del servicio como prefijo

> Gmail@DnHmNhP2022

## Gestor de contraseñas

Por último, podemos utilizar un gestor de contraseñas como *Keepass*, *1password*, *Bitwarden* o *Keeper*. Pero tenemos que tener en cuenta que si es un servicio *on-line* también puede sufrir ataques como ocurrió con **LastPass**. 

Noticias: [LastPass Hacked](https://www.forbes.com/sites/daveywinder/2022/08/25/lastpass-hacked-password-manager-with-25-million-users-confirms-breach/), [LastPass source code breach - incident response report released](https://nakedsecurity.sophos.com/2022/09/19/lastpass-source-code-breach-incident-response-report-released/)

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fblog.cygilant.com%2Fhs-fs%2Fhubfs%2F2016_Blog_and_Social%2Fbreach_LastPass.jpg%3Fwidth%3D539%26height%3D293%26name%3Dbreach_LastPass.jpg&f=1&nofb=1&ipt=e4893651a431b3c35bfa6b54919baeb51da071ea05aeb50cf5a3b51b0d85c8a0&ipo=images)
