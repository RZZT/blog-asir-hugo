---
title: Correo electrónico cifrado
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.redeszone.net%2Fapp%2Fuploads-redeszone.net%2F2018%2F01%2Fservicio-correo-electronico-cifrado.jpg&f=1&nofb=1&ipt=d425ab30045d9f8f4223fa69dcaa3d13952cef844f934975b7d7a5514a2089fa&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-12-12"
date: "2022-12-12 01:00:00+0200"
slug: /2022/12/correo-electronico-cifrado
tags:
- Criptografía
- Correo electrónico
---

Los correos electrónicos cifrados son una forma segura de enviar y recibir mensajes a través de internet. A diferencia de los correos electrónicos convencionales, los correos cifrados utilizan técnicas de cifrado para asegurar que sólo el destinatario autorizado pueda leer el contenido del mensaje.

Existen varios protocolos y herramientas disponibles para cifrar correos electrónicos, como **PGP** *(Pretty Good Privacy)* y **S/MIME** *(Secure/Multipurpose Internet Mail Extensions)*. Estos protocolos utilizan un par de **claves pública** y **privada** para cifrar y descifrar los mensajes. La clave pública se utiliza para **cifrar el mensaje** y la clave privada se utiliza para **descifrarlo**.

Otra opción popular es el uso de servicios de correo electrónico cifrado, como [ProtonMail](https://protonmail.com/) o [Tutanota](https://tutanota.com/), que ofrecen una capa adicional de seguridad al cifrar automáticamente todos los mensajes enviados y recibidos a través de su plataforma.

![](https://fpf.org/wp-content/uploads/2017/01/Email_Privacy_581432440.jpg)

Sin embargo, es importante tener en cuenta que el cifrado de correo electrónico no garantiza completamente la seguridad de los mensajes. Por ejemplo, si un atacante tiene acceso a la clave privada del destinatario, podría descifrar los mensajes cifrados. Además, los atacantes también pueden intentar interceptar los mensajes cifrados durante la transmisión a través de internet. Por lo tanto, es importante seguir practicando buenas prácticas de seguridad en línea, como utilizar contraseñas seguras y mantener sistemas y aplicaciones actualizados.

En resumen, el cifrado de correo electrónico es una **excelente manera de proteger la privacidad de los mensajes y asegurar que sólo el destinatario autorizado pueda leer su contenido**. Sin embargo, es importante tener en cuenta que no es una solución infalible y debe combinarse con otras medidas de seguridad en línea para garantizar la seguridad total de los mensajes.
