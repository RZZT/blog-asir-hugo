---
title: Funciones unidireccionales HASH
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.ehssRaxBohC2UB2THlvAqQHaEf%26pid%3DApi&f=1&ipt=cbb4ac4e3afcd8c8423210340ea937af850cc365c629f83e382c74e055e44204&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-09-25"
date: "2022-09-25 01:00:00+0200"
slug: /2022/09/funciones-unidireccionales-hash
tags:
- Hash
---

## ¿Qué es una función unidireccional?

 Son funciones que tienen la propiedad de ser fáciles de calcular pero difíciles de invertir.

## ¿Qué es un HASH?

Un *Hash* es un algoritmo matemático que transforma cualquier bloque arbitrario de
 datos en una nueva serie de caracteres con una longitud fija.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.altaruru.com%2Fwp-content%2Fuploads%2F2018%2F10%2FHASH1.png&f=1&nofb=1&ipt=49ae02968cf63c4dea0e6348033451c72ec8c0044df4d66393a2d01d1a2fda63&ipo=images)

### Tipos de HASH

Existen una gran variedad de algoritmos HASH, los más utilizados son:

- **MD5**: 16 *bytes*.

- **SHA1**: 20 *bytes*.

- **SHA256**: 55 caracteres de longitud.

- **SHA512**: 98 caracteres de longitud.

### Programas que podemos utilizar

Para comprobar la **integridad** de nuestros archivos existen multitud de programas, tanto para *Windows*, *Linux* y *macOS*. Estos son unos ejemplos:

#### Portables

- [Advanced Hash Calculator](https://www.portalprogramas.com/advanced-hash/)

- [winMd5Sum](https://www.portalprogramas.com/winmd5sum-portable/)

#### Windows

- [OpenHashTab](https://github.com/namazso/OpenHashTab)

- [PowerShell](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-7.2)

#### Multiplataforma

- [QuickHash GUI](https://www.quickhash-gui.org/)

### ¿Para qué se utilizan?

#### Contraseñas

La mayoría de sitios *web* no almacenan las contraseñas en texto claro. Cuando te registras e introduces tu contraseña, esta página solo almacena el *hash* de tu contraseña. Cuando quieres acceder otra vez, genera el *hash* de la contraseña que has introducido y la compara con la que tiene almacenada en su base de datos.

#### Antivirus

Las empresas de antivirus, utilizan funciones criptográficas *hash* para detectar *malware*. Existen listas de *hash* *malware*

#### Archivos y mensajes

Por último, se utiliza para comprobar la **integridad** de **archivos** y mensajes. Encontrarás en muchos sitios *web* de descargas, un archivo llamado *MD5.sum* o algún otro tipo de *hash* con la extensión *.sum*. Esto significa que si quieres comprobar que el archivo no ha sido modificado por un tercero de forma malintencionada.
