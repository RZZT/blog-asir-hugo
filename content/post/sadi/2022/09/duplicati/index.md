---
title: Duplicati
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.redeszone.net%2Fapp%2Fuploads%2F2017%2F04%2Fduplicati-logo.jpg&f=1&nofb=1&ipt=6f0600c08d41976d15a251fa205a62426b8cb6af38bbf205e458e57a79c07a15&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-04"
date: "2022-09-29 01:00:00+0200"
slug: /2022/09/duplicati
tags:
- Duplicati
- Copias de Seguridad
- Windows 10
---

## Instalación

Primero, descargaremos el instalador para *Windows* desde su página web oficial.

![](1.png)

Cuando termine de descargarse, lo ejecutaremos.

![](2.png)

Aceptamos la licencia.

![](3.png)

Instalaremos todos los componentes.

![](4.png)

Comenzaremos la instalación.

![](5.png)

Esperamos a que se instale e iniciaremos la aplicación.

![](6.png)

## Configuración

La configuración se hace desde el navegador.

![](7.png)

### Añadir una copia de seguridad

Primero, crearemos una copia.

![](8.png)

Configuraremos una nueva.

![](9.png)

Indicamos una frase de seguridad.

![](10.png)

Luego, indicaremos el destino de la copia de seguridad.

![](11.png)

Y el directorio de origen. Es decir el que se va a copiar.

![](12.png)

Estableceremos un horario para que se ejecute una vez al día.

![](13.png)

Solo se conservarán 2 copias.

![](14.png)

### Ejecutar

Una vez se haya creado la copia de seguridad, la ejecutaremos ahora.

![](15.png)

Solo tardará 6 segundos...

![](16.png)

Veremos que se han creado unos archivos pero están cifrados.

![](17.png)

### Recuperar

Por último, restauraremos la copia de seguridad desde el panel *web*.

![](18.png)

Veremos que se van a restaurar los 3 archivos de la copia de seguridad.

![](19.png)

Completaremos las opciones de restauración y continuaremos.

![](20.png)

Esperamos a que se restauren losa archivos.

![](21.png)

Observaremos que en la carpeta de la copia de seguridad ahora aparecen los archivos descifrados.

![](22.png)

## Conclusión

**Duplicati** me parece una buena alternativa de *Cobian* para hacer tareas de Copias de Seguridad. El panel de configuración *web* puede ser de gran ayuda cuando se tiene que configurar en un equipo remoto. 
