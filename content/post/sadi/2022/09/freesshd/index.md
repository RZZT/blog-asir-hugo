---
title: Free SSH
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fst2.depositphotos.com%2F1001860%2F10218%2Fi%2F450%2Fdepositphotos_102188322-stock-photo-ssh-secure-shell.jpg&f=1&nofb=1&ipt=c800e0ff2167aee3eeebde30a24d606ad537703588079cbe29071433f3f99079&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-09-27"
date: "2022-09-27 01:00:00+0200"
slug: /2022/09/freesshd
tags:
- FreeSSHd
- SSH
- Windows Server 2019
---

## Instalar

Primero, descargaremos el instalador de su [página web](http://www.freesshd.com/) oficial.

![](1.png)

Luego, ejecutaremos el instalador y comenzará el asistente.

![](2.png)

Indicamos la ruta de instalación.

![](3.png)

Haremos una instalación completa.

![](4.png)

Comprobaremos las configuraciones e instalaremos.

![](7.png)

## Configuración

Crearemos las claves privadas ahora

![](8.png)

Iniciaremos *FreeSSHd* como un servicio del sistema.

![](9.png)

Finalizaremos el asistente.

![](10.png)

## Conexión

Iniciaremos el panel gráfico e iniciaremos el servicio *SSH*.

![](11.png)

Añadiremos al usuario *Administrador*.

![](12.png)

Ahora nos podremos conectar usando *PuTTY* desde el equipo cliente.

![](13.png)

## Conclusión

*FreeSSH* es un buen programa para administrar el servicio *SSH* como alternativa a *OpenSSH*.

> Después de hacer este artículo, he desinstalado el programa para seguir utilizando *OpenSSH*.
