---
title: Almacenamiento externo NAS / Copias de Seguridad
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fwp8179608.jpg&f=1&nofb=1&ipt=abf7150ecb343a44272ea83651e54643e1787b158ca5c8357232d774f858f95d&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-10-05"
date: "2022-09-30 01:00:00+0200"
slug: /2022/09/sistemas-nas
tags:
- NAS 
- Copias de Seguridad
---

## Medios de almacenamiento para copias de seguridad

- Dispositivos *USB*

- Disco externo

- *NAS*

- Almacenamiento en la nube
  

## Servicios profesionales de copias de seguridad

Estos son algunos servicios para hacer copias de seguridad *Online*:

- [BackBlaze](https://www.backblaze.com/best-online-backup-service.html#af9xut "Backblaze Backblaze - BackBlaze")

- [iDrive](https://www.anrdoezrs.net/links/3607085/type/am/sid/790664-xid-fr1665055546aaa/https://www.idrive.com/ "iDrive iDrive - iDrive")

- [Carbonite](https://www.anrdoezrs.net/links/3607085/type/am/sid/790664-xid-fr1665055546aab/https://www.carbonite.com/ "Carbonite Carbonite Safe - Carbonite")

- [Acronis](https://www.anrdoezrs.net/links/3607085/type/am/sid/790664-xid-fr1665055546aac/https://www.acronis.com/en-us/products/true-image/ "Acronis Acronis Cyber Protect Home Office - Acronis")

- [pCloud](https://www.anrdoezrs.net/links/3607085/type/am/sid/790664-xid-fr1665055546aad/https://www.pcloud.com/ "pCloud pCloud - pCloud")

## Sistemas NAS para copias de seguridad

Otra opción para hacer copias de seguridad es utilizar un sistema *NAS*. Si montamos este servidor por nuestra cuenta, podremos tener todo el control sobre nuestros datos. Podemos comprar **dispositivos** específicos para este propósito como:

- [QNAP TS-233-US](https://www.amazon.com/QNAP-TS-233-US-Affordable-Cortex-A55-Quad-core/dp/B09VCYCFSY?tag=p00935-20&ascsubtag=0399iBDMEdnk4qDQOnWCxns)

- [Asustor Lockerstor 2 Gen2 AS6702T](https://www.amazon.com/Asustor-Lockerstor-Gen2-AS6702T-Quad-Core/dp/B09VXCSLR4?tag=p00935-20&ascsubtag=05RR3NPJcD2n64Wa8smtKXw)

- [Synology 4 Bay NAS DS420j](https://www.amazon.com/Synology-DiskStation-DS220j-Diskless-2-Bay/dp/B0855LMP81?tag=p00935-20&ascsubtag=01kebuxO3M71rRaXm83kOGb)

- [QNAP TS-230 2-Bay Home NAS](https://www.amazon.com/QNAP-TS-230-Cortex-A53-Quad-core-Processor/dp/B083W6328Q?tag=p00935-20&ascsubtag=04zJtdoGwBGu6CEmWmyqnH5)

Por otra parte, también podemos utilizar solo el **programa** / **sistema operativo** para montar el servicio como [Synology](https://www.synology.com/en-us/support/download), [TrueNas](https://www.truenas.com/download-truenas-core/) o [NextCloud](https://nextcloud.com/install/#instructions-server) en un ordenador o servidor que tengamos en casa.
