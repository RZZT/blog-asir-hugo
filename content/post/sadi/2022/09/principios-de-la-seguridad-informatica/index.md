---
title: Principios de la Seguridad Informática
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fexpertech.com.co%2Fwp-content%2Fuploads%2F2020%2F08%2F1-6.png&f=1&nofb=1&ipt=9daf11ac5f57babd8bc181147faec04dd458a71941eced20edcc8f6e56562899&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-09-19"
date: "2022-09-19 01:00:00+0200"
slug: /2022/09/principios-de-la-seguridad-informatica
tags:
  -  
---

> Para proteger la información deben **implementar controles de seguridad y planes de respuesta** que mitiguen los múltiples riesgos que afectan tanto a la información en tránsito como en almacenamiento.

## Confidencialidad

Este principio se basa en la **privacidad** de la información, el **denegar el acceso** libre a determinada información y que solo las <u>personas autorizadas</u> puedan ver dicho contenido. Un ejemplo de confidencialidad es el **cifrado de datos**, los factores de autenticación y toda medida impuesta para **denegar el acceso** a una determinada información a personas que no estén autorizadas.

## Integridad

Hace referencia a que la información que se encuentra almacenada en los dispositivos o la que se ha transmitido por cualquier canal de comunicación **no ha sido manipulada por terceros de manera malintencionada**. Esto garantiza que la información **no será modificada** por personas no autorizadas. Para poder comprobar la integridad podemos usar algoritmos **<u>HASH</u>** como `MD5`, `SHA-2`, `SHA-256` o `SHA512`.

## Disponibilidad

Quiere decir que los datos deben estar siempre disponibles para los individuos autorizados. Esto se traduce en su **permanente acceso y posibilidad de recuperación** en caso de incidente. Tiene un coste salarial muy grande,  por ejemplo, medidas de disponibilidad es la **redundancia de datos**, tener dos procesadores, un sistema **SAI**...

--- 

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fvignette.wikia.nocookie.net%2Fticsalborada1%2Fimages%2F3%2F34%2FPrincipios_de_la_seguiridad_informatica.jpg%2Frevision%2Flatest%3Fcb%3D20181211094228%26path-prefix%3Des&f=1&nofb=1)
