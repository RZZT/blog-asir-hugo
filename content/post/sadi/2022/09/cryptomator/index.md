---
title: Cryptomator
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmobilsicher.de%2Fwp-content%2Fuploads%2F2020%2F05%2Fcryptomator.jpg&f=1&nofb=1
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-09-22"
date: "2022-09-22 01:00:00+0200"
slug: /2022/09/cryptomator
tags:
- Encriptación
- Cryptomator
---

> *Cryptomator* es un programa de **código abierto** que sirve para **cifrar** carpetas y archivos de manera local o remota (en la nube) muy similar a *VeraCrypt*.

## Instalación

Descargaremos el programa de su [página web](https://cryptomator.org/downloads/) oficial.

![](1.png)

Ejecutamos el instalador y aceptamos la licencia *GPLv3*.

![](2.png)

Comenzará la instalación y esperaremos a que finalice.

![](3.png)

## Crear una bóveda

Después de instalarlo, iniciaremos el programa y añadiremos una *bóveda*.

![](4.png)

Seleccionaremos crear una nueva.

![](5.png)

Le damos un nombre **original**.

![](6.png)

Seleccionamos una ubicación personalizada en el directorio de nuestro usuario.

![](7.png)

Después estableceremos una **contraseña segura** para acceder a nuestra bóveda.

![](8.png)

## Desbloquear la bóveda

Una vez se haya creado, la desbloquearemos por primera vez.

![](9.png)

Ingresaremos nuestra contraseña secreta para desbloquear la bóveda.

![](10.png)

Después, revelaremos la unidad.

![](11.png)

Ahora podemos montar nuestra bóveda en la unidad *H:* y veremos que hay un archivo.

![](12.png)

## Bloquear la bóveda

Por último, **bloquearemos** de nuevo la bóveda para mantener nuestros archivos a salvo.

![](13.png)

Ya no aparecerá la unidad *H:* montada en nuestro sistema.

![](14.png)

## Conclusiones

**Cryptomator** me ha parecido un programa muy bueno debido a que tiene **soporte** para todos los sistemas operativos disponibles, es de **código abierto** y es muy **fácil** de usar. Además es muy útil ya que implementa el **cifrado** en la **nube** y de esta forma eres tú quien tiene las claves para descifrar tus datos y no el proveedor de este servicio.
