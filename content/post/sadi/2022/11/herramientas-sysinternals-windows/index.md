---
title: Herramientas SysInternals Windows
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.qualiero.com%2Fsource%2Ffiles%2Fuploadimages%2F4a4e517b35667f25e3c31e5ee2fb23cf.jpg&f=1&nofb=1&ipt=bb8a8f00de121e4d7ed33f06eef2b81cc552bdc0a1afb518bd4036dc3fe3c203&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-11-14"
date: "2022-11-08 01:00:00+0200"
slug: /2022/11/herramientas-sysinternals-windows
tags:
  - 
---

Hoy en día, *malwares* se hacen pasar por **procesos legítimos** de nuestro sistema, pero podríamos identificar rápidamente a los **virus**, si conocemos cuales son realmente lo procesos legítimos de **Windows**.

## Documentación oficial

Desde la [página web oficial](https://learn.microsoft.com/es-es/sysinternals/downloads/) de **Microsoft**, veremos varias herramientas.

![](1.png)

## Herramientas

### [Autoruns](https://learn.microsoft.com/es-es/sysinternals/downloads/autoruns)

Enfoque mas detallado acerca de los servicios, aplicaciones y librerías que se estén ejecutando.

![](2.png)

### [Bginfo](https://learn.microsoft.com/es-es/sysinternals/downloads/bginfo)

Escribe un nuevo mapa de bits de escritorio y sale, no tiene que preocuparse por consumir recursos del sistema ni interferir con otras aplicaciones.

![](3.jpg)

### [Cacheset](https://learn.microsoft.com/es-es/sysinternals/downloads/cacheset)

Nos permite establecer parámetros relacionados con la memoria cache.

![](4.gif)

### [Dbgview](https://learn.microsoft.com/es-es/sysinternals/downloads/debugview)

Capturas de los escritorios que tengamos accesibles.

![](5.gif)

### [Diskmon](https://learn.microsoft.com/es-es/sysinternals/downloads/diskmon)

Manipular a tiempo real los sectores de nuestros discos duros activos.

![](6.gif)
