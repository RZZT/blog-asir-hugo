---
title: Keyloggers
description: 
image: https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.uEnr1nxaSgIsfNTlyydK-wHaE7%26pid%3DApi&f=1&ipt=acf9d7fc2fe63b0c79d08c959993dc0c393d785f5959ac489e5909b790e9e523&ipo=images
categories:
  - SADi
author: "Raúl Sánchez"
lastmod: "2022-11-14"
date: "2022-11-04 01:00:00+0200"
slug: /2022/11/keyloggers
tags:
- Keyloggers 
---

Como su nombre lo indica un **Keylogger** es un programa que registra y graba la pulsación de teclas (y algunos también clicks del mouse). La información recolectada será utilizada luego por la persona que lo haya instalado. Actualmente existen dispositivos de **hardware** o bien aplicaciones **(software)** que realizan estas tareas.

## Keyloggers del mercado

En esta página [web](https://www.keyloggers.com/es/) encontraremos varios **keyloggers** a la venta.

![](1.png)

## Aplicaciones de rastreo

Hay muchas aplicaciones que nos permiten hacer un rastreo de nuestros móviles o incluso el de otras personas.

Esto es posible debido a que en la actualidad la mayoría de los dispositivos móviles cuentan con un **GPS** integrado el cual nos mantiene localizables en cualquier momento, siempre que este activado. Esto puede ser de gran ayuda para algunas cosas como por ejemplo buscar un móvil robado, aunque no tanto para otras, hay veces que **no nos interesa que nadie sepa nuestros movimientos**.

### *mSpy*

*mSpy* te ayuda rastrear el celular objetivo de otra persona sin ser detectado. También se puede utilizar para espiar a los datos cruciales y a la actividad de las redes sociales del dispositivo. Sin embargo, se necesita hacerle previamente el **jailbreak** al dispositivo.

![](2.png)

### *The Truth Spy*

Es aplicación popular de espionaje que te permite obtener un acceso  a los datos y la actividad del dispositivo objetivo. Necesitaría un dispositivo con **jailbreak** para que esta aplicación de espía para el rastreo de ubicación en tiempo real funcione sin problemas.

![](3.png)

## Control Parental

Las aplicaciones de control parental **permiten a los padres controlar los movimientos de sus hijos en la red**, ya sea en el ordenador o en su teléfono móvil. Estas aplicaciones permiten poner filtros para evitar que los niños accedan a paginas impropias de su edad, controlar que no accedan a paginas en las que puedan gastar dinero sin el permiso de sus padres... Además también permite rastrear la **localización** para mantener controlados a los hijos.

Algunas de las mejores aplicaciones de control parental son:

### ESET Parental Control

- [Página Web](https://parentalcontrol.eset.com/)

![](4.jpg)

### Qustodio

- [Página Web](https://www.qustodio.com/es/)

![](5.jpg)
